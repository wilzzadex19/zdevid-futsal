<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'm_barang';
    protected $fillable = [
        'nama_barang',
        'kode_barang'
    ];

    public function jenis()
    {
        return $this->hasOne(Jenis::class,'id','jenis_id');
    }
    public function merk()
    {
        return $this->hasOne(Merk::class,'id','merk_id');
    }

    public function detail()
    {
        return $this->hasMany(Detail_BarangKeluar::class,'barang_id','id');
    }

}
