<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_Penjualan extends Model
{
    protected $table = 'detail_barang_masuk';
    public $timestamps = false;

    public function barang()
    {
        return $this->hasOne(Barang::class,'id','barang_id');
    }
}
