<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HUrl extends Model
{
    protected $table = 'booking_url';
    public $timestamps = false;
    protected $fillable  =['ip','data'];
}
