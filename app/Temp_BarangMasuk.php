<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temp_BarangMasuk extends Model
{
    protected $table = 'temp_barang_masuk';
    public $timestamps = false;

    public function barang()
    {
        return $this->hasOne(Barang::class,'id','barang_id');
    }
}
