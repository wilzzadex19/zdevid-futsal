<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merk extends Model
{
    protected $table = 'm_merk';

    public function jenis()
    {
        return $this->hasOne(Jenis::class,'id','jenis_id');
    }
}
