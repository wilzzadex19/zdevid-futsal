<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temp_BarangKeluar extends Model
{
    protected $table = 'temp_barang_keluar';
    public $timestamps = false;

    public function barang()
    {
        return $this->hasOne(Barang::class,'id','barang_id');
    }
}
