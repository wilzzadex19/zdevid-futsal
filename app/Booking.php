<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'booking';
    protected $fillable = [
        'kode',
        'user_id',
        'team_name',
        'tanggal_transaksi',
        'status',
        'jenis_pembayaran',
        'total_dp',
        'total_harga',
        'no_hp',
        'keterangan',
        'payment_code',
        'payment_url',
        'payement_code',
        'payment_exp',
    ];

    public function users(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function detail()
    {
        return $this->hasMany(Detail_Booking::class,'booking_id','id');
    }
}
