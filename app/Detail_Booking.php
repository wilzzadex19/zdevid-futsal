<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_Booking extends Model
{
    protected $table = 'booking_detail';

    public function parents()
    {
        return $this->hasOne(Booking::class, 'id', 'booking_id');
    }

    public function item()
    {
        return $this->hasOne(Item::class, 'id', 'item_id');
    }
    public function hari()
    {
        return $this->hasOne(Hari::class, 'id', 'hari_id');
    }
    public function jam()
    {
        return $this->hasOne(Jam::class, 'id', 'jam_id');
    }
}
