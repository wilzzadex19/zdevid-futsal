<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'm_item';

    public function category()
    {
        return $this->hasOne(Category::class,'id','category_id');
    }

    public function jadwal_all()
    {
        return $this->hasMany(Jadwal::class,'item_id','id');
    }
}
