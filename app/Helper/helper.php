<?php

namespace App\Helper;

use Illuminate\Support\Facades\DB;

class helper
{
    public static function getMenu()
    {
    }

    public static function kode_otomatis($table, $awalan, $lebar, $field)
    {
        $awalan = $awalan;
        $query = DB::table($table)->select($field)->orderBy($field, 'desc');

        $lebar = $lebar;
        $jumlahrecord = $query->count();

        if ($jumlahrecord == 0) {
            $nomor = 1;
        } else {
            $row = $query->get()->toArray();
            $nomor = intval(substr($row[0]->$field, strlen($awalan))) + 1;
        }

        if ($lebar > 0)
            $angka = $awalan . str_pad($nomor, $lebar, "0", STR_PAD_LEFT);
        else
            $angka = $awalan . $nomor;
        return $angka;
    }

    public static function limit_desc($x, $length)
    {
        if (strlen($x) <= $length) {
            echo $x;
        } else {
            $y = substr($x, 0, $length) . '...';
            echo $y;
        }
    }

    public static function to_hari($d)
    {
        if ($d == 'Wed') {
            $s = 'Rabu';
        } else if ($d == 'Thu') {
            $s = 'Kamis';
        } else if ($d == 'Tue') {
            $s = 'Selasa';
        } else if ($d == 'Fri') {
            $s = "Jum'at";
        } else if ($d == 'Sat') {
            $s = 'Sabtu';
        } else if ($d == 'Sun') {
            $s = 'Minggu';
        } else if ($d == 'Mon') {
            $s = 'Senin';
        }

        return $s;
    }

    public static function tgl_indo_jam($tanggal)
    {
        try {
            $bulan = [
                1 => 'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember',
            ];
            $pecahkan = explode('-', $tanggal);
    
            $pecahkan_2 = explode(' ', $pecahkan[2]);
    
            $pecahkan_3 =  explode(':', $pecahkan_2[1]);
    
            // dd($pecahkan_3);
            return  $pecahkan_2[0] . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0] . ' - ' . $pecahkan_3[0] . ':' . $pecahkan_3[1];
        } catch (\Throwable $th) {
            return '-';
        }
        
    }


    public static function  tgl_indo($tanggal)
    {
        $bulan = [
            1 => 'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember',
        ];
        $pecahkan = explode('-', $tanggal);

        try {
            $res =  $pecahkan[2] . ' ' . $bulan[(int) $pecahkan[1]] . ' ' . $pecahkan[0];
        } catch (\Throwable $th) {
            $res = '-';
        }

        return $res;
    }
}
