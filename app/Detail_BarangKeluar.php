<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_BarangKeluar extends Model
{
    protected $table = 'detail_barang_keluar';
    public $timestamps = false;

    public function barang()
    {
        return $this->hasOne(Barang::class,'id','barang_id');
    }

    public function parents(){
        return $this->belongsTo(BarangKeluar::class,'barangkeluar_id','id');
    }
}
