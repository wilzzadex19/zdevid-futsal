<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangKeluar extends Model
{
    protected $table = 'barang_keluar';

    public function detail()
    {
        return $this->hasMany(Detail_BarangKeluar::class,'barangkeluar_id','id');
    }

}
