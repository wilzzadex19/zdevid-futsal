<?php

namespace App\Http\Controllers;

use App\App_Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        return view('back.pages.setting.index');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $setting = App_Setting::find(1);
        $setting->app_name = $request->nama_aplikasi;
        $setting->app_description = $request->tentang;

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $file_name = 'app_logoss.' . $file->getClientOriginalExtension();
            $file->move('assets/img/', $file_name);
            $setting->app_logo = $file_name;
        }

        $setting->save();

        return redirect()->back()->with('success', 'Berhasil menyimpan perubahan');
    }
}
