<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helper\helper;
use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function index()
    {
        $data['item'] = Item::orderBy('kode', 'asc')->get();
        return view('back.pages.item.index', $data);
    }

    public function add()
    {
        $data['item_kode'] = helper::kode_otomatis('m_item', 'L', 1, 'kode');
        $data['category'] = Category::all();
        return view('back.pages.item.add', $data);
    }

    public function store(Request $request)
    {
        $item = new Item();
        $item->kode = $request->kode_item;
        $item->name = $request->nama;
        $item->category_id = 1;
        $item->desc = $request->deskripsi;
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $file_name = md5($request->kode_item) . '.' . $file->getClientOriginalExtension();
            $file->move('img/item/', $file_name);
            $item->foto = $file_name;
        }

        $item->save();

        return redirect(route('item'))->with('success', 'Data berhasil di simpan');
    }

    public function edit($id)
    {
        $item = Item::findOrFail($id);
        $data['item'] = $item;
        $data['category'] = Category::all();
        return view('back.pages.item.edit', $data);
    }

    public function update($id, Request $request)
    {
        $item = Item::findOrFail($id);
        $item->name = $request->nama;
        // $item->category_id = 1;
        $item->desc = $request->deskripsi;
        if ($request->hasFile('logo')) {
            $old_foto = 'img/item/' . $item->foto;
            if(is_file($old_foto)){
                unlink($old_foto);
            }
            $file = $request->file('logo');
            $file_name = md5($request->kode_item) . '.' . $file->getClientOriginalExtension();
            $file->move('img/item/', $file_name);
            $item->foto = $file_name;
        }

        $item->save();
        return redirect()->back()->with('success','Data berhasil di ubah');
    }

    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        if($item->foto != null){
            $file_ex = 'img/item/' . $item->foto;
            if(is_file($file_ex)){
                unlink($file_ex);
            }
        }
        $item->delete();
        return redirect(route('item'))->with('success','Data berhasil di hapus');
    }
}
