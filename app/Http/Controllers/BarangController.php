<?php

namespace App\Http\Controllers;

use App\Detail_Penjualan;
use App\Barang;
use App\BarangKeluar;
use App\BarangMasuk;
use App\Detail_BarangKeluar;
use App\Helper\helper;
use App\Jenis;
use App\Merk;
use App\Suplier;
use App\Temp_BarangKeluar;
use App\Temp_BarangMasuk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

class BarangController extends Controller
{
    public function index()
    {

        $data['barang'] = Barang::orderBy('id', 'ASC')->get();
        return view('back.pages.list_barang.barang', $data);
    }

    public function add()
    {
        $query = Barang::orderBy('kode_barang', 'desc');
        $awalan = 'BG';
        $lebar = 3;
        $jumlahrecord = $query->count();
        if ($jumlahrecord == 0) {
            $nomor = 1;
        } else {
            $row = $query->get()->toArray();
            $nomor = intval(substr($row[0]['kode_barang'], strlen($awalan))) + 1;
        }

        if ($lebar > 0)
            $angka = $awalan . str_pad($nomor, $lebar, "0", STR_PAD_LEFT);
        else
            $angka = $awalan . $nomor;


        $data['kode'] = $angka;
        return view('back.pages.list_barang.add_barang', $data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'kode_barang' => 'unique:m_barang,kode_barang',
        ]);
        $barang = new Barang();
        $barang->kode_barang = $request->kode_barang;
        // $barang->nama_barang = $request->nama_barang;
        $barang->harga = str_replace(".", "", $request->harga);
        $barang->harga_jual = str_replace(".", "", $request->harga_jual);
        $barang->jenis_id = $request->jenis_id;
        $barang->merk_id = $request->merk_id;
        $barang->kategori = $request->kategori;
        // $barang->jumlah = $request->jumlah;
        $barang->save();

        return redirect(route('barang'))->with('success', 'Data barang berhasil di simpan !');
    }

    public function destroy(Request $request)
    {
        $barang = Barang::find($request->id);
        $cek = Detail_Penjualan::where('barang_id', $barang->id)->count();
        if ($cek == 0) {
            $barang->delete();
            return response()->json('oke');
        } else {
            return response()->json('no');
        }
    }

    public function edit($id)
    {
        $barang = Barang::find($id);
        if ($barang->kategori == 'Makanan') {
            $merk = Merk::where('kategori', 'Makanan')->get();
        } else {
            $merk = Merk::where('kategori', 'Minuman')->get();
        }
        $data['merk'] = $merk;
        $data['barang'] = $barang;
        return view('back.pages.list_barang.edit_barang', $data);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'kode_barang' => 'unique:m_barang,kode_barang,' . $id,
        ]);
        $barang = Barang::findOrFail($id);
        $barang->kode_barang = $request->kode_barang;
        // $barang->nama_barang = $request->nama_barang;
        $barang->harga = str_replace(".", "", $request->harga);
        $barang->harga_jual = str_replace(".", "", $request->harga_jual);
        $barang->jenis_id = $request->jenis_id;
        $barang->merk_id = $request->merk_id;
        $barang->kategori = $request->kategori;
        $barang->save();

        return redirect(route('barang'))->with('success', 'Data barang berhasil di ubah !');
    }

    public function cetak(Request $request)
    {
        $pisah = explode('/', $request->tanggal);
        $tanggal_awal = $pisah[0];
        $tanggal_akhir = $pisah[1];
        $barangs = Barang::orderBy('kode_barang', 'asc')->get();
        if (!empty($request->urutan)) {
            $sort = $request->urutan == 'tidak' ? 'asc' : 'desc';
            $barangs = Barang::withCount([
                'detail AS jml_terjual' => function ($query) use ($tanggal_awal, $tanggal_akhir) {
                    $query->select(DB::raw("SUM(jumlah) as paidsum"))->whereHas('parents', function ($q) use ($tanggal_awal, $tanggal_akhir) {
                        $q->whereBetween('tgl_faktur', [$tanggal_awal, $tanggal_akhir]);
                    });
                }
            ])->orderBy('jml_terjual', $sort)->get();
        }

        // dd($barangs);
        $data['barang'] = $barangs;
        $data['tanggal_awal'] = $tanggal_awal;
        $data['tanggal_akhir'] = $tanggal_akhir;
        $data['judul'] = 'Laporan Data Stok Barang Periode ' .  helper::tgl_indo($tanggal_awal) . ' s/d ' . helper::tgl_indo($tanggal_akhir);
        return view('back.pages.list_barang.cetak', $data);
    }

    // Pembelian Barang

    public function indexBarangMasuk()
    {
        $data['data'] = BarangMasuk::orderBy('id', 'desc')->get();
        $data['suplier'] = Suplier::all();

        return view('back.pages.barangMasuk.index', $data);
    }

    public function addBarangMasuk()
    {
        $date = date('dmy') . '-' . date('His') . '-';
        $awalan = 'SB' . $date;
        $query = BarangMasuk::where('no_faktur', 'like', '%' . date('dmy') . '%')->orderBy('no_faktur', 'desc');

        $lebar = 2;
        $jumlahrecord = $query->count();
        if ($jumlahrecord == 0) {
            $nomor = 1;
        } else {
            $row = $query->get()->toArray();
            $nomor = intval(substr($row[0]['no_faktur'], strlen($awalan))) + 1;
        }

        if ($lebar > 0)
            $angka = $awalan . str_pad($nomor, $lebar, "0", STR_PAD_LEFT);
        else
            $angka = $awalan . $nomor;
        $data['kode'] = $angka;
        $data['barang'] = Barang::orderBy('nama_barang', 'asc')->get();
        $data['suplier'] = Suplier::all();
        return view('back.pages.barangMasuk.add', $data);
    }

    public function addTempBarangMasuk(Request $request)
    {
        // dd($request->all());

        $cek = Temp_BarangMasuk::where('user_id', Auth::user()->id)->where('barang_id', $request->barang_id)->count();
        if ($cek == 0) {
            $total_harga = $request->harga * $request->jumlah;
            $diskon = ($request->diskon / 100) * $total_harga;
            $total_harga = $total_harga - $diskon;
            $new =  new Temp_BarangMasuk();
            $new->barang_id = $request->barang_id;
            $new->jumlah = $request->jumlah;
            $new->diskon = $request->diskon;
            $new->total_harga = $total_harga;
            $new->total_diskon = $diskon;
            $new->user_id = Auth::user()->id;
            $new->save();

            return response()->json('oke');
        } else {
            return response()->json('exist');
        }
    }

    public function renderTable()
    {
        $data['barang'] = Temp_BarangMasuk::where('user_id', Auth::user()->id)->with('barang')->get();
        $data['html'] = view('back.pages.barangMasuk._tabel', $data)->render();
        return response()->json($data);
    }

    public function editBarangMasuk(Request $request)
    {
        $data['temp'] = Temp_BarangMasuk::find($request->id);
        $data['barang'] = Barang::orderBy('nama_barang', 'asc')->get();
        return view('back.pages.barangMasuk._modal_edit', $data);
    }

    public function editTempBarangMasuk(Request $request)
    {
        // dd($request->all());
        $cek = Temp_BarangMasuk::where('user_id', Auth::user()->id)->where('barang_id', $request->barang_id)->where('id', '!=', $request->id)->count();
        if ($cek == 0) {
            $total_harga = $request->harga * $request->jumlah;
            $diskon = ($request->diskon / 100) * $total_harga;
            $total_harga = $total_harga - $diskon;
            $new =  Temp_BarangMasuk::find($request->id);
            $new->barang_id = $request->barang_id;
            $new->jumlah = $request->jumlah;
            $new->diskon = $request->diskon;
            $new->total_harga = $total_harga;
            $new->total_diskon = $diskon;
            $new->user_id = Auth::user()->id;
            $new->save();

            return response()->json('oke');
        } else {
            return response()->json('exist');
        }
    }

    public function deleteBarangMasuk(Request $request)
    {
        $delete = Temp_BarangMasuk::find($request->id)->delete();
    }

    public function storeBarangMasuk(Request $request)
    {
        // dd($request->all());
        $cek = BarangMasuk::where('no_faktur', $request->no_faktur)->count();
        if ($cek != 0) {
            return redirect()->back()->with('warning', 'No faktur sudah terdaftar!');
        } else {
            $new = new BarangMasuk();
            $new->no_faktur = $request->no_faktur;
            $new->tgl_faktur = $request->tgl_masuk;
            $new->total_diskon = $request->total_diskon;
            $new->sub_total = $request->sub_total;
            $new->suplier_id = $request->suplier_id;
            $new->pajak = $request->pajak;
            $new->grand_total = $request->grand_total;
            $new->user_id = Auth::user()->id;

            if ($request->hasFile('file_faktur')) {
                $file = $request->file('file_faktur');
                $file_name = time() . 'faktur' . '.' . $file->getClientOriginalExtension();
                $file->move('img/', $file_name);
                $new->file_faktur_suplier = $file_name;
            }
            // dd($new);
            // die();
            $new->save();

            $temp = Temp_BarangMasuk::where('user_id', Auth::user()->id)->get();
            foreach ($temp as $item) {
                $detail = new Detail_Penjualan();
                $detail->barangmasuk_id = $new->id;
                $detail->barang_id = $item->barang_id;
                $detail->jumlah = $item->jumlah;
                $detail->diskon = $item->diskon;
                $detail->total_harga = $item->total_harga;
                $detail->total_diskon = $item->total_diskon;
                $detail->user_id = Auth::user()->id;
                $detail->save();

                $update = Barang::find($detail->barang_id);
                $update->jumlah += $detail->jumlah;
                $update->save();
            }

            Temp_BarangMasuk::where('user_id', Auth::user()->id)->delete();
            return redirect(route('barangMasuk'))->with('success', 'Data Berhasil di simpan');
        }
    }

    public function detailBarangMasuk(Request $request)
    {
        $data['data'] = BarangMasuk::find($request->id);
        return view('back.pages.barangMasuk._modal_detail', $data);
    }

    public function printBarangMasuk($id)
    {
        // dd($id);
        $data['data'] = BarangMasuk::findOrFail($id);
        return view('back.pages.barangMasuk.print', $data);
    }

    // Penjualan Barang
    public function indexBarangKeluar()
    {

        $data['data'] = BarangKeluar::orderBy('id', 'desc')->get();
        return view('back.pages.barangKeluar.index', $data);
    }

    public function addBarangKeluar()
    {
        $date = date('dmy') . '-' . date('His') . '-';
        $awalan = 'SJ' . $date;
        $query = BarangKeluar::where('no_faktur', 'like', '%' . date('dmy') . '%')->orderBy('no_faktur', 'desc');

        $lebar = 2;
        $jumlahrecord = $query->count();
        if ($jumlahrecord == 0) {
            $nomor = 1;
        } else {
            $row = $query->get()->toArray();
            $nomor = intval(substr($row[0]['no_faktur'], strlen($awalan))) + 1;
        }

        if ($lebar > 0)
            $angka = $awalan . str_pad($nomor, $lebar, "0", STR_PAD_LEFT);
        else
            $angka = $awalan . $nomor;
        $data['kode'] = $angka;
        $data['barang'] = Barang::orderBy('nama_barang', 'asc')->get();
        return view('back.pages.barangKeluar.add', $data);
    }

    public function addTempBarangKeluar(Request $request)
    {
        // dd($request->all());

        $cek = Temp_BarangKeluar::where('user_id', Auth::user()->id)->where('barang_id', $request->barang_id)->count();
        if ($cek == 0) {
            $barang = Barang::find($request->barang_id);
            if ($barang->jumlah >= $request->jumlah) {
                $total_harga = $request->harga * $request->jumlah;
                $diskon = ($request->diskon / 100) * $total_harga;
                $total_harga = $total_harga - $diskon;
                $new =  new Temp_BarangKeluar();
                $new->barang_id = $request->barang_id;
                $new->jumlah = $request->jumlah;
                $new->diskon = $request->diskon;
                $new->total_harga = $total_harga;
                $new->total_diskon = $diskon;
                $new->user_id = Auth::user()->id;
                $new->save();

                return response()->json('oke');
            } else {
                return response()->json('sold');
            }
        } else {
            return response()->json('exist');
        }
    }

    public function renderTableBarangKeluar()
    {
        $data['barang'] = Temp_BarangKeluar::where('user_id', Auth::user()->id)->with('barang')->get();
        $data['html'] = view('back.pages.barangKeluar._tabel', $data)->render();
        return response()->json($data);
    }

    public function editBarangKeluar(Request $request)
    {
        $data['temp'] = Temp_BarangKeluar::find($request->id);
        $data['barang'] = Barang::orderBy('nama_barang', 'asc')->get();
        return view('back.pages.barangMasuk._modal_edit', $data);
    }

    public function editTempBarangKeluar(Request $request)
    {
        // dd($request->all());
        $cek = Temp_BarangKeluar::where('user_id', Auth::user()->id)->where('barang_id', $request->barang_id)->where('id', '!=', $request->id)->count();
        if ($cek == 0) {
            $barang = Barang::find($request->barang_id);
            if ($barang->jumlah >= $request->jumlah) {
                $total_harga = $request->harga * $request->jumlah;
                $diskon = ($request->diskon / 100) * $total_harga;
                $total_harga = $total_harga - $diskon;
                $new =  Temp_BarangKeluar::find($request->id);
                $new->barang_id = $request->barang_id;
                $new->jumlah = $request->jumlah;
                $new->diskon = $request->diskon;
                $new->total_harga = $total_harga;
                $new->total_diskon = $diskon;
                $new->user_id = Auth::user()->id;
                $new->save();

                return response()->json('oke');
            } else {
                return response()->json('sold');
            }
        } else {
            return response()->json('exist');
        }
    }

    public function deleteBarangKeluar(Request $request)
    {
        $delete = Temp_BarangKeluar::find($request->id)->delete();
    }

    public function storeBarangKeluar(Request $request)
    {
        // dd($request->all()); 
        $cek = BarangKeluar::where('no_faktur', $request->no_faktur)->count();
        if ($cek != 0) {
            return redirect()->back()->with('warning', 'No faktur sudah terdaftar!');
        } else {
            $new = new BarangKeluar();
            $new->no_faktur = $request->no_faktur;
            $new->tgl_faktur = $request->tgl_masuk;
            $new->total_diskon = str_replace('.', '', $request->total_diskon);
            $new->sub_total = str_replace('.', '', $request->sub_total);
            $new->pajak = $request->pajak;
            $new->grand_total = str_replace('.', '', $request->grand_total);;
            $new->user_id = Auth::user()->id;
            $new->nama_pelanggan = $request->nama_pelanggan;
            $new->uang_bayar = str_replace('.', '', $request->uang_bayar);;
            $new->uang_kembali = str_replace('.', '', $request->uang_kembali);;
            $new->save();

            $temp = Temp_BarangKeluar::where('user_id', Auth::user()->id)->get();
            foreach ($temp as $item) {
                $detail = new Detail_BarangKeluar();
                $detail->barangkeluar_id = $new->id;
                $detail->barang_id = $item->barang_id;
                $detail->jumlah = $item->jumlah;
                $detail->diskon = $item->diskon;
                $detail->total_harga = $item->total_harga;
                $detail->total_diskon = $item->total_diskon;
                $detail->user_id = Auth::user()->id;
                $detail->save();

                $update = Barang::find($detail->barang_id);
                $update->jumlah -= $detail->jumlah;
                $update->save();
            }
            if ($request->is_print == 'ya') {

                $request->session()->flash('is_print', route('barangKeluar.print', $new->id));
            }
            Temp_BarangKeluar::where('user_id', Auth::user()->id)->delete();
            return redirect(route('barangKeluar'))->with('success', 'Data Berhasil di simpan');
        }
    }

    public function detailBarangKeluar(Request $request)
    {
        $data['data'] = BarangKeluar::find($request->id);
        return view('back.pages.barangKeluar._modal_detail', $data);
    }

    public function printBarangKeluar($id)
    {
        // dd($id);
        $data['data'] = BarangKeluar::findOrFail($id);
        // return view('back.pages.barangKeluar.print', $data);
        $customPaper = array(90, 180);
        $pdf = PDF::loadview('back.pages.barangKeluar.invoice', $data)->setPaper(array(0, 0, 230, 400));
        $pdf->setOptions(['isRemoteEnabled' => true]);
        $pdf->getDomPDF()->setProtocol($_SERVER['DOCUMENT_ROOT']);
        return $pdf->stream();
    }

    public function laporanBarangMasuk(Request $request)
    {

        $pisah = explode('/', $request->tanggal);
        $tanggal_awal = $pisah[0];
        $tanggal_akhir = $pisah[1];


        if (!empty($request->suplier_id)) {
            $suplier = Suplier::find($request->suplier_id);
            $data['transaksi'] = BarangMasuk::where('suplier_id', $request->suplier_id)->whereBetween('tgl_faktur', [$tanggal_awal, $tanggal_akhir])->orderBy('tgl_faktur', 'desc')->get();
            $data['judul'] = 'Laporan Data Pembelian Barang <br> Periode ' . helper::tgl_indo($tanggal_awal) . ' s/d ' . helper::tgl_indo($tanggal_akhir) . '<br>' . ' Suplier ' . $suplier->nama;
        } else {
            $data['transaksi'] = BarangMasuk::whereBetween('tgl_faktur', [$tanggal_awal, $tanggal_akhir])->orderBy('tgl_faktur', 'desc')->get();
            $data['judul'] = 'Laporan Data Pembelian Barang <br> Periode ' . helper::tgl_indo($tanggal_awal) . ' s/d ' . helper::tgl_indo($tanggal_akhir);
        }



        return view('back.pages.barangMasuk.cetak', $data);
    }
    public function laporanBarangKeluar(Request $request)
    {
        $pisah = explode('/', $request->tanggal);
        $tanggal_awal = $pisah[0];
        $tanggal_akhir = $pisah[1];

        $data['transaksi'] = BarangKeluar::whereBetween('tgl_faktur', [$tanggal_awal, $tanggal_akhir])->orderBy('tgl_faktur', 'desc')->get();
        $data['judul'] = 'Laporan Data Penjualan Barang <br> Periode ' . helper::tgl_indo($tanggal_awal) . ' s/d ' . helper::tgl_indo($tanggal_akhir);
        // dd($data) ;

        return view('back.pages.barangKeluar.cetak', $data);
    }

    public function getKode(Request $request)
    {
        $jenis = Jenis::find($request->jenis);
        $merk = Merk::find($request->merk);

        $kode_awal = $request->kategori == 'Makanan' ? "MK" : 'MI';
        if ($kode_awal == 'MK') {
            $kode_2 = $jenis->kode;
            $kode_3 = $merk->kode;
            $kode_4 = 0;
            $kode_5 = 0;
        } else {
            $kode_2 = 0;
            $kode_3 = 0;
            $kode_4 = $jenis->kode;
            $kode_5 = $merk->kode;
        }

        $fix = $kode_awal  . $kode_2 . $kode_3 . $kode_4 . $kode_5;
        return response()->json($fix);
    }
}
