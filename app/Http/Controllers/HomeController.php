<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Detail_Booking;
use App\Hari;
use App\HUrl;
use App\Item;
use App\Jadwal;
use App\Jam;
use App\User;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function test()
    {
        $booking = Booking::where('kode', 'K22081901')->first();
        $data['booking'] = $booking;
        $email = ['ahmadwaliyudin2018@gmail.com'];
        return view('email.kode_booking', $data);
        Mail::send('email.kode_booking', $data, function ($message) use ($email) {
            $message->to($email)->subject('Pembayaran Penyewaan');
        });
    }
    public function test2()
    {
        $hari = Hari::all();
        $jam = Jam::whereNotIn('id', [1, 2, 3, 4, 5, 6, 7, 8])->get();
        $lapangan = Item::all();
        $index = 0;
        $data = [];
        foreach ($lapangan as $l) {
            foreach ($hari as $h) {
                foreach ($jam as $j) {
                    $data[$index]['item_id'] = $l->id;
                    $data[$index]['hari_id'] = $h->id;
                    $data[$index]['jam_id'] = $j->id;
                    $data[$index]['status'] = 1;
                    $data[$index]['created_at'] = date('Y-m-d H:i:s');
                    $data[$index]['updated_at'] = date('Y-m-d H:i:s');

                    if ($h->id == 1 || $h->id == 7) {
                        if ($j->id == 9 || $j->id == 10 || $j->id == 11 || $j->id == 12 || $j->id == 13 || $j->id == 14 || $j->id == 15 || $j->id == 16 || $j->id == 17 || $j->id == 18) {
                            $data[$index]['harga'] = 125000;
                        } else {
                            $data[$index]['harga'] = 150000;
                        }
                    } else {
                        if ($j->id == 9 || $j->id == 10 || $j->id == 11 || $j->id == 12 || $j->id == 13 || $j->id == 14 || $j->id == 15 || $j->id == 16 || $j->id == 17 || $j->id == 18) {
                            $data[$index]['harga'] = 90000;
                        } else {
                            $data[$index]['harga'] = 110000;
                        }
                    }

                    $index++;
                }
            }
        }
        Jadwal::insert($data);
        return response()->json($data);
    }

    public function index()
    {
        $data['lapangan'] = Item::where('category_id', 1)->take(2)->get();
        $current_date = date('H');
        $cure_hour = $current_date + 1;
        $detail_booking = Detail_Booking::where('jam_id', $cure_hour)->where('tanggal', date('Y-m-d'))->first();
        $data['cur_hour'] = Jam::where('id', $cure_hour)->first();
        if ($detail_booking != null) {
            $data['booking'] = $detail_booking->parents;
            $data['detail_booking'] = $detail_booking;
            $data['detail'] = $detail_booking->parents->detail[count($detail_booking->parents->detail) - 1];
        }
        // dd($data);
        $cek_jam = Jadwal::where('jam_id', $data['cur_hour']->id)->count();


        if ($cek_jam == 0) {
            $data['is_tutup'] = true;
        }

        return view('front.pages.home', $data);
    }

    public function login()
    {
        if (Auth::user()) {
            if (Auth::user()->role == 'member') {

                return redirect()->route('booking');
            }
        }
        return view('front.pages.login');
    }

    public function register()
    {
        if (Auth::user()) {
            if (Auth::user()->role == 'member') {
                return view('front.pages.home');
            }
        }
        return view('front.pages.register');
    }

    public function loginPost(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($credentials)) {
            $cek_url = HUrl::where('ip', request()->ip())->first();
            if ($cek_url != null) {
                $datas = json_decode($cek_url->data,true);
                
                $temp = Booking::where('user_id', Auth::user()->id)->where('status', 'on_process')->where('status_pembayaran', 'pending')->first();
                if ($temp != null) {
                    Detail_Booking::where('booking_id', $temp->id)->delete();
                    $temp->delete();
                }

                $jadwal = Jadwal::where([
                    'item_id' => $datas['item_id'],
                    'hari_id' => $datas['hari_id'],
                    'jam_id' => $datas['jam_id'],
                ])->first();

                $jam_tersedia = Jadwal::where([
                    'item_id' => $datas['item_id'],
                    'hari_id' => $datas['hari_id'],
                ])->where('jam_id', '>=', $datas['jam_id'])->groupBy('jam_id')->get();


                $data['jam_tersedia'] = $jam_tersedia;
                $data['jadwal'] = $jadwal;
                $data['tanggal'] = $datas['tanggal'];
                // dd($request->all());

                return view('front.pages.booking_form', $data);
            }

            if(auth()->user()->is_suspend == 1){
                $message = 'Akun anda ditangguhkan sampai tanggal '. \App\Helper\helper::tgl_indo_jam(auth()->user()->suspend_date) . ' Karena tidak melakukan 3x Pelunasan !';
                Auth::logout();
                return redirect()->back()->with('error', $message);
            }
            return redirect(route('booking'));
        }

        return redirect()->back()->with('error', 'Username Atau Password Salah !');
    }

    public function registerPost(Request $request)
    {
        // dd($request->all());

        $cek_email = User::where('email', $request->email)->get();
        if (count($cek_email) > 0) {
            return redirect()->back()->with('error', 'Email ' . $request->email . ' Telah terdaftar, silahkan gunakan email yang lain ');
        }

        $new = new User();
        $new->name = $request->name;
        $new->email = $request->email;
        $new->alamat = $request->alamat;
        $new->password = bcrypt($request->password);
        $new->role = 'member';
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $file_name = md5($request->kode_item) . '.' . $file->getClientOriginalExtension();
            $file->move('img/member/', $file_name);
            $new->foto = $file_name;
        }
        $new->save();
        return redirect()->route('login.home')->with('success-al', 'Pendaftaran berhasil, silahkan login di bawah ini untuk melakukan booking !');
    }

    public function jadwal(Request $request)
    {
        if(empty($request->week)){
            $week = 7;
            $_c = date('Y-m-d', strtotime("+".$week." day", strtotime(date('Y-m-d'))));
            $_d =date('Y-m-d', strtotime("-7 day", strtotime($_c)));
            $current_date = $_d;
            $end_date = $_c;
        }else{
            $week = $request->week;
            $_c = date('Y-m-d', strtotime("+".$week." day", strtotime(date('Y-m-d'))));
            $_d =date('Y-m-d', strtotime("-7 day", strtotime($_c)));
            $current_date = $_d;
            $end_date = $_c;
        }
        // dd($current_date,$end_date);
        $lapangan = Item::where('category_id', 1)->get();
        $period = new DatePeriod(
            new DateTime($current_date),
            new DateInterval('P1D'),
            new DateTime($end_date)
        );
        $hari = Hari::all();
        $data['labels'] = $week / 7;
        $data['week'] = $week;
        $data['tanggal'] = $period;
        $data['lapangan'] = $lapangan;
        $data['hari'] = $hari;
        return view('front.pages.jadwal', $data);
    }

    public function about()
    {
        return view('front.pages.tentang');
    }

    public function panduan()
    {
        return view('front.pages.panduan');
    }
}
