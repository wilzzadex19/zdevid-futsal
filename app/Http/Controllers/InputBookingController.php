<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Detail_Booking;
use App\Hari;
use App\Helper\helper;
use App\Item;
use App\Jadwal;
use App\User;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class InputBookingController extends Controller
{
    public function index()
    {
        $data['member'] = User::where('role', 'member')->orderBy('name', 'asc')->get();
        $data['lapangan'] = Item::where('category_id', 1)->get();
        return view('back.pages.input.index', $data);
    }

    function getJadwal(Request $request)
    {
        // dd($request->all());

        if ($request->item_id) {
            $hari = helper::to_hari(date('D', strtotime($request->dates)));
            $hari =  Hari::where('name', $hari)->first();
            $jadwal = Jadwal::where('item_id', $request->item_id)->where('hari_id', $hari->id)->get();
            $jam_exist = Detail_Booking::where('tanggal', $request->dates)->where('item_id', $request->item_id)->get();
            $val = [];
            foreach ($jam_exist as $item) {
                $val[] .= $item->jam_id;
            }
            $data['jam_exist'] = $val;
            $data['jadwal'] = $jadwal;
            $data['tanggal'] = $request->dates;
            $data['option_jadwal'] = view('back.pages.input._option_jadwal', $data)->render();
            $data['hari'] = $hari->name;

            return response()->json($data);
        }
    }

    public function getJam(Request $request)
    {
        // dd($request->all());
        $hari = Hari::where('name', $request->hari)->first();
        $jadwal = Jadwal::find($request->id_jadwal);

        $jam_tersedia = Jadwal::where([
            'item_id' => $request->item_id,
            'hari_id' => $hari->id,
        ])->where('jam_id', '>=', $jadwal->jam_id)->groupBy('jam_id')->get();

        $data['tanggal'] = $request->tanggal;
        $data['jadwal'] = $jadwal;
        $data['jam_tersedia'] = $jam_tersedia;
        $data['option_durasi'] = view('back.pages.input._option_durasi', $data)->render();
        $data['harga'] = $jadwal->harga;

        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
        // dd($request->all());
        $jadwal = Jadwal::find($request->jadwal_id);
        $member = User::find($request->member_id);
        $cek_admin = User::where('role', 'admin')->first()->name;
        $awalan = strtoupper($cek_admin[0]) . date('ymd');
        $booking_code = helper::kode_otomatis('booking', $awalan, 2, 'kode');
        $cek = Booking::where('user_id', $request->member_id)->where('status', 'on_process')->count();
        if ($cek == 0) {
            Booking::updateOrCreate([
                'user_id' => $request->member_id,
                'status' => 'on_process',
            ], [
                'kode' => $booking_code,
                'user_id' => $request->member_id,
                'team_name' => $request->team_name,
                'tanggal_transaksi' => date('Y-m-d'),
                'status' => 'on_process',
                'status_pembayaran' => 'pending',
                'jenis_pembayaran' => $request->jenis_pembayaran,
                'total_dp' => $request->jenis_pembayaran == 'full' ? 0 : str_replace('.', '', $request->jumlah_bayar),
                'total_harga' => str_replace('.', '', $request->total_harga),
                'no_hp' => $request->no_hp,
                'keterangan' => $request->keterangan,
            ]);
        } else {
            Booking::updateOrCreate([
                'user_id' => $request->member_id,
                'status' => 'on_process',
            ], [
                'user_id' => $request->member_id,
                'team_name' => $request->team_name,
                'tanggal_transaksi' => date('Y-m-d'),
                'status' => 'on_process',
                'status_pembayaran' => 'pending',
                'jenis_pembayaran' => $request->jenis_pembayaran,
                'total_dp' => $request->jenis_pembayaran == 'full' ? 0 : str_replace('.', '', $request->jumlah_bayar),
                'total_harga' => str_replace('.', '', $request->total_harga),
                'no_hp' => $request->no_hp,
                'keterangan' => $request->keterangan,
            ]);
        }
        $booking = Booking::where('user_id', $request->member_id)->where('status', 'on_process')->first();

        $jam_id = [];
        $no = 0;
        for ($i = 0; $i < $request->durasi; $i++) {
            $jam_id[] .= $jadwal->jam_id + $no;
            $no++;
        }

        Detail_Booking::where('booking_id', $booking->id)->delete();

        foreach ($jam_id as $val) {
            $detail = new Detail_Booking();
            $detail->booking_id = $booking->id;
            $detail->jam_id = $val;
            $detail->item_id = $request->item_id;
            $detail->hari_id = $jadwal->hari_id;
            $detail->user_id = $request->member_id;
            $detail->tanggal = $request->tanggal;
            $detail->save();
        }

        $data['booking'] = $booking;
        return view('back.pages.input.confirmation', $data);

        // if ($booking->payment_id == null) {
        //     $payment_id = $booking_code . '-' . date('his');
        //     $this->initPaymentGateway();
        //     $memberDetails = [
        //         'first_name' => $member->name,
        //         'last_name' => '',
        //         'email' => $member->email,
        //         'phone' => $booking->no_hp,
        //     ];

        //     $params = [
        //         'enable_payments' => \App\Payment::PAYMENT_CHANNELS,
        //         'transaction_details' => [
        //             'order_id' =>  $payment_id,
        //             'gross_amount' => $booking->jenis_pembayaran == 'full' ? $booking->total_harga : $booking->total_dp,
        //         ],
        //         'customer_details' => $memberDetails,
        //         'expiry' => [
        //             'start_time' => date('Y-m-d H:i:s T'),
        //             'unit' => 'hours',
        //             'duration' => 1,
        //         ]
        //     ];

        //     $snap = \Midtrans\Snap::createTransaction($params);
        //     // dd($snap);
        //     if ($snap->token) {
        //         try {
        //             $payment_exp = date('Y-m-d H:i:s', strtotime("+1 hours", strtotime(date('Y-m-d H:i:s'))));
        //             Booking::where('id', $booking->id)->update([
        //                 'payment_code' => $payment_id,
        //                 'payment_url' => $snap->redirect_url,
        //                 'payment_id' => $snap->token,
        //                 'status_pembayaran' => 'pending',
        //                 'payment_exp' => $payment_exp,
        //             ]);
        //         } catch (\Throwable $th) {
        //             dd($th);
        //         }
        //     }
        // } else {
        //     $cur = strtotime(date('Y-m-d H:i:s'));
        //     $exp = strtotime($booking->payment_exp);

        //     if ($cur > $exp) {
        //         $data['kadaluarsa'] = 'yes';
        //     }

        //     if ($booking->status_pembayaran == 'success') {
        //         $data['is_paid'] = 'yes';
        //     }
        // }

        // $booking = Booking::where('user_id', Auth::user()->id)->where('status', 'on_process')->first();

        // if ($booking->payment_url != null) {
        //     $data['is_url'] = 'yes';
        //     $data['booking'] = $booking;
        //     dd($data);
        //     // return view('front.pages.booking_confirmation', $data);
        // }
    }

    public function proses($id)
    {
        
        $data = Booking::findOrFail($id);
        if($data->jenis_pembayaran == 'full'){
            $data->is_lunas = 1;
        }
        $data->status_pembayaran = 'success';
        $data->status = 'filled';
        $data->save();

        $datas['booking'] = $data;
        $email = [$data->users->email];
        $datas['is_admin'] = true;
        // dd($data)
        // return view('email.kode_booking',$datas);
        Mail::send('email.kode_booking', $datas, function ($message) use ($email) {
            $message->to($email)->subject('Selesai Pembayaran');
        });

        return redirect()->route('input')->with('success','Proses Input Booking telah selesai');
    }

    public function cekStatusPembayaran(Request $request)
    {
        $item = Booking::find($request->id);
        $curl = curl_init();
        $data['status'] = 0;
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.sandbox.midtrans.com/v2/' . $item->payment_code . '/status',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => array('username' => 'admin', 'password' => 'admin123', 'lowongan_kode' => 'tst'),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic U0ItTWlkLXNlcnZlci0xaGtlUFcweElROWNCNWdNZThveE1uUng6'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $res = json_decode($response, true);

        if (!empty($res['order_id'])) {
            // dump($res);
            $booking = Booking::where('payment_code', $item->payment_code)->first();
            if ($res['transaction_status'] == 'settlement') {
                $data['status'] = 1;
                if($booking->jenis_pembayaran == 'full'){
                    $booking->is_lunas = '1';
                }
                $booking->status_pembayaran = 'success';
                $booking->status = 'filled';
                $booking->save();

                $datas['booking'] = $booking;
                $email = [$booking->users->email];
                $datas['is_admin'] = true;
                // dd($data)
                // return view('email.kode_booking',$data);
                Mail::send('email.kode_booking', $datas, function ($message) use ($email) {
                    $message->to($email)->subject('Selesai Pembayaran');
                });
            } else {
                $data['status'] = 0;
            }
        } else {
            $data['status'] = 0;
        }

        return response()->json($data);
    }
}
