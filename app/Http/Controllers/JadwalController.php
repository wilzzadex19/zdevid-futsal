<?php

namespace App\Http\Controllers;

use App\Hari;
use App\Item;
use App\Jadwal;
use App\Jam;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    public function index()
    {
        $data['jadwal'] = Jadwal::orderBy('item_id')
            ->orderBy('hari_id')
            ->orderBy('jam_id')
            ->get();
        return view('back.pages.jadwal.index',$data);
    }

    public function add()
    {
        $data['lapangan'] = Item::where('category_id', 1)->orderBy('kode', 'asc')->get();
        $data['hari'] = Hari::all();
        $data['jam'] = Jam::all();

        return view('back.pages.jadwal.add', $data);
    }

    public function store(Request $request)
    {
        $jadwal = new Jadwal();
        $jadwal->item_id = $request->item_id;
        $jadwal->hari_id = $request->hari_id;
        $jadwal->jam_id = $request->jam_id;
        $jadwal->status = 1;
        $jadwal->desc = $request->deskripsi;
        $jadwal->harga = $request->harga;
        $jadwal->save();

        return redirect(route('jadwal'))->with('success', 'Data berhasil di simpan');
    }

    public function cekJam(Request $request)
    {
        $data['jam'] = Jam::all(); 
        $cek = Jadwal::where('item_id',$request->item_id)->where('hari_id',$request->hari_id)->get();
        if(count($cek) > 0){
            $data['terisi'] = $cek;
        }else{
            $data['terisi'] = 0;
        }
        return response()->json($data);

    }

    public function cekJamEdit(Request $request)
    {
        // dd($request->all());
        $data['jam'] = Jam::all(); 
        $cek = Jadwal::where('id','!=',$request->jadwal_id)->where('item_id',$request->item_id)->where('hari_id',$request->hari_id)->get();
        if(count($cek) > 0){
            $data['terisi'] = $cek;
        }else{
            $data['terisi'] = 0;
        }
        return response()->json($data);

    }

    public function edit($id)
    {
        $data['jadwal'] = Jadwal::findOrFail($id);
        $data['lapangan'] = Item::where('category_id', 1)->orderBy('kode', 'asc')->get();
        $data['hari'] = Hari::all();
        $data['jam'] = Jam::all();
        return view('back.pages.jadwal.edit',$data);
    }

    public function update(Request $request,$id){
        $jadwal = Jadwal::findOrFail($id);
        $jadwal->item_id = $request->item_id;
        $jadwal->hari_id = $request->hari_id;
        $jadwal->jam_id = $request->jam_id;
        // $jadwal->status = $request->status;
        $jadwal->desc = $request->deskripsi;
        $jadwal->harga = $request->harga;
        $jadwal->save();

        return redirect()->back()->with('success', 'Data berhasil di simpan');
    }

    public function destroy($id)
    {
        $jadwal = Jadwal::findOrFail($id);
        $jadwal->delete();

        return redirect()->back()->with('success','Data berhasil di hapus');
    }


}
