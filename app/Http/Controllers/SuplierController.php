<?php

namespace App\Http\Controllers;

use App\BarangMasuk;
use App\Detail_Penjualan;
use App\Suplier;
use Illuminate\Http\Request;

class SuplierController extends Controller
{
    public function index()
    {

        $data['suplier'] = Suplier::orderBy('id', 'ASC')->get();
        return view('back.pages.suplier.index', $data);
    }

    public function add()
    {
        return view('back.pages.suplier.add');
    }

    public function store(Request $request)
    {
        $suplier = new Suplier();
        $suplier->nama = $request->nama;
        $suplier->no_telpon = $request->no_telpon;
        $suplier->alamat = $request->alamat;
        $suplier->save();

        return redirect(route('suplier'))->with('success', 'Data suplier berhasil di simpan !');
    }

    public function destroy(Request $request)
    {
        $barang = Suplier::find($request->id);
        $cek = BarangMasuk::where('suplier_id',$barang->id)->count();
        if ($cek == 0) {
            $barang->delete();
            return response()->json('oke');
        } else {
            return response()->json('no');
        }
    }

    public function edit($id)
    {
        $barang = Suplier::find($id);
        $data['suplier'] = $barang;
        return view('back.pages.suplier.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $suplier = Suplier::findOrFail($id);
        $suplier->nama = $request->nama;
        $suplier->no_telpon = $request->no_telpon;
        $suplier->alamat = $request->alamat;
        $suplier->save();

        return redirect(route('suplier'))->with('success', 'Data barang berhasil di ubah !');
    }
}
