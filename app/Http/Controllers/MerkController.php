<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Jenis;
use App\Merk;
use Illuminate\Http\Request;

class MerkController extends Controller
{
    public function index()
    {
        // $data['jenis'] = Jenis::all();
        $data['merk'] = Merk::all();
        return view('back.pages.merk.index',$data);
    }

    public function add()
    {
        return view('back.pages.merk.add');
    }

    public function getJenis(Request $request)
    {
        $jenis =  Jenis::where('kategori',$request->kategori)->get();
        $data['jenis'] = $jenis;
        return view('back.pages.merk._option_jenis',$data);
    }

    public function getJenisEdit(Request $request)
    {
        $jenis =  Jenis::where('kategori',$request->kategori)->get();
        $data['jenis'] = $jenis;
        $data['selected'] = $request->selected;
        return view('back.pages.merk._option_jenis_edit',$data);
    }

    public function getKode(Request $request)
    {
        // $jenis = Jenis::find($request->jenis);
        $lastKode = Merk::where('jenis_id',$request->jenis)->where('kategori',$request->kategori)->count();
        if($lastKode == 0){
            $lastKode = 1;
        }else {
            $lastKode = $lastKode + 1;
        }

        if(empty($request->jenis)){
            $lastKode = '';
        }
        return response()->json($lastKode);
    }

    public function getMerk(Request $request)
    {
      
        $merk = Merk::where('jenis_id',$request->jenis)->get();
        $data['merk'] = $merk;
        return view('back.pages.merk._option_merk',$data);
    }
    public function getMerkEdit(Request $request)
    {
      
        $merk = Merk::where('jenis_id',$request->jenis)->get();
        $data['merk'] = $merk;
        $data['selected'] = $request->selected;
        return view('back.pages.merk._option_merk_edit',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required|unique:m_merk,nama',
        ]);
        $merk = new Merk();
        $merk->kode = $request->kode;
        $merk->kategori = $request->kategori;
        $merk->jenis_id = $request->jenis_id;
        $merk->nama = $request->nama;
        $merk->save();

        return redirect(route('merk'))->with('success','Data berhasil di simpan');

    }

    public function destroy(Request $request)
    {
        $barang = Merk::find($request->id);
        $cek = Barang::where('merk_id', $barang->id)->count();
        if ($cek == 0) {
            $barang->delete();
            return response()->json('oke');
        } else {
            return response()->json('no');
        }
    }

    public function edit($id)
    {
        $merk = Merk::find($id);
        if($merk->kategori == 'Makanan'){
            $jenis = Jenis::where('kategori','Makanan')->get();
        }else{
            $jenis= Jenis::where('kategori','Minuman')->get();
        }

        $data['merk'] = $merk;
        $data['jenis'] = $jenis;
        return view('back.pages.merk.edit',$data);
    }

    public function update($id, Request $request)
    {
        $merk = Merk::find($id);
        $this->validate($request, [
            'nama' => 'required|unique:m_merk,nama,'.$id,
        ]);
        $merk->kode = $request->kode;
        $merk->kategori = $request->kategori;
        $merk->jenis_id = $request->jenis_id;
        $merk->nama = $request->nama;
        $merk->save();

        return redirect(route('merk'))->with('success','Data berhasil di update');
    }
}
