<?php

namespace App\Http\Controllers;

use App\Booking;
use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->prosesSelesai();
        $this->suspend_akun();
        $pending_payment = Booking::where('status_pembayaran', 'pending')->get();
        foreach ($pending_payment as $item) {

            $cur_date = strtotime(date('Y-m-d H:i:s'));
            $exp_date = strtotime($item->payment_exp);

            if ($exp_date < $cur_date) {
                $u = Booking::where('id', $item->id)->first();
                if ($u != null) {
                    $u->status_pembayaran = 'unfinish';
                    $u->status = 'filled';
                    $u->kode = '-';
                    $u->save();
                }
            }


            // return view('email.kode_booking',$data);


            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.sandbox.midtrans.com/v2/' . $item->payment_code . '/status',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_POSTFIELDS => array('username' => 'admin', 'password' => 'admin123', 'lowongan_kode' => 'tst'),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Basic U0ItTWlkLXNlcnZlci0xaGtlUFcweElROWNCNWdNZThveE1uUng6'
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $res = json_decode($response, true);
            // dump($res);
            if (!empty($res['order_id'])) {
                // dump($res);
                $booking = Booking::where('payment_code', $item->payment_code)->first();
                if ($res['transaction_status'] == 'settlement') {
                    $booking->status_pembayaran = 'success';
                    $booking->status = 'filled';
                    $booking->payment_success = $res['settlement_time'];
                    $booking->payment_url = null;
                    // $booking->payment_id = null;
                    $booking->payment_code = null;
                    $booking->tanggal_bayar = date('Y-m-d H:i:s');
                    $booking->is_edit = 1;
                    $booking->save();
                    $data['booking'] = $booking;
                    $email = [$item->users->email];
                    // return view('email.kode_booking');
                    Mail::send('email.kode_booking', $data, function ($message) use ($email) {
                        $message->to($email)->subject('Pembayaran Penyewaan');
                    });
                }
            }
        }
    }

    protected function initPaymentGateway()
    {
        // Set your Merchant Server K
        \Midtrans\Config::$serverKey = 'SB-Mid-server-1hkePW0xIQ9cB5gMe8oxMnRx';
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;
    }

    public function suspend_akun(){
        $akun_suspen = User::where('is_suspend',1)->get();
        foreach($akun_suspen as $as){
            $cur_date = strtotime(date('Y-m-d H:i:s'));
            $suspend_date = strtotime($as->suspend_date);
            
            if($cur_date > $suspend_date){
                $user = User::find($as->id);
                $user->is_suspend = null;
                $user->suspend_date = null;
                $user->save();
            }
        }
    }

    public function prosesSelesai()
    {
        $data = Booking::where('status_pembayaran', 'success')->get();
        foreach ($data as $item) {
            $tanggal_akhir_sewa = date('Y-m-d H:i:s', strtotime($item->detail[0]->tanggal . ' ' . $item->detail[count($item->detail) - 1]->jam->jam_akhir));
            $tanggal_akhir_sewa_fix = strtotime($tanggal_akhir_sewa);
            $cur_time = time();



            if ($tanggal_akhir_sewa_fix <= $cur_time) {

                $update = Booking::find($item->id);
                if($update->jenis_pembayaran == 'dp'){
                    if($update->is_lunas == null){
                        $keterangan = 'Tidak Lunas';
                    }else{
                        $keterangan = 'Lunas';
                    }
                }else{
                    $keterangan = 'Lunas';
                }
                $update->keterangan_lunas = $keterangan;
                $update->status_pembayaran = 'selesai';
                $update->save();
                // dump($item->kode,'sudah lewat');
                $get_user_id = $item->user_id;
                $cek_suspend = Booking::where('user_id', $get_user_id)->where('jenis_pembayaran', 'dp')->where('is_lunas', null)->get();
                if (count($cek_suspend) >= 3) {
                    $_3weeks =  strtotime("+1 day", strtotime(date('Y-m-d H:i:s')));
                    $u_update = User::find($get_user_id);
                    $u_update->is_suspend = 1;
                    $u_update->suspend_date = date('Y-m-d H:i:s',$_3weeks);
                    $u_update->save();
                }
            }
        }
    }
}
