<?php

namespace App\Http\Controllers;

use App\BarangKeluar;
use App\BarangMasuk;
use App\Booking;
use App\Detail_Booking;
use App\Hari;
use App\Helper\helper;
use App\HUrl;
use App\Item;
use App\Jadwal;
use App\User;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class BookingController extends Controller
{
    public function index(Request $request)
    {
        $cek_suspend = Booking::where('user_id', auth()->user()->id)->where('jenis_pembayaran', 'dp')->where('is_lunas', null)->get();
        if (count($cek_suspend) >= 2) {
            Session::flash('message', 'Successfully updated!');
        }

        if(empty($request->week)){
            $week = 7;
            $_c = date('Y-m-d', strtotime("+".$week." day", strtotime(date('Y-m-d'))));
            $_d =date('Y-m-d', strtotime("-7 day", strtotime($_c)));
            $current_date = $_d;
            $end_date = $_c;
        }else{
            $week = $request->week;
            $_c = date('Y-m-d', strtotime("+".$week." day", strtotime(date('Y-m-d'))));
            $_d =date('Y-m-d', strtotime("-7 day", strtotime($_c)));
            $current_date = $_d;
            $end_date = $_c;
        }


        $lapangan = Item::where('category_id', 1)->get();
        // $current_date = date('Y-m-d');
        // $end_date = date('Y-m-d', strtotime("+7 day", strtotime(date('Y-m-d'))));
        $period = new DatePeriod(
            new DateTime($current_date),
            new DateInterval('P1D'),
            new DateTime($end_date)
        );
        $hari = Hari::all();
        $data['labels'] = $week / 7;
        $data['week'] = $week;
        $data['tanggal'] = $period;
        $data['lapangan'] = $lapangan;
        $data['hari'] = $hari;
        return view('front.pages.booking_index', $data);
    }

    public function jadwalAdmin(Request $request)
    {
        if(empty($request->week)){
            $week = 7;
            $_c = date('Y-m-d', strtotime("+".$week." day", strtotime(date('Y-m-d'))));
            $_d =date('Y-m-d', strtotime("-7 day", strtotime($_c)));
            $current_date = $_d;
            $end_date = $_c;
        }else{
            $week = $request->week;
            $_c = date('Y-m-d', strtotime("+".$week." day", strtotime(date('Y-m-d'))));
            $_d =date('Y-m-d', strtotime("-7 day", strtotime($_c)));
            $current_date = $_d;
            $end_date = $_c;
        }
        $lapangan = Item::where('category_id', 1)->get();
        // $current_date = date('Y-m-d');
        // $end_date = date('Y-m-d', strtotime("+7 day", strtotime(date('Y-m-d'))));
        $period = new DatePeriod(
            new DateTime($current_date),
            new DateInterval('P1D'),
            new DateTime($end_date)
        );
        $hari = Hari::all();
        $data['labels'] = $week / 7;
        $data['week'] = $week;
        $data['tanggal'] = $period;
        $data['lapangan'] = $lapangan;
        $data['hari'] = $hari;
        return view('back.pages.dashboard.jadwal', $data);
    }

    public function getInfo(Request $request)
    {
        $jadwal = Jadwal::where([
            'item_id' => $request->item_id,
            'hari_id' => $request->hari_id,
            'jam_id' => $request->jam_id,
        ])->first();
        $data['jadwal'] = $jadwal;
        $data['tanggal'] = $request->tanggal;
        $data['info'] = $request->info;
        if (!empty($request->info_booking)) {
            $data['info_booking'] = true;
        }
        if (!empty($request->is_edit)) {
            $data['edit_id'] = $request->is_edit;
        }
        // dd($data);
        // dd($data);
        return view('front.pages._modal_info', $data);
    }

    public function saveHistory(Request $request)
    {
        // dd($request->all(),request()->ip());
        $data = $request->except('_token');
        $new = HUrl::updateOrCreate([
            'ip' => $request->ip(),
        ], [
            'ip' => $request->ip(),
            'data' => json_encode($data),
        ]);
    }

    public function form(Request $request)
    {
        // $cek_suspend = Booking::where('user_id',auth()->user()->id)->where('jenis_pembayaran','dp')->where('is_lunas',null)->get();
        // if(count($cek_suspend) >= 2){
        //     $data['is_alert'] = true;
        // }
        // Session::flash('message', 'Successfully updated!');
        $temp = Booking::where('user_id', Auth::user()->id)->where('status', 'on_process')->where('status_pembayaran', 'pending')->first();
        if ($temp != null) {
            Detail_Booking::where('booking_id', $temp->id)->delete();
            $temp->delete();
        }

        $jadwal = Jadwal::where([
            'item_id' => $request->item_id,
            'hari_id' => $request->hari_id,
            'jam_id' => $request->jam_id,
        ])->first();

        $jam_tersedia = Jadwal::where([
            'item_id' => $request->item_id,
            'hari_id' => $request->hari_id,
        ])->where('jam_id', '>=', $request->jam_id)->groupBy('jam_id')->get();


        $data['jam_tersedia'] = $jam_tersedia;
        $data['jadwal'] = $jadwal;
        $data['tanggal'] = $request->tanggal;
        // dd($request->all());

        return view('front.pages.booking_form', $data);
    }

    // public function 

    public function formEdit(Request $request)
    {


        $jadwal = Jadwal::where([
            'item_id' => $request->item_id,
            'hari_id' => $request->hari_id,
            'jam_id' => $request->jam_id,
        ])->first();

        $jam_tersedia = Jadwal::where([
            'item_id' => $request->item_id,
            'hari_id' => $request->hari_id,
        ])->where('jam_id', '>=', $request->jam_id)->groupBy('jam_id')->get();


        $data['jam_tersedia'] = $jam_tersedia;
        $data['jadwal'] = $jadwal;
        $data['tanggal'] = $request->tanggal;

        $data['booking'] = Booking::find($request->edit_id);

        return view('front.pages.booking_form_edit', $data);
    }

    public function formAdd(Request $request)
    {

        $delete_hurl = HUrl::where('ip', request()->ip())->delete();
        $cek_admin = User::where('role', 'admin')->first()->name;

        $awalan = strtoupper($cek_admin[0]) . date('ymd');
        $booking_code = helper::kode_otomatis('booking', $awalan, 2, 'kode');
        // dd($booking_code);
        $cek = Booking::where('user_id', Auth::user()->id)->where('status', 'on_process')->count();
        if ($cek == 0) {
            Booking::updateOrCreate([
                'user_id' => Auth::user()->id,
                'status' => 'on_process',
            ], [
                'kode' => $booking_code,
                'user_id' => Auth::user()->id,
                'team_name' => $request->team_name,
                'tanggal_transaksi' => date('Y-m-d'),
                'status' => 'on_process',
                'status_pembayaran' => 'pending',
                'jenis_pembayaran' => $request->jenis_pembayaran,
                'total_dp' => $request->jenis_pembayaran == 'full' ? 0 : str_replace('.', '', $request->jumlah_bayar),
                'total_harga' => str_replace('.', '', $request->total_harga),
                'no_hp' => $request->no_hp,
                'keterangan' => $request->keterangan,
            ]);
        } else {
            Booking::updateOrCreate([
                'user_id' => Auth::user()->id,
                'status' => 'on_process',
            ], [
                'user_id' => Auth::user()->id,
                'team_name' => $request->team_name,
                'tanggal_transaksi' => date('Y-m-d'),
                'status' => 'on_process',
                'status_pembayaran' => 'pending',
                'jenis_pembayaran' => $request->jenis_pembayaran,
                'total_dp' => $request->jenis_pembayaran == 'full' ? 0 : str_replace('.', '', $request->jumlah_bayar),
                'total_harga' => str_replace('.', '', $request->total_harga),
                'no_hp' => $request->no_hp,
                'keterangan' => $request->keterangan,
            ]);
        }
        $booking = Booking::where('user_id', Auth::user()->id)->where('status', 'on_process')->first();

        $jam_id = [];
        $no = 0;
        for ($i = 0; $i < $request->durasi; $i++) {
            $jam_id[] .= $request->jam_id + $no;
            $no++;
        }

        Detail_Booking::where('booking_id', $booking->id)->delete();

        foreach ($jam_id as $val) {
            $detail = new Detail_Booking();
            $detail->booking_id = $booking->id;
            $detail->jam_id = $val;
            $detail->item_id = $request->item_id;
            $detail->hari_id = $request->hari_id;
            $detail->user_id = Auth::user()->id;
            $detail->tanggal = $request->tanggal;
            $detail->save();
        }

        if ($booking->payment_id == null) {
            $payment_id = $booking_code . '-' . date('his');
            $this->initPaymentGateway();
            $memberDetails = [
                'first_name' => Auth::user()->name,
                'email' => Auth::user()->email,
                'phone' => $booking->no_hp,
            ];

            $params = [
                'enable_payments' => \App\Payment::PAYMENT_CHANNELS,
                'transaction_details' => [
                    'order_id' =>  $payment_id,
                    'gross_amount' => $booking->jenis_pembayaran == 'full' ? $booking->total_harga : $booking->total_dp,
                ],
                'customer_details' => $memberDetails,
                'expiry' => [
                    'start_time' => date('Y-m-d H:i:s T'),
                    'unit' => 'hours',
                    'duration' => 1,
                ]
            ];

            $snap = \Midtrans\Snap::createTransaction($params);
            // dd($snap);
            if ($snap->token) {
                try {
                    $payment_exp = date('Y-m-d H:i:s', strtotime("+1 hours", strtotime(date('Y-m-d H:i:s'))));
                    Booking::where('id', $booking->id)->update([
                        'payment_code' => $payment_id,
                        'payment_url' => $snap->redirect_url,
                        'payment_id' => $snap->token,
                        'status_pembayaran' => 'pending',
                        'payment_exp' => $payment_exp,
                    ]);
                } catch (\Throwable $th) {
                    dd($th);
                }
            }
        } else {
            $cur = strtotime(date('Y-m-d H:i:s'));
            $exp = strtotime($booking->payment_exp);

            if ($cur > $exp) {
                $data['kadaluarsa'] = 'yes';
            }

            if ($booking->status_pembayaran == 'success') {
                $data['is_paid'] = 'yes';
            }
        }

        $booking = Booking::where('user_id', Auth::user()->id)->where('status', 'on_process')->first();
        // dd($booking);

        if ($booking->payment_url != null) {
            $data['is_url'] = 'yes';
            $data['booking'] = $booking;
            return view('front.pages.booking_confirmation', $data);
        }
    }
    public function formUpdate(Request $request)
    {
        // dd($request->all());
        $booking = Booking::find($request->edit_id);
        $detail_history = $booking->detail;
        $sisa_bayar = str_replace('.', '', $request->sisa_pembayaran);
        $uang_hangus = str_replace('.', '', $request->hangus);
        $total_sewa_baru = str_replace('.', '', $request->total_harga);
        $total_harga_sebelumnya = str_replace('.', '', $request->total_harga_sebelumnya);
        $book_update = Booking::findOrFail($request->edit_id);
        // dd($sisa_bayar);
        if ($booking->jenis_pembayaran == 'full') {
            if ($sisa_bayar > 0) {
                $total_dp_baru = $total_harga_sebelumnya;
                $total_harga_baru = $total_sewa_baru;
                $book_update->jenis_pembayaran = 'dp';
                $is_lebih = 0;
            } else {
                // dd('ss');
                $book_update->jenis_pembayaran = 'full';
                $total_dp_baru = 0;
                $total_harga_baru = $total_sewa_baru;
                $is_lebih = 0;
            }
        } else {
            if ((int) $uang_hangus > 0) {
                $book_update->jenis_pembayaran = 'full';
                $total_dp_baru = 0;
                $total_harga_baru = $total_harga_sebelumnya;
                $is_lebih = 0;
            } else {
                $total_dp_baru = $total_harga_sebelumnya;
                $total_harga_baru = $total_sewa_baru;
                $is_lebih = 0;
            }
        }

        // dd($total_harga_baru);


        // if ((int) $uang_hangus > 0) {
        //     $book_update->jenis_pembayaran = 'full';
        //     $total_dp_baru = 0;
        //     $total_harga_baru = $total_harga_sebelumnya;
        //     $is_lebih = 0;
        // }

        // dd($total_harga_baru);

        if ((int) $sisa_bayar > 0) {
            $book_update->jenis_pembayaran = 'dp';
            $total_dp_baru = $total_harga_sebelumnya;
            $total_harga_baru = $total_sewa_baru;
            $is_lebih = 0;
        }

        $book_update->team_name = $request->team_name;
        $book_update->tanggal_transaksi = date('Y-m-d');
        $book_update->total_dp = $total_dp_baru;
        $book_update->total_harga = $total_harga_baru;
        $book_update->no_hp = $request->no_hp;
        $book_update->keterangan = $request->keterangan;
        $book_update->is_lebih = $is_lebih;
        $book_update->is_edit = 2;
        $book_update->total_dp_sebelumnya = $total_harga_sebelumnya;
        $book_update->total_harga_sebelumnya = $total_harga_sebelumnya;
        $book_update->sisa_bayar = $sisa_bayar;
        $book_update->uang_hangus = $uang_hangus;



        // dd($book_update);
        $book_update->save();

            // Booking::where('id',$request->edit_id)->update(
            //      [

            //     'user_id' => Auth::user()->id,
            //     'team_name' => $request->team_name,
            //     'tanggal_transaksi' => date('Y-m-d'),
            //     // 'status' => 'on_process',
            //     // 'status_pembayaran' => 'pending',
            //     // 'jenis_pembayaran' => $request->jenis_pembayaran,
            //     // 'total_dp' => $request->jenis_pembayaran == 'full' ? 0 : str_replace('.', '', $request->jumlah_bayar),
            //     'total_harga' => str_replace('.', '', $request->total_harga),

            // ]);

        // $booking = Booking::find($request->edit_id);

        $jam_id = [];
        $no = 0;
        for ($i = 0; $i < $request->durasi; $i++) {
            $jam_id[] .= $request->jam_id + $no;
            $no++;
        }

        // dd($jam_id);

        $deletess = Detail_Booking::where('booking_id', $request->edit_id)->delete();

        foreach ($jam_id as $val) {
            $detail = new Detail_Booking();
            $detail->booking_id = $request->edit_id;
            $detail->jam_id = $val;
            $detail->item_id = $request->item_id;
            $detail->hari_id = $request->hari_id;
            $detail->user_id = Auth::user()->id;
            $detail->tanggal = $request->tanggal;

            $detail->save();
        }


        $datas['booking'] = $book_update;
        $datas['detail_histori'] = $detail_history;
        $email = [$book_update->users->email];
        // $datas['is_admin'] = true;
        // dd($data)
        // return view('email.kode_booking_histori',$datas);
        Mail::send('email.kode_booking_histori', $datas, function ($message) use ($email) {
            $message->to($email)->subject('Pergantian Jadwal');
        });

        // die();

        return redirect()->route('histori')->with('success', 'Berhasil melakukan pergantian jadwal');
    }

    public function checkout()
    {
        $this->initPaymentGateway();

        $memberDetails = [
            'first_name' => 'Willy',
            'last_name' => '',
            'email' => 'ahmadwaliyudin@gmail.com',
            'phone' => '08964324532',
        ];

        $params = [
            'enable_payments' => \App\Payment::PAYMENT_CHANNELS,
            'transaction_details' => [
                'order_id' => 'INV/1111/TES4',
                'gross_amount' => 250000,
            ],
            'customer_details' => $memberDetails,
            'expiry' => [
                'start_time' => date('Y-m-d H:i:s T'),
                'unit' => 'minutes',
                'duration' => 1,
            ]
        ];

        $snap = \Midtrans\Snap::createTransaction($params);
        dd($snap);
    }

    public function pay(Request $request)
    {
        if ($request->payment_url) {
            return redirect($request->payment_url);
        }
    }

    public function histori()
    {
        $booking = Booking::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
        $data['booking'] = $booking;
        // dd($data);
        return view('front.pages.histori_booking', $data);
    }

    public function historiPost(Request $request)
    {

        // dd(Auth::user());
        $get_user = Booking::where('payment_id', $request->id)->first();
        // dd($get_user);
        $booking = Booking::where('user_id', Auth::user()->id)->orderBy('kode', 'desc')->get();
        $data['booking'] = $booking;
        // dd($data);
        return view('front.pages.histori_booking', $data);
    }

    public function dataBooking($status)
    {
        $booking = Booking::where('status_pembayaran', $status)->orderBy('kode', 'desc')->get();
        $data['booking'] = $booking;
        $data['status'] = $status;
        return view('back.pages.booking.index', $data);
    }

    public function dataBookingLunasi(Request $request)
    {
        $data['booking'] = Booking::find($request->id);
        return view('back.pages.booking._modal_pembayaran', $data);
    }

    public function dataBookingVirtual($id)
    {
        $booking = Booking::findOrFail($id);
        if ($booking->is_lunas == null) {
            // if ($booking->payment_url == null) {
            if ($booking->jenis_pembayaran == 'full') {
                $amount = $booking->total_harga;
            } else {
                $amount = $booking->total_harga - $booking->total_dp;
            }
            $payment_id = $booking->kode . '-' . date('his');
            $this->initPaymentGateway();
            $memberDetails = [
                'first_name' => $booking->users->name,
                'last_name' => '',
                'email' => $booking->users->email,
                'phone' => $booking->no_hp,
            ];

            $params = [
                'enable_payments' => \App\Payment::PAYMENT_CHANNELS,
                'transaction_details' => [
                    'order_id' =>  $payment_id,
                    'gross_amount' => $amount,
                ],
                'customer_details' => $memberDetails,
                'expiry' => [
                    'start_time' => date('Y-m-d H:i:s T'),
                    'unit' => 'days',
                    'duration' => 1,
                ],
                "custom_field1" => "custom field 1 content",
            ];

            $snap = \Midtrans\Snap::createTransaction($params);
            if ($snap->token) {
                $update = Booking::where('id', $booking->id)->update([
                    'payment_code' => $payment_id,
                    'payment_url' => $snap->redirect_url,
                    'payment_id' => $snap->token,
                ]);

                return redirect($snap->redirect_url);
            }
            // } else {
            //     $booking->is_lunas = 1;
            //     $booking->save();
            //     return redirect($booking->payment_url);
            // }
        } else {
            return redirect()->back()->with('warning', 'Pembayaran Sudah dilunasi !');
        }
    }


    public function cekStatusPembayaran(Request $request)
    {
        $item = Booking::find($request->id);
        $curl = curl_init();
        $data['status'] = 0;
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.sandbox.midtrans.com/v2/' . $item->payment_code . '/status',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => array('username' => 'admin', 'password' => 'admin123', 'lowongan_kode' => 'tst'),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic U0ItTWlkLXNlcnZlci0xaGtlUFcweElROWNCNWdNZThveE1uUng6'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $res = json_decode($response, true);
        // dd($res);

        if (!empty($res['order_id'])) {
            // dump($res);
            $booking = Booking::where('payment_code', $item->payment_code)->first();
            if ($res['transaction_status'] == 'settlement') {
                $data['status'] = 1;
                $booking->is_lunas = '1';
                $booking->save();

                $datas['booking'] = $booking;
                $email = [$booking->users->email];
                // $datas['is_admin'] = true;
                // dd($data)
                // return view('email.kode_booking',$data);
                Mail::send('email.kode_booking_lunasi', $datas, function ($message) use ($email) {
                    $message->to($email)->subject('Pelunasan Pembayaran');
                });
            } else {
                $data['status'] = 0;
            }
        } else {
            $data['status'] = 0;
        }

        return response()->json($data);
    }

    public function dataBookingProses($id)
    {

        // dd('tes');
        $data = Booking::findOrFail($id);
        $data->is_lunas = 1;
        $data->save();

        $datas['booking'] = $data;
        $email = [$data->users->email];
        // $datas['is_admin'] = true;
        // dd($data);
        // return view('email.kode_booking_lunasi',$datas);
        Mail::send('email.kode_booking_lunasi', $datas, function ($message) use ($email) {
            $message->to($email)->subject('Pelunasan Pembayaran');
        });

        // die();

        return redirect()->back()->with('success', 'Pembayaran Selesai !');
    }
    public function dataBookingSiap($id)
    {
        $data = Booking::findOrFail($id);
        $data->status_pembayaran = 'siap-digunakan';
        $data->save();

        return redirect()->back()->with('success', 'Penyewa sudah bisa menggunakan lapangan sesuai jadwal');
    }
    public function dataBookingSelesaikan($id)
    {
        $data = Booking::findOrFail($id);
        $data->status_pembayaran = 'selesai';
        $data->save();

        return redirect()->back()->with('success', 'Transaksi selesai');
    }

    public function dataBookingDetail(Request $request)
    {
        $booking = Booking::find($request->id);
        $data['booking'] = $booking;

        if (Auth::user()->role != 'member') {
            return view('back.pages.booking._modal_detail', $data);
        } else {
            return view('front.pages._modal_detail_booking', $data);
        }
    }

    public function print($id)
    {
        $booking = Booking::find($id);
        $data['booking'] = $booking;

        return view('front.pages.print_bukti', $data);
    }

    // EDIT JADWAL
    public function editJadwalIndex()
    {
        $data['booking'] = Booking::where('user_id', auth()->user()->id)->where('is_edit', 1)->where('status_pembayaran', 'success')->get();
        return view('front.pages.edit', $data);
    }

    public function editJadwalDetail($id,Request $request)
    {
        // dd($id);
        if(empty($request->week)){
            $week = 7;
            $_c = date('Y-m-d', strtotime("+".$week." day", strtotime(date('Y-m-d'))));
            $_d =date('Y-m-d', strtotime("-7 day", strtotime($_c)));
            $current_date = $_d;
            $end_date = $_c;
        }else{
            $week = $request->week;
            $_c = date('Y-m-d', strtotime("+".$week." day", strtotime(date('Y-m-d'))));
            $_d =date('Y-m-d', strtotime("-7 day", strtotime($_c)));
            $current_date = $_d;
            $end_date = $_c;
        }
        $lapangan = Item::where('category_id', 1)->get();
        // $current_date = date('Y-m-d');
        // $end_date = date('Y-m-d', strtotime("+7 day", strtotime(date('Y-m-d'))));
        $period = new DatePeriod(
            new DateTime($current_date),
            new DateInterval('P1D'),
            new DateTime($end_date)
        );
        $hari = Hari::all();
        $data['labels'] = $week / 7;
        $data['week'] = $week;
        $data['tanggal'] = $period;
        $data['lapangan'] = $lapangan;
        $data['hari'] = $hari;
        $data['is_edit'] = $id;
        // dd($data);
        return view('front.pages.jadwal_edit', $data);
    }

    public function cetakLaporanIndex()
    {
        return view('back.pages.booking.cetak');
    }

    public function cetakLaporanProses(Request $request)
    {
        // dd($request->all());
        $pisah = explode('/', $request->tanggal);
        $tanggal_awal = $pisah[0];
        $tanggal_akhir = $pisah[1];

        $total_sewa_lapang_selesai_dp = Booking::whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->whereIn('status_pembayaran', ['success', 'siap-digunakan', 'selesai'])->where('jenis_pembayaran', 'dp')->sum('total_dp');
        $total_sewa_lapang_selesai_full = Booking::whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->whereIn('status_pembayaran', ['success', 'siap-digunakan', 'selesai'])->where('jenis_pembayaran', 'full')->sum('total_harga');

        $data['transaksi'] = Booking::whereIn('status_pembayaran', ['success', 'selesai', 'siap-digunakan'])->whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->orderBy('kode', 'desc')->get();
        $data['judul'] = 'Laporan Data Sewa Lapangan <br> Periode ' . helper::tgl_indo($tanggal_awal) . ' s/d ' . helper::tgl_indo($tanggal_akhir);
        $data['total_pendapatan'] = $total_sewa_lapang_selesai_full + $total_sewa_lapang_selesai_dp;

        return view('back.pages.booking.print', $data);
    }

    public function laporan(Request $request)
    {


        return view('back.pages.booking.laporan');
    }

    public function laporanProses(Request $request)
    {
        $pisah = explode('/', $request->tanggal);
        $tanggal_awal = $pisah[0];
        $tanggal_akhir = $pisah[1];

        $total_sewa_lapang_selesai_dp = Booking::whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->whereIn('status_pembayaran', ['success', 'siap-digunakan', 'selesai'])->where('jenis_pembayaran', 'dp')->sum('total_dp');
        $total_sewa_lapang_selesai_full = Booking::whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->whereIn('status_pembayaran', ['success', 'siap-digunakan', 'selesai'])->where('jenis_pembayaran', 'full')->sum('total_harga');
        $sisa_pembayaran1 = Booking::whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->where('jenis_pembayaran', 'dp')->where('status_pembayaran', 'success')->sum('total_dp');
        $sisa_pembayaran2 = Booking::whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->where('jenis_pembayaran', 'dp')->where('status_pembayaran', 'success')->sum('total_harga');
        $menunggu_pembayaran = Booking::whereBetween('tanggal_transaksi', [$tanggal_awal, $tanggal_akhir])->where('status_pembayaran', 'pending')->sum('total_harga');
        $barang_keluar = BarangKeluar::whereBetween('tgl_faktur', [$tanggal_awal, $tanggal_akhir])->sum('grand_total');
        $barang_masuk = BarangMasuk::whereBetween('tgl_faktur', [$tanggal_awal, $tanggal_akhir])->sum('grand_total');
        $data['sisa_pembayaran'] = $sisa_pembayaran2 - $sisa_pembayaran1;
        $data['total_sewa_lapang'] = $total_sewa_lapang_selesai_dp + $total_sewa_lapang_selesai_full;
        $data['menunggu_pembayaran'] = $menunggu_pembayaran;
        $data['barang_keluar'] = $barang_keluar;
        $data['barang_masuk'] = $barang_masuk;

        // $data['transaksi'] = Booking::whereIn('status_pembayaran',['success','selesai','siap-digunakan'])->whereBetween('tanggal_transaksi',[$tanggal_awal,$tanggal_akhir])->orderBy('kode','asc')->get();
        $data['judul'] = 'Laporan Pendapatan Keseluruhan <br> Periode ' . helper::tgl_indo($tanggal_awal) . ' s/d ' . helper::tgl_indo($tanggal_akhir);
        return view('back.pages.booking.print_keseluruhan', $data);
    }
}
