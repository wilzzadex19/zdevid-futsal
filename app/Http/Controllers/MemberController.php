<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MemberController extends Controller
{
    public function index()
    {
        $data['member'] = User::where('role', 'member')->orderBy('id', 'desc')->get();
        return view('back.pages.member.index', $data);
    }

    public function add()
    {
        return view('back.pages.member.add');
    }

    public function edit($id)
    {
        $member = User::findOrFail($id);
        $data['member'] = $member;
        return view('back.pages.member.edit',$data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $cek_email = User::where('email', $request->email)->get();
        if (count($cek_email) > 0) {
            return redirect()->back()->with('error', 'Email ' . $request->email . ' Telah terdaftar, silahkan gunakan email yang lain ');
        }

        $new = new User();
        $new->name = $request->nama;
        $new->email = $request->email;
        $new->alamat = $request->alamat;
        $new->password = bcrypt('futsal2022');
        $new->role = 'member';
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $file_name = md5($request->email) . '.' . $file->getClientOriginalExtension();
            $file->move('img/member/', $file_name);
            $new->foto = $file_name;
        }
        $new->save();

        $data['member'] = $new;
        $email = [$new->email];
        // return view('email.member',$data);
        Mail::send('email.member', $data, function ($message) use ($email) {
            $message->to($email)->subject('Pendaftaran Member');
        });
        return redirect()->route('member')->with('success', 'Data berhasil di simpan !');
    }

    public function update(Request $request,$id)
    {
        // dd($request->all(),$id);
        $cek_email = User::where('email', $request->email)->where('id','!=',$id)->get();
        if (count($cek_email) > 0) {
            return redirect()->back()->with('error', 'Email ' . $request->email . ' Telah terdaftar, silahkan gunakan email yang lain ');
        }

        $new = User::findOrFail($id);
        $new->name = $request->nama;
        $new->email = $request->email;
        $new->alamat = $request->alamat;
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $file_name = md5($request->email) . '.' . $file->getClientOriginalExtension();
            $file->move('img/member/', $file_name);
            $new->foto = $file_name;
        }
        $new->save();
        return redirect()->back()->with('success', 'Data berhasil di ubah ');
    }
}
