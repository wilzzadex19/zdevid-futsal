<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        if(Auth::user()){
            return redirect()->route('dashboard');
        }
        return view('back.pages.auth.index');
    }

    public function post(Request $request)
    {
        $credentials = [
            'username' => $request->username,
            'password' => $request->password,
        ];

        if (Auth::attempt($credentials)) {
            return redirect(route('dashboard'));
        }

        return redirect()->back()->with('error','Username Atau Password Salah !');
    }

    public function logout()
    {
        
        if(Auth::user()){
            if(Auth::user()->role == 'member'){
                $red = 'home';
            }else{
                $red = 'login';
            }
            Auth::logout();
        }

        return redirect(route($red));
    }
}
