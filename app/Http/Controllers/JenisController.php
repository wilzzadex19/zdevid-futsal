<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Jenis;
use App\Merk;
use Illuminate\Http\Request;

class JenisController extends Controller
{
    public function index()
    {
        $data['jenis'] = Jenis::all();
        return view('back.pages.jenis.index',$data);
    }

    public function add()
    {
        $lastKodeMakanan = Jenis::where('kategori', 'Makanan')->count();
        $lastKodeMinuman = Jenis::where('kategori', 'Minuman')->count();
        if ($lastKodeMakanan == 0) {
            $kodeMakanan = 1;
        } else {
            $kodeMakanan = $lastKodeMakanan + 1;
        }
        if ($lastKodeMinuman == 0) {
            $kodeMinuman = 1;
        } else {
            $kodeMinuman = $lastKodeMinuman + 1;
        }
        $data['kode_makanan'] = $kodeMakanan;
        $data['kode_minuman'] = $kodeMinuman;
        return view('back.pages.jenis.add', $data);
    }

    public function store(Request $request)
    {

        $customMessages = [
            'required' => 'Jenis sudah terdaftar.'
        ];
        $this->validate($request, [
            'nama' => 'required|unique:m_jenis,nama',
        ],$customMessages);

        $jenis = new Jenis();
        $jenis->nama = $request->nama;
        $jenis->kategori = $request->kategori;
        if ($request->kategori == 'Makanan') {
            $jenis->kode = $request->kode_makanan;
        } else {
            $jenis->kode = $request->kode_minuman;
        }
        $jenis->save();

        return redirect(route('jenis'))->with('success', 'Data berhasil di simpan');
    }

    public function destroy(Request $request)
    {
        $barang = Jenis::find($request->id);
        $cek = Merk::where('jenis_id', $barang->id)->count();
        if ($cek == 0) {
            $barang->delete();
            return response()->json('oke');
        } else {
            return response()->json('no');
        }
    }

    public function edit($id)
    {
        $jenis = Jenis::find($id);
        $data['jenis'] = $jenis;
        return view('back.pages.jenis.edit',$data);
    }

    public function update($id,Request $request)
    {
        $jenis = Jenis::find($id);
        $jenis->kategori = $request->kategori;
        $jenis->nama =  $request->nama;
        return redirect(route('jenis'))->with('success', 'Data berhasil di update');
    }
}
