<?php

namespace App\Http\Controllers;

use App\BarangKeluar;
use App\BarangMasuk;
use App\Booking;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $total_sewa_lapang_selesai_dp = Booking::whereIn('status_pembayaran', ['success', 'siap-digunakan', 'selesai'])->where('jenis_pembayaran', 'dp')->sum('total_dp');
        $total_sewa_lapang_selesai_full = Booking::whereIn('status_pembayaran', ['success', 'siap-digunakan', 'selesai'])->where('jenis_pembayaran', 'full')->sum('total_harga');
        $sisa_pembayaran1 = Booking::where('jenis_pembayaran', 'dp')->where('status_pembayaran', 'success')->where('is_lunas',NULL)->sum('total_dp');
        $sisa_pembayaran2 = Booking::where('jenis_pembayaran', 'dp')->where('status_pembayaran', 'success')->where('is_lunas',NULL)->sum('total_harga');
        $menunggu_pembayaran = Booking::where('status_pembayaran', 'pending')->sum('total_harga');
        $barang_keluar = BarangKeluar::sum('grand_total');
        $barang_masuk = BarangMasuk::sum('grand_total');

        $transaksi  = Booking::whereIn('status_pembayaran', ['success', 'selesai', 'siap-digunakan'])->orderBy('kode', 'asc')->get();

        $total_pemasukan = 0;
        $total_pemasukans = 0;
        $telah_dibayar = 0;
        foreach ($transaksi as $item) {
            if ($item->jenis_pembayaran == 'dp') {
                if ($item->is_lunas == 1) {
                    $telah_dibayar += $item->total_harga;
                } else {
                    $telah_dibayar += $item->total_dp;
                }
            } else {
                $telah_dibayar += $item->total_harga;
            }
            $total_pemasukans += $item->total_harga;
        }

        $data['total_pemasukan'] = $total_pemasukan;
        $data['total_pemasukans'] = $total_pemasukans;
        $data['telah_dibayar'] = $telah_dibayar;
        $data['belum_lunas'] = $total_pemasukans - $telah_dibayar;


        $data['sisa_pembayaran'] = $sisa_pembayaran2 - $sisa_pembayaran1;
        $data['total_sewa_lapang'] = $total_sewa_lapang_selesai_dp + $total_sewa_lapang_selesai_full;
        $data['menunggu_pembayaran'] = $menunggu_pembayaran;
        $data['barang_keluar'] = $barang_keluar;
        $data['barang_masuk'] = $barang_masuk;
        return view('back.pages.dashboard.index', $data);
    }
}
