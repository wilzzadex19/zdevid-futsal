<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function indexProfilAdmin()
    {
        $data['user'] = User::find(auth()->user()->id);
        return view('back.pages.auth.profil',$data);
    }

    public function updateAdmin(Request $request)
    {
        $user = User::find(auth()->user()->id);
        $user->username = $request->username;
        $user->name = $request->name;
        if(!empty($request->password)){
            $user->password = bcrypt($request->password);
        }
        $user->save();

        return redirect()->back()->with('success','Profil berhasil di ubah');
    }

    public function indexProfil()
    {
        $data['user'] = User::find(auth()->user()->id);
        return view('front.pages.profile',$data);
    }

    public function update(Request $request)
    {

        $cek_email = User::where('email', $request->email)->where('id','!=',auth()->user()->id)->get();
        if (count($cek_email) > 0) {
            return redirect()->back()->with('error', 'Email ' . $request->email . ' Telah terdaftar, silahkan gunakan email yang lain ');
        }

        $new =  User::find(auth()->user()->id);
        $new->name = $request->name;
        $new->email = $request->email;
        $new->alamat = $request->alamat;
        $new->password = bcrypt($request->password);
        $new->role = 'member';
        if ($request->hasFile('logo')) {

            $file_ex = 'img/member/' . $new->foto;
            if(is_file($file_ex)){
                unlink($file_ex);
            }

            $file = $request->file('logo');
            $file_name = md5($request->kode_item) . '.' . $file->getClientOriginalExtension();
            $file->move('img/member/', $file_name);
            $new->foto = $file_name;
        }
        $new->save();

        return redirect()->back()->with('success','Profil berhasil di perbaharui');
    }
}
