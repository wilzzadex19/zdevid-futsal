<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'm_jadwal_lapangan';

    public function item()
    {
        return $this->hasOne(Item::class, 'id', 'item_id');
    }
    public function hari()
    {
        return $this->hasOne(Hari::class, 'id', 'hari_id');
    }
    public function jam()
    {
        return $this->hasOne(Jam::class, 'id', 'jam_id');
    }
}
