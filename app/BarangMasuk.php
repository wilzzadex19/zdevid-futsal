<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangMasuk extends Model
{
    protected $table = 'barang_masuk';

    public function detail()
    {
        return $this->hasMany(Detail_Penjualan::class,'barangmasuk_id','id');
    }

    public function suplier()
    {
        return $this->hasOne(Suplier::class,'id','suplier_id');
    }
}
