@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Pengaturan Aplikasi</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header">
                    Pengaturan Aplikasi
                </div>
                <div class="card-body">
                    <form action="{{ route('setting.store') }}" id="formAdd" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Nama Aplikasi <span class="text-danger">*</span> </label>
                            <input type="text" class="form-control" value="{{ config('global.app_setting')->app_name }}" name="nama_aplikasi" placeholder="Nama Aplikasi...">
                        </div>
                        <div class="form-group">
                            <label>Tentang<span class="text-danger">*</span> </label>
                            <textarea name="tentang" class="form-control" cols="30" rows="10" placeholder="Tentang aplikasi...">{{ config('global.app_setting')->app_description }}</textarea>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Logo Aplikasi<span class="text-danger">*</span> </label>
                                    <input type="file" onchange="readFile(this,'preview')" id="input_image" class="form-control"
                                        name="logo" accept="image/*">

                                    <small>Format yang di dukung : jpg,jpeg,png</small> <br>
                                    <small>Ukuran File Maksimal : 2MB</small>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{ config('global.app_setting')->app_logo == null ? '' : asset('assets/img/' . config('global.app_setting')->app_logo) }}"
                                        alt="" class="img-thumbnail" id="preview_image">
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-primary float-right">Simpan Perubahan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        var runValidator = function() {
            var form = $('#formAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    nama_aplikasi: {
                        required: true,
                        minlength: 3,
                    },
                    tentang: {
                        required: true,
                        minlength: 10,
                    },
                    // logo: {
                    //     required: true,
                    // },




                },
                // messages: {
                //     username: {
                //         required: "Please enter a username",
                //         minlength: "Your username must consist of at least 3 characters"
                //     },
                //     password: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long"
                //     },
                //     kpassword: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long",
                //         equalTo: "Please enter the same password as above"
                //     },
                // },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
