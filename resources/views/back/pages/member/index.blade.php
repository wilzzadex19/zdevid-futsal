@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Penyewa</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Daftar Penyewa </span>

                    <a href="{{ route('member.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Data</span>
                    </a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tabelItem">
                            <thead>
                                <tr>
                                    <th width="10px">No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>Bergabung Sejak</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($member as $key => $item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td class=""> <img style="width:40px;margin-left: 20px;"
                                                class="img-profile my-img"
                                                src="{{ $item->foto != null ? asset('img/member/' . $item->foto) : asset('assets/img/undraw_profile.svg') }}">
                                            {{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->alamat }}</td>
                                        <td>{{ \App\Helper\helper::tgl_indo_jam($item->created_at) }}</td>
                                        <td> <a href="{{ route('member.edit', $item->id) }}"
                                                class="btn btn-warning btn-circle btn-sm">
                                                <i class="fas fa-edit"></i>
                                            </a></td>
                                    </tr>
                                @endforeach
                                {{-- @foreach ($item as $key => $item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->kode }} | {{ $item->name }}</td>
                                        <td>{{ $item->category->name }}</td>
                                        <td> {!! \App\Helper\helper::limit_desc($item->desc, 200)!!} </td>
                                        <td><img src="{{ asset('img/item/' . $item->foto) }}" style="width: 150px" alt="">
                                        </td>
                                        <td>
                                            <a href="{{ route('item.edit', $item->id) }}"
                                                class="btn btn-warning btn-circle btn-sm">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-url="{{ route('item.delete',$item->id) }}" onclick="itemDelete(this)" id="{{ $item->id }}"
                                                class="btn btn-danger btn-circle btn-sm">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        $('#tabelItem').DataTable();

        function itemDelete(obj) {
            let id = $(obj).attr('id');
            let url = $(obj).attr('data-url');

            Swal.fire({
                title: 'Yakin ?',
                text: "Data akan terhapus permanen !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }

        var runValidator = function() {
            var form = $('#formAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    nama_aplikasi: {
                        required: true,
                        minlength: 3,
                    },
                    tentang: {
                        required: true,
                        minlength: 10,
                    },
                    // logo: {
                    //     required: true,
                    // },




                },
                // messages: {
                //     username: {
                //         required: "Please enter a username",
                //         minlength: "Your username must consist of at least 3 characters"
                //     },
                //     password: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long"
                //     },
                //     kpassword: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long",
                //         equalTo: "Please enter the same password as above"
                //     },
                // },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
