<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $judul }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        @media print {
            @page {
                size: auto;
                margin-top: 0;
                margin-bottom: 0px;
            }

            #data,
            #data th,
            #data td {
                border: 1px solid;
            }

            #data td,
            #data th {
                padding: 5px;
            }

            #data {
                border-spacing: 0px;
                margin-top: 40px;
                font-size: 17px;
            }

            #childTable {
                border: none;
            }

            body {
                padding-top: 10px;
                font-family: sans-serif;
            }
        }
    </style>
</head>

<body onload="window.print()">
    <table style="width:100%;margin-top: 50px;vertical-align: middle" border="0">
        <tr>
            <td style="width: 100%" colspan="3">
                <div class="row">
                    <div class="col-3 text-center">
                        <img width="60%" src="{{ url('assets/img/' . config('global.app_setting')->app_logo) }}"
                            alt="">
                    </div>
                    <div class="col-8">
                        <div class="row mt-5">
                            <div class="col-12">
                                {{-- <h4> <strong> {{ config('global.app_setting')->app_name }} </strong></h4> --}}
                                <h5><strong>{!! $judul !!}</strong></h5>
                            </div>
                            <div class="col-12">
                                <span>Jl. Raya Leles No. 8 Garut</span>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-6 text-center">
                        <span>{{ config('global.app_setting')->app_name }}</span>
                    </div>
                </div> --}}
            </td>

        </tr>
    </table>

    <hr style="border: 1px solid black">

    <table id="data" style="width:100%">
        <tr>
            <td class="text-center">NO.</td>
            <td class="text-center">KODE BARANG</td>
            <td class="text-center">KATEGORI</td>
            <td class="text-center">JENIS BARANG</td>
            <td class="text-center">MERK BARANG</td>
            <td class="text-center">STOK</td>
            <td class="text-center">TERJUAL</td>
        </tr>
        @foreach ($barang as $key => $item)
            <tr>
                <td class="text-center">{{ $key + 1 }}</td>
                <td class="text-center">{{ $item->kode_barang }}</td>
                <td class="text-center">{{ $item->kategori }}</td>
                <td class="text-center">{{ $item->jenis->nama }}</td>
                <td class="text-center">{{ $item->merk->nama }}</td>
                <td class="text-center">{{ $item->jumlah == '' ? '0' : $item->jumlah }}</td>
                <td class="text-center">
                    @php
                        $jml = \App\Detail_BarangKeluar::where('barang_id', $item->id)
                            ->whereHas('parents', function ($q) use ($tanggal_awal, $tanggal_akhir) {
                                $q->whereBetween('tgl_faktur', [$tanggal_awal, $tanggal_akhir]);
                            })
                            ->sum('jumlah');
                    @endphp
                    {{ $jml }}
                </td>
            </tr>
        @endforeach
        {{-- @php
            $total_pemasukan = 0;
        @endphp
        @foreach ($transaksi as $key => $item)
        @php
            $total_pemasukan += $item->grand_total;
        @endphp
            <tr>
                <td class="text-center">{{ $key + 1 }}</td>
                <td class="text-center">{{ $item->no_faktur }}</td>
                <td class="text-center">{{ \App\Helper\helper::tgl_indo($item->tgl_faktur) }}</td>
                <td class="text-center">{{ $item->suplier->nama }}</td>
                <td class="text-center">{{ number_format($item->sub_total) }}</td>
                <td class="text-center">{{ number_format($item->total_diskon) }}</td>
                <td class="text-center">{{ number_format($item->pajak) }} %</td>
                <td class="text-center">{{ number_format($item->grand_total) }}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="7"><strong>Total Pengeluaran</strong></td>
            <td style="text-align: center"> <strong> {{  number_format($total_pemasukan)  }} </strong></td>
        </tr> --}}


    </table>
    <br>
    <table border="0" style="width:100%">
        @php
            $admins = \App\User::where('role', 'admin-barang')->first();
        @endphp
        <tr>
            <td class="text-right"><strong> Penanggung Jawab </strong></td>



        </tr>
        <tr>
            <td><br><br></td>
        </tr>
        <tr>
            <td class="text-right"><strong>{{ $admins->name }}</strong></td>
        </tr>
    </table>
</body>

</html>
