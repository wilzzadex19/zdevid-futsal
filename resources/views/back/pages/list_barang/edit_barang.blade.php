@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Edit Barang </span>

                    {{-- <a href="{{ route('item.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Data</span>
                    </a> --}}
                </div>
                <div class="card-body">
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ session('error') }}</strong>
                        </div>
                    @endif
                    @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            <strong>Kode Barang Sudah Terdaftar</strong>
                        </div>
                    @endforeach
                @endif
                    <form method="POST" action="{{ route('barang.update',$barang->id) }}" id="userAdd">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Kategori
                                    <span class="text-danger">*</span></label>
                                <select name="kategori" id="kategori" onchange="getKategori()" required
                                    class="form-control">
                                    <option value=""> - Pilih Kategori - </option>
                                    <option value="Makanan" {{ $barang->kategori == 'Makanan' ? 'selected' : '' }}>Makanan</option>
                                    <option value="Minuman" {{ $barang->kategori == 'Minuman' ? 'selected' : '' }}>Minuman</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Jenis
                                    <span class="text-danger">*</span></label>
                                <select name="jenis_id" id="jenis_id" onchange="getMerk()" required
                                    class="form-control">
                                    <option value=""> - Pilih - </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Merk
                                    <span class="text-danger">*</span></label>
                                <select name="merk_id" id="merk_id" onchange="getKode(this)" required
                                    class="form-control">
                                    <option value=""> - Pilih - </option>
                                    @foreach ($merk as $item)
                                        <option value="{{ $item->id }}" {{ $item->id == $barang->merk_id ? 'selected' : '' }}>{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Kode Barang
                                    <span class="text-danger">*</span></label>
                                <input type="text" readonly class="form-control" value="{{ $barang->kode_barang }}" id="kode_barang"
                                    name="kode_barang" placeholder="Kode Barang" />
                                <small class="text-danger">Kode Barang Tergenerate Setelah Memilih Kategori, Jenis dan
                                    Merk</small>
                            </div>
                           
                            <div class="form-group">
                                <label>Harga Beli
                                    <span class="text-danger">*</span></label>
                                <input type="number" onkeyup="cekHarga(this)" min="0" class="form-control" value="{{ $barang->harga }}"
                                    id="harga_beli"  name="harga"
                                    placeholder="Masukan Harga..." />
                            </div>
                            <div class="form-group">
                                <label>Harga Jual
                                    <span class="text-danger">*</span></label>
                                <input type="number" onkeyup="cekHarga(this)" min="0" class="form-control"
                                    id="harga_jual"  name="harga_jual" value="{{ $barang->harga_jual }}"
                                    placeholder="Masukan Harga Jual..." />
                            </div>
                            {{-- <div class="form-group">
                                <label>Jumlah
                                    <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" name="jumlah" placeholder="Masukan Jumlah..." />
                            </div> --}}
                        </div>
                        <div class="card-footer">
                            <button type="submit" id="btn-simpan" class="btn btn-primary mr-2">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

    <script>
        // CKEDITOR.replace('editor_deskripsi');
        $(document).ready(function() {
            $('#harga_beli').mask('000.000.000', {
                reverse: true
            });
            $('#harga_jual').mask('000.000.000', {
                reverse: true
            });

            getKategori()
            
        })

        function getKategori() {
            let kategori = $('#kategori').find(':selected').val();
            if (kategori == '') {
                $('#jenis_id').html(`<option value=""> - Pilih - </option>`);
            }
            $.ajax({
                url: '{{ route('merk.getJenis.edit') }}',
                type: 'get',
                data: {
                    kategori: kategori,
                    selected : '{{ $barang->jenis_id }}'
                },
                success: function(res) {
                    $('#jenis_id').html(res);
                }
            })

            // getMerk()
        }

        function getMerk() {
            let jenis = $('#jenis_id').find(':selected').val();
            if (jenis == '') {
                $('#jenis_id').html(`<option value=""> - Pilih - </option>`);
            }
            $.ajax({
                url: '{{ route('merk.getMerkEdit') }}',
                type: 'get',
                data: {
                    jenis: jenis,
                    selected : '{{ $barang->merk_id }}'
                },
                success: function(res) {
                    $('#merk_id').html(res);
                }
            })
        }

        function getKode(obj) {
            let kategori = $('#kategori').find(':selected').val();
            let jenis = $('#jenis_id').find(':selected').val();
            let merk = $(obj).find(':selected').val();
            console.log(kategori, jenis, merk);

            $.ajax({
                url: '{{ route('barang.getKode') }}',
                type: 'get',
                data: {
                    kategori: kategori,
                    jenis: jenis,
                    merk: merk,
                },
                success: function(res) {
                    $('#kode_barang').val(res);
                }
            })
        }
        // $('#tabelItem').DataTable();
        function cekHarga() {
            let harga_beli = $('#harga_beli').val();
            let harga_jual = $('#harga_jual').val();

            if (parseInt(harga_jual) < parseInt(harga_beli)) {
                $('#btn-simpan').prop('disabled', true);
            } else {
                $('#btn-simpan').prop('disabled', false);
            }
        }

        var runValidator = function() {
            var form = $('#userAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    nama_barang: "required",
                    kode_barang: "required",
                    harga: "required",
                },
                messages: {

                },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
