@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Barang</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Daftar Barang </span>
                    <div class="div">
                        @if (auth()->user()->role == 'admin-barang')
                            <a href="{{ route('barang.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-plus"></i>
                                </span>
                                <span class="text">Tambah Data</span>
                            </a>
                        @endif
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#modalCetak"
                            class="btn btn-warning btn-sm btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-print"></i>
                            </span>
                            <span class="text">Cetak Laporan</span>
                        </a>
                        {{-- <a href="{{ route('barang.cetak') }}" target="_blank"
                            class="btn btn-warning btn-sm btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-print"></i>
                            </span>
                            <span class="text">Cetak Laporan</span>
                        </a> --}}
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="user_table">
                            <thead>
                                <tr>
                                    <th width="10px">No.</th>
                                    <th>Kode Barang</th>
                                    <th>Kategori</th>
                                    <th>Jenis</th>
                                    <th>Merk Barang</th>
                                    <th>Harga Beli(Rp)</th>
                                    <th>Harga Jual(Rp)</th>
                                    <th>Stok</th>
                                    <th>Jumlah Terjual</th>
                                    @if (auth()->user()->role == 'admin-barang')
                                        <th>Aksi</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($barang as $key => $item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->kode_barang }}</td>
                                        <td>{{ $item->kategori }}</td>
                                        <td>{{ $item->jenis->nama }}</td>
                                        <td>{{ $item->merk->nama }}</td>
                                        <td>{{ number_format($item->harga) }}</td>
                                        <td>{{ number_format($item->harga_jual) }}</td>
                                        <td>{{ $item->jumlah == null ? 0 : $item->jumlah }}</td>
                                        <td>
                                            @php
                                                $jml = \App\Detail_BarangKeluar::where('barang_id',$item->id)->sum('jumlah');
                                            @endphp
                                            {{ $jml }}
                                        </td>
                                        @if (auth()->user()->role == 'admin-barang')
                                            <td nowrap="nowrap">
                                                <a href="{{ route('barang.edit', $item->id) }}"
                                                    class="btn btn-warning btn-circle btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                                <a onclick="hapusBarang(this)" id="{{ $item->id }}"
                                                    href="javascript:void(0)" class="btn btn-danger btn-circle btn-sm">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                                {{-- <div class="dropdown dropdown-inline mr-4">
                                              
                                                <button type="button" class="btn btn-light-primary btn-icon btn-sm"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="ki ki-bold-more-hor"></i>
                                                </button>
                                                <div class="dropdown-menu" style="">
                                                    <a class="dropdown-item"
                                                        href="{{ route('barang.edit', $item->id) }}">Edit</a>
                                                    <a class="dropdown-item">Hapus</a>
                                                </div>
                                            </div> --}}
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalCetak" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Laporan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('barang.cetak') }}" target="_blank" method="POST">
                    @csrf
                    <div class="modal-body">
                       
                        <div class="form-group">
                            <label>Pilih Periode</label>
                            <div id="reportrange"
                                style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="fa fa-calendar"></i>&nbsp;
                                <span></span> <i class="fa fa-caret-down"></i>
                            </div>
                            <input type="hidden" name="tanggal" id="tanggal">
                        </div>
                        <div class="form-group">
                            <label>Cetak Berdasarkan</label>
                            <select name="urutan" class="form-control">
                                <option value="">Default</option>
                                <option value="laris">Paling Laris</option>
                                <option value="tidak">Paling Tidak Laku</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Cetak</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        $('#user_table').DataTable();

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#tanggal').val(start.format('YYYY-MM-DD') + '/' + end.format('YYYY-MM-DD'))
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month')
                    .endOf(
                        'month')
                ]
            }
        }, cb);

        cb(start, end);

        function hapusBarang(obj) {
            let id = $(obj).attr('id');
            Swal.fire({
                title: "Anda Yakin ?",
                text: "Data akan terhapus permanen",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Iya, Hapus saja!",
                cancelButtonText: "Tidak, Batalkan!",
                reverseButtons: true
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: '{{ route('barang.destroy') }}',
                        type: 'get',
                        data: {
                            id: id,
                        },
                        beforeSend: function() {
                            // KTApp.blockPage({
                            //     overlayColor: '#000000',
                            //     state: 'danger',
                            //     message: 'Silahkan Tunggu...'
                            // });
                        },
                        success: function(res) {
                            // KTApp.unblockPage();
                            if (res == 'no') {
                                Swal.fire(
                                    "Gagal dihapus!",
                                    "Data ini ada di transaksi",
                                    "warning"
                                )
                            } else {
                                Swal.fire(
                                    "Terhapus!",
                                    "Data berhasil di hapus.",
                                    "success"
                                ).then(function() {
                                    window.location.reload();
                                })
                            }

                        }
                    })
                    // Swal.fire(
                    //     "Deleted!",
                    //     "Your file has been deleted.",
                    //     "success"
                    // )
                }
            });
        }

        var runValidator = function() {
            var form = $('#formAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    nama_aplikasi: {
                        required: true,
                        minlength: 3,
                    },
                    tentang: {
                        required: true,
                        minlength: 10,
                    },
                    // logo: {
                    //     required: true,
                    // },




                },
                // messages: {
                //     username: {
                //         required: "Please enter a username",
                //         minlength: "Your username must consist of at least 3 characters"
                //     },
                //     password: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long"
                //     },
                //     kpassword: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long",
                //         equalTo: "Please enter the same password as above"
                //     },
                // },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
