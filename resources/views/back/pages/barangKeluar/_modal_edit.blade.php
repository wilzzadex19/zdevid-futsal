<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Edit Barang</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="formEditTemp">
            <div class="modal-body">
                <div class="container-fluid">

                    @csrf
                    <div class="form-group">
                        <label for="">Barang</label> <br>
                        <input type="hidden" name="id" value="{{ $temp->id }}">
                        <select name="barang_id" id="barang_id" onchange="getHarga(this)" style="width: 100%" class="form-control select2"
                            required>
                            <option value="">- Pilih Barang</option>
                            @foreach ($barang as $item)
                                <option value="{{ $item->id }}" {{ $item->id == $temp->barang_id ? 'selected' : '' }} data-harga="{{ $item->harga }}">
                                    {{ $item->nama_barang }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Harga</label> <br>
                        <input type="text" name="harga" value="{{ $temp->barang->harga }}" readonly class="form-control" required id="edit_harga_barang">
                    </div>
                    <div class="form-group">
                        <label for="">Jumlah</label> <br>
                        <input type="number" min="0" name="jumlah" value="{{ $temp->jumlah }}" class="form-control" required
                            id="jumlah_barang">
                    </div>
                    <div class="form-group">
                        <label for="">Diskon (%)</label> <br>
                        <input type="number" min="0" name="diskon" value="{{ $temp->diskon }}" class="form-control" required id="diskon">
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>