<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="robots" content="noindex, nofollow">
    <title>{{ $data->no_faktur }}</title>
    <style>
        @page {
            header: page-header;
            footer: page-footer;
            margin: 5px;
            line-height: 1;


        }

        body {
            font-family: courier;
            font-size: 12px;
            line-height: 1;
        }
    </style>
</head>

<body>
    {{-- <htmlpageheader name="page-header">{{ $data->no_faktur }}</htmlpageheader> --}}
    <div style="text-align:center">
        <htmlpagefooter name="page-footer"></htmlpagefooter>
    </div>
    <div>
        <table style="width: 100%">
            <tr>
                <td style="width: 25%">
                    <img width="100%" src="{{ url('assets/img/' . config('global.app_setting')->app_logo) }}"
                        alt="">
                </td>
                <td style="text-align: center">
                    <h2>{{ config('global.app_setting')->app_name }}</h2>
                </td>
            </tr>
            <tr>
              <td colspan="2" style="text-align: center"><span>Jl. Raya Leles No. 8 Garut</span></td>
            </tr>
        </table>
        <hr>
       <table style="width: 100%">
        <tr>
          <td> Tanggal </td>
          <td>: {{ date('Y-m-d - H:i') }}</td>
        </tr>
        <tr>
          @php
          $admins = \App\User::where('role', 'admin')->first();
      @endphp
          <td> Penanggung Jawab </td>
          <td>: {{ $admins->name }}</td>
        </tr>
      </table>
    </div>
    <hr style="border : 0.5px dashed black">
    <div>
      {{-- <br> --}}
        {{-- <hr> --}}
        <table style="width:100%" border="0">
            <thead>
                <tr>
                    <th style="text-align: left">Barang</th>
                    <th>Jumlah</th>
                    <th>Harga</th>
                    <th>Disc</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($data->detail as $key => $item)
                  <tr>
                    <td>{{ $item->barang->kategori == 'Makanan' ? $item->barang->jenis->nama : ''  }} {{ $item->barang->merk->nama }}</td>
                    <td>{{ $item->jumlah }}</td>
                    <td>{{ number_format($item->barang->harga_jual) }}</td>
                    <td>{{ $item->diskon }}%</td>
                    <td> <strong> {{ number_format($item->total_harga) }} </strong></td>
                  </tr>
              @endforeach

                {{-- @php
            $no=1;
        @endphp
        @foreach (DB::table('order_details')->where('invoice', $item->invoice)->join('products', 'order_details.product_id', '=', 'products.id')->select('order_details.*', 'products.name')->get() as $cart)
        <tr>
          <td>{{$no++}}</td>
          <td>{{ $cart->name }}</td>
          <td>{{ $cart->qty }}</td>
          <td style="text-align:right">{{ number_format($cart->price) }}</td>
          <td style="text-align:right">{{ number_format($cart->price * $cart->qty)}}</td>
        </tr>
        @endforeach --}}
            </tbody>
        </table>
        <hr style="border : 0.5px dashed black">
        <table style="width: 100%">
          <tr>
            <td>Sub Total</td>
            <td style="text-align: right"><strong> {{ number_format($data->sub_total) }}</strong></td>
          </tr>
          <tr>
            <td>Total Diskon</td>
            <td style="text-align: right"><strong> {{ number_format($data->total_diskon) }}</strong></td>
          </tr>
          <tr>
            <td>Grand Total</td>
            <td style="text-align: right"><strong> {{ number_format($data->grand_total) }}</strong></td>
          </tr>
          <tr>
            <td>Tunai</td>
            <td style="text-align: right"><strong> {{ number_format($data->uang_bayar) }}</strong></td>
          </tr>
          <tr>
            <td>Kembali</td>
            <td style="text-align: right"><strong> {{ number_format($data->uang_kembali) }}</strong></td>
          </tr>
        </table>

        {{-- <hr> --}}
        {{-- <table style="width:100%">
      <tbody>
        <tr>
          <td colspan="3" rowspan="4">
            <div>
              <label>Notes:</label>
            </div>
          </td>
          <td style="text-align:right">Sub-Total</td>
          <td colspan="2" style="text-align:right">{{ number_format($subtotal) }}</td>
        </tr>
        <tr>
          <td style="text-align:right">Tax (10%)</td>
          <td colspan="2" style="text-align:right">{{ number_format($tax) }}</td>
        </tr>
        <tr>
          <td style="text-align:right">Discount</td>
          <td colspan="2" style="text-align:right">-0</td>
        </tr>
        <tr>
          <td style="text-align:right">Total</td>
          <td colspan="2" style="text-align:right">
            <h4>{{ number_format($grandtotal) }}</h4>
          </td>
        </tr>
      </tbody>
    </table> --}}
    </div>
    {{-- <div class="page-break"></div> --}}
</body>

</html>
