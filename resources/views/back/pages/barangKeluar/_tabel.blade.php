<table class="table table-bordered">
    <thead>
        <tr>
            <th>No.</th>
            <th>Kategori</th>
            <th>Jenis</th>
            <th>Merk</th>
            <th>Harga</th>
            <th>Jumlah</th>
            <th>Diskon (%)</th>
            <th>Total Harga</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($barang as $key=> $item)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $item->barang->kategori }}</td>
                <td>{{ $item->barang->jenis->nama }}</td>
                <td>{{ $item->barang->merk->nama }}</td>
                <td>{{ number_format($item->barang->harga_jual) }}</td>
                <td>{{ $item->jumlah }}</td>
                <td>{{ $item->diskon }}</td>
                <td>{{  number_format($item->total_harga)  }}</td>
                <td>
                    <button type="button" class="btn btn-sm btn-warning" onclick="editData(this)" id="{{ $item->id }}"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="deleteData(this)" id="{{ $item->id }}"><i class="fa fa-trash"></i></button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>