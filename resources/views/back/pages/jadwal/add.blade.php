@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Tambah Item Sewa </span>

                    {{-- <a href="{{ route('item.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Data</span>
                    </a> --}}
                </div>
                <div class="card-body">
                    <form action="{{ route('jadwal.store') }}" method="POST" id="formAddJadwal"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Lapangan <span class="text-danger">*</span></label>
                            <select name="item_id" id="item_id" required class="form-control select2">
                                <option value="">- Pilih Lapangan</option>
                                @foreach ($lapangan as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Hari <span class="text-danger">*</span></label>
                                <select name="hari_id" id="hari_id" required class="form-control select2" onchange="cekJam(this)">
                                    <option value="">- Pilih Hari -</option>
                                    @foreach ($hari as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Jam <span class="text-danger">*</span></label>
                                <select name="jam_id" id="jam_id" required class="form-control select2">
                                    <option value="">- Pilih Jam -</option>
                                    @foreach ($jam as $item)
                                        <option value="{{ $item->id }}">{{ $item->display }}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{-- <div class="col-md-4">
                                <label>Status <span class="text-danger">*</span></label>
                                <select name="status" class="form-control select2">
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                            </div> --}}
                        </div>
                        <div class="form-group">
                            <label>Harga</label>
                            <input type="text" name="harga" class="form-control" placeholder="Masukkan Harga..." required>
                        </div>

                        <button type="submit" class="btn btn-success float-right">Simpan</button>
                        <a href="{{ route('jadwal') }}" class="btn btn-secondary float-right">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        // CKEDITOR.replace('editor_deskripsi');
        $('.select2').select2();

        $('#item_id').on('change',function(){
            $('#hari_id').val('').trigger('change')
            $('#jam_id').val('').trigger('change')
        });

        $('#hari_id').on('change',function(){
            $('#jam_id').val('').trigger('change')
        });

        function cekJam(obj) {
            let item_id = $('#item_id').find(':selected').val();
            let hari_id = $(obj).find(':selected').val();
            $.ajax({
                type: 'get',
                url: '{{ route('jadwal.cekJam') }}',
                data: {
                    item_id: item_id,
                    hari_id: hari_id,
                },
                success: function(res) {
                    $.each(res.jam, function(kj, ij) {
                        $('#jam_id option[value="' + ij.id + '"]').prop('disabled', false);
                    });
                    if (res.terisi != 0) {
                        $.each(res.terisi, function(k, i) {
                            $('#jam_id option[value="' + i.jam_id + '"]').prop('disabled', true);
                        });
                    }
                }
            })
        }
        var runValidator = function() {
            var form = $('#formAddJadwal');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    // nama_aplikasi: {
                    //     required: true,
                    //     minlength: 3,
                    // },
                    // tentang: {
                    //     required: true,
                    //     minlength: 10,
                    // },
                    // logo: {
                    //     required: true,
                    // },




                },
                // messages: {
                //     username: {
                //         required: "Please enter a username",
                //         minlength: "Your username must consist of at least 3 characters"
                //     },
                //     password: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long"
                //     },
                //     kpassword: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long",
                //         equalTo: "Please enter the same password as above"
                //     },
                // },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
