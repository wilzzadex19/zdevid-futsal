@php
$dur = 1;
$jam_ada = \App\Detail_Booking::where([
    'item_id' => $jadwal->item_id,
    'hari_id' => $jadwal->hari_id,
    'tanggal' => $tanggal,
])
    ->with('jam')
    ->where('jam_id', '>', $jadwal->jam_id)
    ->with('jam')
    ->first();

if ($jam_ada != null) {
    $hasil = $jam_ada->jam_id - $jadwal->jam_id;
}

@endphp

@foreach ($jam_tersedia as $key => $item)
    @if (empty($hasil))
        <option value="{{ $dur }}">{{ $dur }}
            ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
            {{ date('H:i', strtotime($item->jam->jam_akhir)) }})
        </option>
    @else
        <option value="{{ $dur }}" {{ $key < $hasil ? '' : 'disabled' }}>{{ $dur }}
            ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
            {{ date('H:i', strtotime($item->jam->jam_akhir)) }})
        </option>
    @endif

    @php
        $dur++;
    @endphp
@endforeach
