@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Input Booking </span>

                    {{-- <a href="{{ route('item.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Data</span>
                    </a> --}}
                </div>
                <div class="card-body">
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ session('error') }}</strong>
                        </div>
                    @endif

                    <form action="{{ route('input.store') }}" method="POST" id="formAddMember" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Penyewa</label>
                            <select name="member_id" class="select2 form-control" required>
                                <option value="">- Pilih Penyewa -</option>
                                @foreach ($member as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Diinput oleh</label>
                            <input type="text" readonly value="{{ Auth::user()->name }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Nama Tim</label>
                            <input type="text" required name="team_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>No Hp Penanggung Jawab</label>
                            <input type="text" required name="no_hp" class="form-control">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Pilih Tanggal</label>
                                <input type="date" name="tanggal" onchange="getDate()" min="{{ date('Y-m-d') }}"
                                    class="form-control" id="tanggal" required>
                            </div>
                            <div class="col-md-6">
                                <label>Lapangan</label>
                                <select name="item_id" required onchange="getJadwal(this)" id="item_id"
                                    class="form-control">
                                    <option value="">- Pilih Lapangan -</option>
                                    @foreach ($lapangan as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Hari</label>
                                <input type="text" name="hari_view" readonly required value="-" class="form-control"
                                    id="hari_view">
                            </div>
                            <div class="col-md-6">
                                <label>Jadwal</label>
                                <select name="jadwal_id" id="jadwal_id" required onchange="getJam(this)"
                                    class="form-control">
                                    <option value="">- Pilih Jadwal -</option>

                                </select>
                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>Harga Per Jam</label>
                                <input type="text" readonly value="-" class="form-control" name="harga_sewa"
                                    id="harga_sewa">
                            </div>
                            <div class="col-md-3">
                                <label>Total Harga</label>
                                <input type="text" readonly id="total_harga" value="-" name="total_harga"
                                    class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>Durasi</label>
                                <select name="durasi" id="durasi" class="form-control">
                                    <option value="">- Pilih Durasi -</option>

                                </select>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control" cols="30" rows="5"></textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Pembayaran</label>
                                <select name="jenis_pembayaran" required id="jenis_pembayaran" class="form-control">
                                    <option value="">- Pilih Jenis Pembayaran -</option>
                                    <option value="full">Full</option>
                                    <option value="dp">DP (20% Total Harga)</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Jumlah Harus Dibayar</label>
                                <input type="text" readonly name="jumlah_bayar" id="jumlah_bayar" value=""
                                    name="total_harga" class="form-control">
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control" cols="30" rows="5"></textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Pembayaran</label>
                                <select name="jenis_pembayaran" required id="jenis_pembayaran" class="form-control">
                                    <option value="">- Pilih Pembayaran -</option>
                                    <option value="full">Full</option>
                                    <option value="dp">DP (Minimal 20% Total Harga)</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Jumlah Dibayar</label>
                                <input type="text" required name="jumlah_bayar" readonly id="jumlah_bayar"
                                    onkeyup="cekDp(this)" name="total_harga" class="form-control">

                            </div>
                            <div class="col-md-6" style="display: none" id="box_dp">
                                <label>Minimal Pembayaran</label>
                                <input type="text" readonly name="min_dp" id="min_dp" value="0"
                                    class="form-control">

                            </div>
                            <div class="col-md-6" style="display: none" id="box_dp2">
                                <label>Sisa Pembayaran</label>
                                <input type="text" readonly name="sisa_bayar" id="sisa_bayar" value="0"
                                    class="form-control">

                            </div>
                        </div>
                        <button type="submit" class="btn btn-success float-right" id="btn_book">Simpan</button>
                        <a href="{{ route('member') }}" class="btn btn-secondary float-right">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#total_harga').mask('000.000.000', {
                reverse: true
            });
            $('#jumlah_bayar').mask('000.000.000', {
                reverse: true
            });
        })
        $('.select2').select2();
        // CKEDITOR.replace('editor_deskripsi');
        // $('#tabelItem').DataTable();

        function hitungTotalHarga() {
            let harga_per_jam = ($('#harga_sewa').val()).replace(/\./g, "");
            let durasi = $('#durasi').find(':selected').val();
            let total_harga = parseInt(harga_per_jam) * parseInt(durasi);
            $('#total_harga').mask('000.000.000', {
                reverse: true
            }).val(total_harga).trigger('input');

        }



        $('#jenis_pembayaran').on('change', function() {
            hitungBayar();
            let jenis = $('#jenis_pembayaran').find(':selected').val();
            if (jenis == 'dp') {
                $('#jumlah_bayar').prop('readonly', false);
            } else {
                $('#jumlah_bayar').prop('readonly', true);
            }
        })


        function cekDp(obs) {
            let jenis = $('#jenis_pembayaran').find(':selected').val();
            if (jenis == 'dp') {
                $('#jumlah_bayar').prop('readonly', false);
                let total_harga = ($('#total_harga').val()).replace(/\./g, "");
                let min_dp = ($('#min_dp').val()).replace(/\./g, "");
                let bayar = ($(obs).val()).replace(/\./g, "");
                if (parseInt(bayar) < parseInt(min_dp)) {
                    $('#btn_book').prop('disabled', true);
                } else if (parseInt(bayar) >= parseInt(total_harga)) {
                    $('#btn_book').prop('disabled', true);
                } else {
                    $('#btn_book').prop('disabled', false);
                }


                let sisa_bayar = parseInt(total_harga) - parseInt(bayar);
                $('#sisa_bayar').mask('000.000.000', {
                    reverse: true
                }).val(sisa_bayar).trigger('input');
            } else {
                $('#jumlah_bayar').prop('readonly', true);
                $('#sisa_bayar').mask('000.000.000', {
                    reverse: true
                }).val(0).trigger('input');
            }


        }

        $('#durasi').on('change', function() {
            hitungTotalHarga();
            // updateHarga();
        })

        function updateHarga(obj) {
            let dur = $(obj).find(':selected').val();
            let harga = ($('#harga_sewa').val()).replace(/\./g, "");
            let total_harga = parseInt(harga) * parseInt(dur);
            $('#total_harga').mask('000.000.000', {
                reverse: true
            }).val(total_harga).trigger('input');

            hitungBayar();

            // sewa_tempat.replace(/\./g, "");
        }

        // function hitungBayar() {
        //     let jenis = $('#jenis_pembayaran').find(':selected').val();
        //     let total_harga = ($('#total_harga').val()).replace(/\./g, "");
        //     let jumlah_bayar = 0;

        //     if (jenis == 'full') {
        //         jumlah_bayar = total_harga;
        //     } else {
        //         let potongan = (20 / 100) * parseInt(total_harga);
        //         jumlah_bayar = potongan;
        //     }
        //     $('#jumlah_bayar').mask('000.000.000', {
        //         reverse: true
        //     }).val(jumlah_bayar).trigger('input');

        //     $('#jumlah_bayar').mask('000.000.000', {
        //         reverse: true
        //     }).val(jumlah_bayar).trigger('input');
        //     $('#min_dp').mask('000.000.000', {
        //         reverse: true
        //     }).val(jumlah_bayar).trigger('input');


        // }
        function hitungBayar() {
            let jenis = $('#jenis_pembayaran').find(':selected').val();
            let total_harga = ($('#total_harga').val()).replace(/\./g, "");
            let jumlah_bayar = 0;

            if (jenis == 'full') {
                jumlah_bayar = total_harga;
                $('#box_dp').hide();
                $('#box_dp2').hide();
            } else {
                let potongan = (20 / 100) * parseInt(total_harga);
                jumlah_bayar = potongan;
                $('#box_dp').show();
                $('#box_dp2').show();
            }





            $('#jumlah_bayar').mask('000.000.000', {
                reverse: true
            }).val(jumlah_bayar).trigger('input');
            $('#min_dp').mask('000.000.000', {
                reverse: true
            }).val(jumlah_bayar).trigger('input');
            // $('#min_dp').val(jumlah_bayar);


        }

        function getDate() {
            $('#item_id').val('').trigger('change');
            $('#hari').val('-');
        }

        function getJadwal(obj) {
            let item_id = $(obj).find(':selected').val();
            let dates = $('#tanggal').val();
            $.ajax({
                url: '{{ route('input.getJadwal') }}',
                type: 'get',
                data: {
                    item_id: item_id,
                    dates: dates,
                },
                success: function(res) {
                    $('#jadwal_id').html(res.option_jadwal);
                    $('#hari_view').val(res.hari);
                }
            })

        }

        function getJam(obj) {
            let id_jadwal = $(obj).find(':selected').val();
            let item_id = $('#item_id').find(':selected').val();
            let tanggal = $('#tanggal').val();
            let hari = $('#hari_view').val();
            $.ajax({
                url: '{{ route('input.getJam') }}',
                type: 'get',
                data: {
                    id_jadwal: id_jadwal,
                    item_id: item_id,
                    tanggal: tanggal,
                    hari: hari,
                },
                success: function(res) {
                    $('#durasi').html(res.option_durasi);
                    // $('#harga_sewa').val(res.harga);
                    $('#harga_sewa').mask('000.000.000', {
                        reverse: true
                    }).val(res.harga).trigger('input');
                    hitungTotalHarga();
                }
            })

        }

        var runValidator = function() {
            var form = $('#formAddMember');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    // nama_aplikasi: {
                    //     required: true,
                    //     minlength: 3,
                    // },
                    // tentang: {
                    //     required: true,
                    //     minlength: 10,
                    // },
                    // logo: {
                    //     required: true,
                    // },




                },
                // messages: {
                //     username: {
                //         required: "Please enter a username",
                //         minlength: "Your username must consist of at least 3 characters"
                //     },
                //     password: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long"
                //     },
                //     kpassword: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long",
                //         equalTo: "Please enter the same password as above"
                //     },
                // },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
