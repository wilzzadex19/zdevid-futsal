<option value="">- Pilih Jadwal -</option>
@foreach ($jadwal as $item)
    @if (in_array($item->jam_id, $jam_exist))
        <option value="{{ $item->id }}" disabled data-harga="{{ $item->jam->harga }}">{{ $item->jam->display }}
        </option>
    @else
        @php
            $cur_date = date('Y-m-d H:i:s');
            $jam_jadwal = $tanggal . ' ' . $item->jam->jam_awal;
        @endphp
        @if (strtotime($jam_jadwal) < strtotime($cur_date))
            <option value="{{ $item->id }}" data-harga="{{ $item->jam->harga }}" disabled>{{ $item->jam->display }}</option>
        @else
            <option value="{{ $item->id }}" data-harga="{{ $item->jam->harga }}">{{ $item->jam->display }}</option>
        @endif
    @endif
@endforeach
