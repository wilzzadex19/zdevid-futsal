@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Proses Pembayaran </span>

                    {{-- <a href="{{ route('item.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Data</span>
                    </a> --}}
                </div>
                <div class="card-body">
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ session('error') }}</strong>
                        </div>
                    @endif

                    <form action="{{ route('booking.pay') }}" method="POST">
                        @csrf
                        {{-- <input type="hidden" name="item_id" value="{{ $jadwal->item_id }}">
                        <input type="hidden" name="hari_id" value="{{ $jadwal->hari_id }}">
                        <input type="hidden" name="jam_id" value="{{ $jadwal->jam_id }}">
                        <input type="hidden" name="tanggal" value="{{ $tanggal }}"> --}}
                        <div class="form-group">
                            <label>Booking ID</label>
                            <input type="text" readonly value="{{ $booking->kode }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" readonly value="{{ Auth::user()->name }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Nama Tim</label>
                            <input type="text" required name="team_name" readonly value="{{ $booking->team_name }}"
                                class="form-control">
                        </div>
                        <div class="form-group">
                            {{-- <label>Durasi</label> --}}
                            <table style="width: 100%" class="table table-bordered">
                                <tr>
                                    <td>Lapangan</td>
                                    <td>Jam</td>
                                    <td>Total Durasi</td>
                                </tr>
                                @foreach ($booking->detail as $key => $item)
                                    <tr>
                                        <td>{{ $item->item->name }}</td>
                                        <td>{{ $item->jam->display }}</td>
                                        @if ($key == 0)
                                            <td rowspan="{{ count($booking->detail) }}">{{ count($booking->detail) }}
                                                Jam</td>
                                        @endif

                                    </tr>
                                @endforeach
                            </table>
                        </div>

                        <div class="form-group">
                            <label>No Hp Penanggung Jawab</label>
                            <input type="text" required name="no_hp" readonly value="{{ $booking->no_hp }}"
                                class="form-control">
                        </div>
                        {{-- <div class="form-group row">
                            <div class="col-md-6">
                                <label>Durasi (Jam)</label>
                                <select name="durasi" class="form-control" onchange="updateHarga(this)">
                                    <option value="1">1</option>
                                    @php
                                        $dur = 1;
                                    @endphp
                                    @foreach ($jam_tersedia as $item)
                                        <option value="{{ $dur }}">{{ $dur }}
                                            ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
                                            {{ date('H:i', strtotime($item->jam->jam_akhir)) }}) </option>
                                        @php
                                            $dur++;
                                        @endphp
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Total Harga</label>
                                <input type="text" readonly id="total_harga" value="{{ $booking->total_harga }}"
                                    name="total_harga" class="form-control">
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" readonly class="form-control" cols="30" rows="5">{{ $booking->keterangan }}</textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Pembayaran</label>
                                <select disabled name="jenis_pembayaran" id="jenis_pembayaran" class="form-control">
                                    <option value="full" {{ $booking->jenis_pembayaran == 'full' ? 'selected' : '' }}>
                                        Full</option>
                                    <option value="dp" {{ $booking->jenis_pembayaran == 'dp' ? 'selected' : '' }}>DP
                                        (Minimal 20% Total Harga)</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Jumlah Harus Dibayar</label>
                                <input type="text" readonly name="jumlah_bayar" id="jumlah_bayar"
                                    value="{{ $booking->jenis_pembayaran == 'full' ? $booking->total_harga : $booking->total_dp }}"
                                    name="total_harga" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            {{-- <label>Metode Pembayaran</label> --}}
                            <select name="metode" onchange="cekMetode(this)" style="display: none" id="metode" class="form-control">
                                <option value="">- Pilih Metode -</option>
                                <option value="cash" selected>Cash</option>
                                <option value="virtual">Virtual</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <a href="{{ route('dataBooking.virtual',$booking->id) }}" target="_blank" style="display: none" class="btn btn-success btn-sm btn-block" id="btn_link_bayar">Proses Pembayaran</a>
                            <button type="button" style="display: none" onclick="cekStatus(this)" data-id="{{ $booking->id }}" class="btn btn-warning btn-sm btn-block" id="btn_cek_status">Cek Status Pembayaran</button>
                        </div>
                        <button type="button" id="btn_proses_bayar" onclick="proses(this)" data-url="{{ route('input.proses',$booking->id)  }}" class="btn btn-primary">Proses</button>

                        {{-- @dump($booking->payment_url) --}}

                        {{-- @if (!empty($is_paid))
                            <a href="{{ route('booking') }}" class="buttons binfo">Lihat Histori</a>
                        @else
                            <a href="{{ route('booking') }}" class="buttons bgrey">Batalkan</a>
                        @endif
                        @if (!empty($kadaluarsa) || !empty($is_paid))
                        @else
                            @if (!empty($is_url))
                                <input type="hidden" name="payment_url" value="{{ $booking->payment_url }}">
                                <button type="submit" target="_blank" class="buttons binfo">Proses
                                    Pembayaran</button>
                            @else
                                <a href="" class="buttons binfo">Refresh</a>
                            @endif
                        @endif --}}

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script>
        function cekMetode(obj) {
            let metode = $(obj).val();
            if (metode != null) {
                $('#btn_proses_bayar').hide();
                $('#btn_link_bayar').hide();
                $('#btn_cek_status').hide();
                if (metode == 'cash') {
                    $('#btn_proses_bayar').show();
                } else if (metode == 'virtual') {
                    $('#btn_link_bayar').show();
                    $('#btn_cek_status').show();
                }
            }
        }

        function proses(obj) {
            let url = $(obj).attr('data-url');
            Swal.fire({
                title: 'Yakin ?',
                text: "Pastikan user telah memberikan uang tunai",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }
        function cekStatus(obj) {
            let id = $(obj).attr('data-id');

            $.ajax({
                url: '{{ route('input.cekStatus') }}',
                type: 'get',
                data: {
                    id: id
                },
                success: function(res) {
                    if (res.status == 1) {
                        Swal.fire('Sukses', 'Pemabayaran Berhasil !', 'success').then(function() {
                            window.location.href = '{{ url("data-booking/list/success") }}'
                        });

                        // $('#btn_proses_bayar').show();
                    } else {
                        Swal.fire('', 'Pemabayaran Belum dilakukan !', 'warning');
                        $('#btn_proses_bayar').hide();
                    }
                }
            })
        }
    </script>
@endsection
