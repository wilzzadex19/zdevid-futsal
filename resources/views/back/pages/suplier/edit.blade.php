@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Edit Suplier </span>

                    {{-- <a href="{{ route('item.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Data</span>
                    </a> --}}
                </div>
                <div class="card-body">
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ session('error') }}</strong>
                        </div>
                    @endif

                    <form method="POST" action="{{ route('suplier.update', $suplier->id) }}" id="userAdd">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Nama
                                    <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" required value="{{ $suplier->nama }}" name="nama"
                                    placeholder="Nama Suplier..." />
                            </div>
                            <div class="form-group">
                                <label>No Telpon
                                    <span class="text-danger">*</span></label>
                                <input type="text" required class="form-control" value="{{ $suplier->no_telpon }}"
                                    name="no_telpon" placeholder="No Telpon..." />
                            </div>
                            <div class="form-group">
                                <label>Alamat
                                    <span class="text-danger">*</span></label>
                                <textarea name="alamat" class="form-control" required cols="30" rows="10">{{ $suplier->alamat }}</textarea>
                            </div>
                            {{-- <div class="form-group">
                                <label>Jumlah
                                    <span class="text-danger">*</span></label>
                                <input type="number" class="form-control" name="jumlah" placeholder="Masukan Jumlah..." />
                            </div> --}}
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        // CKEDITOR.replace('editor_deskripsi');
        // $('#tabelItem').DataTable();
        var runValidator = function() {
            var form = $('#userAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    no_telpon: {
                        minlength: 3,
                        maxlength: 13,
                        digits: true,
                    }
                },
                messages: {

                },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
