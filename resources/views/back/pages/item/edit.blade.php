@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Edit Lapangan </span>
                </div>
                <div class="card-body">
                    <form action="{{ route('item.update',$item->id) }}" method="POST" id="formAddItem" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Kode Item</label>
                            <input type="text"  class="form-control form-control-solid" value="{{ $item->kode }}" required name="kode_item" readonly>
                        </div>
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control form-control" value="{{ $item->name }}" required placeholder="Nama..." name="nama">
                        </div>
                        {{-- <div class="form-group">
                            <label>Kategori {{ $item->category_id }}</label>
                            <select name="kategori_id" required class="form-control">
                                <option value="">- Pilih Kategori</option>
                                @foreach ($category as $items)
                                    <option value="{{ $items->id }}" {{ $items->id == $item->category_id ? 'selected' : '' }}>{{ $items->name }}</option>
                                @endforeach
                            </select>
                        </div> --}}
                        {{-- <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="deskripsi" id="editor_deskripsi" cols="30" rows="10">{{ $item->desc }}</textarea>
                        </div> --}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Gambar </label>
                                    <input type="file" onchange="readFileEdit(this,'preview','{{ asset('img/item/'.$item->foto) }}')" id="input_image"
                                        class="form-control" name="logo" accept="image/*">

                                    <small>Format yang di dukung : jpg,jpeg,png</small> <br>
                                    <small>Ukuran File Maksimal : 2MB</small>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{ asset('img/item/'.$item->foto) }}"
                                        alt="" class="img-thumbnail" id="preview_image">
                                </div>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-success float-right">Simpan</button>
                        <a href="{{ route('item') }}" class="btn btn-secondary float-right">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        CKEDITOR.replace('editor_deskripsi');
        $('#tabelItem').DataTable();
        var runValidator = function() {
            var form = $('#formAddItem');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    // nama_aplikasi: {
                    //     required: true,
                    //     minlength: 3,
                    // },
                    // tentang: {
                    //     required: true,
                    //     minlength: 10,
                    // },
                    // logo: {
                    //     required: true,
                    // },




                },
                // messages: {
                //     username: {
                //         required: "Please enter a username",
                //         minlength: "Your username must consist of at least 3 characters"
                //     },
                //     password: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long"
                //     },
                //     kpassword: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long",
                //         equalTo: "Please enter the same password as above"
                //     },
                // },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
