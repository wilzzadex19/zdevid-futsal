@extends('back.master')
@section('custom-css')
    <style>
        .box {
            height: 20px;
            width: 20px;
            border: 1px solid black;
        }

        .red {
            background-color: red;
        }

        .green {
            background-color: #90EE90;
        }

        .blue {
            background-color: blue;
        }
    </style>
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <table style="width: 100%" border="0">
            <tr>
                <td>
                    <h1 class="h3 mb-0 text-gray-800">Jadwal Lapangan</h1>
                </td>
                <td style="float: right">
                    @if ($week != 7)
                        <a href="{{ route('jadwal-all') }}?week=7" class="btn btn-sm btn-info">
                            Minggu Sekarang</a>
                        <a href="{{ route('jadwal-all') }}?week={{ $week - 7 }}" class="btn btn-sm btn-secondary"><i class="fa fa-angle-left"></i>
                            Prev</a>
                    @endif

                    <a href="{{ route('jadwal-all') }}?week={{ $week + 7 }}" class="btn btn-sm btn-secondary">Next <i
                            class="fa fa-angle-right"></i></a>
                </td>
            </tr>
        </table>
      
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">

        <div class="col-md-12">
            @foreach ($lapangan as $item)
                <div class="feature-matchs">
                    <h5>{{ $item->name }}</h5>
                    <div class="table-resposive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle">Jam</th>
                                    @foreach ($tanggal as $value)
                                        <th
                                            style="background-color: {{ \App\Helper\helper::to_hari(date('D')) == \App\Helper\helper::to_hari($value->format('D')) ? '#90EE90' : '' }}">
                                            {{ \App\Helper\helper::to_hari($value->format('D')) }}
                                            <br>
                                            <small>{{ \App\Helper\helper::tgl_indo($value->format('Y-m-d')) }}</small>
                                        </th>
                                    @endforeach


                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $jadwal = \App\Jadwal::where('item_id', $item->id)
                                        ->groupBy('jam_id')
                                        ->orderBy('jam_id', 'asc')
                                        ->get();
                                @endphp
                                @foreach ($jadwal as $j)
                                    <tr>
                                        <td>{{ $j->jam->display }}</td>
                                        @foreach ($tanggal as $value)
                                            @foreach ($hari as $h)
                                                @if ($h->code == $value->format('D'))
                                                    @php
                                                        
                                                        try {
                                                            $booked = false;
                                                            $cur_date = date('Y-m-d H:i:s');
                                                            $jam_jadwal = $value->format('Y-m-d') . ' ' . $j->jam->jam_awal;
                                                            $btn = 'info';
                                                            if (strtotime($jam_jadwal) < strtotime($cur_date)) {
                                                                $btn = 'info';
                                                            }
                                                        
                                                            $is_booked = \App\Detail_Booking::where([
                                                                'item_id' => $item->id,
                                                                'hari_id' => $h->id,
                                                                'jam_id' => $j->jam_id,
                                                                'tanggal' => $value->format('Y-m-d'),
                                                            ])->first();
                                                        
                                                            if ($is_booked != null) {
                                                                if ($is_booked->parents->status_pembayaran == 'success' || $is_booked->parents->status_pembayaran == 'siap-digunakan') {
                                                                    $booked = true;
                                                                    $btn = 'info';
                                                                }
                                                            }
                                                        } catch (\Throwable $th) {
                                                            //throw $th;
                                                        }
                                                        
                                                    @endphp
                                                    @if (strtotime($jam_jadwal) > strtotime($cur_date))
                                                        <td class="text-center"
                                                            style="background-color: {{ !empty($booked) ? '#0882e5' : (strtotime($jam_jadwal) < strtotime($cur_date) ? '#d65252' : '') }}">
                                                            <span class="badge badge-success"
                                                                data-item_id="{{ $item->id }}"
                                                                data-hari_id="{{ $h->id }}"
                                                                data-jam_id="{{ $j->jam_id }}"
                                                                data-tanggal="{{ $value->format('Y-m-d') }}"
                                                                data-info="{{ $btn }}" onclick="getInfo(this)"
                                                                style="background-color: {{ $btn == 'info' ? 'grey' : 'grey' }};cursor: pointer;">

                                                                @if ($btn == 'info')
                                                                    Info
                                                                @else
                                                                    Info
                                                                @endif
                                                                {{-- {{ dump($is_booked) }} --}}
                                                                {{-- {{ dump($booked) }} --}}
                                                            </span>
                                                        </td>
                                                    @else
                                                        <td class="text-center"
                                                            style="background-color: {{ strtotime($jam_jadwal) < strtotime($cur_date) ? '#d65252' : '' }}">


                                                        </td>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <br>
                    <small>Keterangan :</small>
                    <table border="0">
                        <tr>
                            <td style="vertical-align: middle">
                                <div class='box green'></div>
                            </td>
                            <td> &nbsp; Hari Ini</td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle">
                                <div class='box red'></div>
                            </td>
                            <td> &nbsp; Jam Terlewat</td>
                        </tr>
                        <tr>
                            <td style="vertical-align: middle">
                                <div class='box blue'></div>
                            </td>
                            <td> &nbsp; Telah Di Booking</td>
                        </tr>
                    </table>

                </div>
                <br>
                <br>
            @endforeach
        </div>
    </div>
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">

    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        function getInfo(obj) {
            let item_id = $(obj).attr('data-item_id');
            let hari_id = $(obj).attr('data-hari_id');
            let jam_id = $(obj).attr('data-jam_id');
            let tanggal = $(obj).attr('data-tanggal');
            let info = $(obj).attr('data-info');
            // console.log(info);
            $.ajax({
                type: 'get',
                url: '{{ route('booking.getInfo') }}',
                data: {
                    item_id: item_id,
                    hari_id: hari_id,
                    jam_id: jam_id,
                    tanggal: tanggal,
                    info: info,
                },
                success: function(res) {
                    $('#modalDetail').html(res);
                    $('#modalDetail').modal('show');
                }
            })
        }
    </script>
@endsection
