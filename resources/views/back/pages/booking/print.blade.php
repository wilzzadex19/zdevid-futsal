<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $judul }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        @media print {
            @page {
                size: 25cm 35.7cm;
                margin-top: 0;
                margin-bottom: 0px;
            }

            #data,
            #data th,
            #data td {
                border: 1px solid;
            }

            #data td,
            #data th {
                padding: 5px;
            }

            #data {
                border-spacing: 0px;
                margin-top: 40px;
                font-size: 17px;
            }

            #childTable {
                border: none;
            }

            body {
                padding-top: 10px;
                font-family: sans-serif;
            }
        }
    </style>
</head>

<body>
    <table style="width:100%;margin-top: 50px">
        <tr>
            <td style="width: 100%" colspan="3">
                <div class="row">
                    <div class="col-3 text-center">
                        <img width="60%" src="{{ url('assets/img/' . config('global.app_setting')->app_logo) }}"
                            alt="">
                    </div>
                    <div class="col-8">
                        <div class="row">
                            <div class="col-12">
                                <h4> <strong> {{ config('global.app_setting')->app_name }} </strong></h4>
                                <h5><strong>{!! $judul !!}</strong></h5>
                            </div>
                            <div class="col-12">
                                {{-- No. {{ $data->no_faktur }} --}}
                            </div>
                            <div class="col-12">
                                <span>Jl. Raya Leles No. 8 Garut</span>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-6 text-center">
                        <span>{{ config('global.app_setting')->app_name }}</span>
                    </div>
                </div> --}}
            </td>

        </tr>
    </table>

    <hr style="border: 1px solid black">

    <table id="data" style="width:100%">
        <tr>
            <td class="text-center">NO.</td>
            <td class="text-center">Kode Booking</td>
            <td class="text-center">Penyewa</td>
            <td class="text-center">Nama Tim</td>
            <td class="text-center">Tanggal Transaksi</td>
            <td class="text-center">Tanggal Penyewaan</td>
            <td class="text-center">Lapangan</td>
            <td class="text-center">Lama Pemakaian</td>
            <td class="text-center">Status Pembayaran</td>
            <td class="text-center">Telah Di bayar</td>
            <td class="text-center">Total Harga</td>
        </tr>
        @php
            $total_pemasukan = 0;
            $total_pemasukans = 0;
            $telah_dibayar = 0;
        @endphp
        @foreach ($transaksi as $key => $item)
            @php
                if ($item->jenis_pembayaran == 'dp') {
                    if ($item->is_lunas == 1) {
                        $telah_dibayar += $item->total_harga;
                    } else {
                        $telah_dibayar += $item->total_dp;
                    }
                } else {
                    $telah_dibayar += $item->total_harga;
                }
                $total_pemasukans += $item->total_harga;
            @endphp
            <tr>
                <td class="text-center">{{ $key + 1 }}</td>
                <td class="text-center">{{ $item->kode }}</td>
                <td class="text-center">{{ $item->users->name }}</td>
                <td class="text-center">{{ $item->team_name }}</td>
                <td class="text-center">{{ \App\Helper\helper::tgl_indo_jam($item->created_at) }}</td>
                <td class="text-center">{{ \App\Helper\helper::tgl_indo($item->detail[0]->tanggal) }} -
                    ({{ date('H:i', strtotime($item->detail[0]->jam->jam_awal)) }} -
                    {{ date('H:i', strtotime($item->detail[count($item->detail) - 1]->jam->jam_akhir)) }})</td>
                <td class="text-center">{{ $item->detail[0]->item->name }}</td>
                <td class="text-center">{{ count($item->detail) }} Jam</td>
                <td class="text-center">
                    @if ($item->status_pembayaran == 'pending')
                        Menunggu
                        Pembayaran
                    @elseif($item->status_pembayaran == 'siap-digunakan')
                        Siap Digunakan
                    @elseif($item->status_pembayaran == 'success')
                        @if ($item->jenis_pembayaran == 'dp')
                            @if ($item->is_lunas == 1)
                                Pembayaran Pelunasan Sukses
                            @else
                                Pembayaran DP Sukses
                            @endif
                        @else
                            Pembayaran Full Sukses
                        @endif
                    @elseif($item->status_pembayaran == 'unfinish')
                        Transaksi
                        Dibatalkan
                    @elseif($item->status_pembayaran == 'selesai')
                        Selesai
                    @endif

                    @if ($item->jenis_pembayaran == 'full')
                        {{-- Rp. {{ number_format($item->total_harga) }} --}}
                    @else
                        <br>
                        {{-- Rp. {{ number_format($item->total_harga) }} <br> DP 20% = Rp.
                        {{ number_format($item->total_dp) }} --}}
                        {!! $item->is_lunas == 1 ? '(Lunas)' : '' !!}
                        {!! $item->is_lunas == 1 ? '' : '(Tidak Melakukan Pelunasan)' !!}
                    @endif
                </td>
                <td class="text-center">
                    @if ($item->jenis_pembayaran == 'dp')
                        @if ($item->is_lunas == 1)
                            Rp. {{ number_format($item->total_harga) }}
                        @else
                            Rp. {{ number_format($item->total_dp) }}
                        @endif
                    @else
                        Rp. {{ number_format($item->total_harga) }}
                    @endif


                </td>
                <td class="text-center">
                    {{-- @if ($item->jenis_pembayaran == 'full')
                        Rp. {{ number_format($item->total_harga) }}
                    @else
                        Rp. {{ number_format($item->total_harga) }} <br> DP 20% = Rp.
                        {{ number_format($item->total_dp) }} {!! $item->is_lunas == 1 ? '(Lunas)' : '' !!}
                        {!! $item->is_lunas == 1 ? '' : '(Tidak Melakukan Pelunasan)' !!}
                    @endif --}}
                    Rp. {{ number_format($item->total_harga) }}
                </td>

                {{-- <td class="text-center">{{ number_format($item->grand_total) }}</td> --}}
            </tr>
        @endforeach
        <tr>
            <td colspan="9"><strong>Total Harga</strong></td>
            {{-- <td style="text-align: center"> <strong>Rp. {{ number_format($telah_dibayar) }} </strong></td> --}}
            <td style="text-align: center" colspan="2"> <strong>Rp. {{ number_format($total_pemasukans) }} </strong>
            </td>
        </tr>
        <tr>
            <td colspan="9"><strong>Total Pendapatan</strong></td>
            <td style="text-align: center" colspan="2"> <strong>Rp. {{ number_format($telah_dibayar) }} </strong>
            </td>
            {{-- <td style="text-align: center"> <strong>Rp. {{ number_format($total_pemasukans) }} </strong></td> --}}
        </tr>


        {{-- <tr>
            <td colspan="5">Total Diskon</td>
            <td style="text-align: right">{{  number_format($data->total_diskon)  }}</td>
        </tr>
        <tr>
            <td colspan="5">Pajak</td>
            <td style="text-align: right">{{  number_format($data->pajak)  }}</td>
        </tr>
        <tr>
            <td colspan="5">Grand Total</td>
            <td style="text-align: right">{{  number_format($data->grand_total)  }}</td>
        </tr> --}}
        {{-- @php
            $total = 0;
        @endphp
        @foreach ($detail as $key => $item)
        @php
            $total+=$item->total;
        @endphp
            <tr>
                <td>{{ $key + 1 }}</td>
                <td class="text-center">{{ $item->barang->nama_barang }}</td>
                <td class="text-center">Rp. {{ number_format($item->barang->harga) }}</td>
                <td class="text-center">{{ $item->qty }}</td>
                <td class="text-center">Rp .{{ number_format($item->total) }}</td>
            </tr>
        @endforeach

        <tr>
            <td colspan="3"><b>Grand Total</b></td>
            <td colspan="2" class="text-center"><b>Rp. {{ number_format($total) }}</b></td>
        </tr> --}}

        {{-- @foreach ($detail_penjualan as $key => $item)
        <tr>
            <td>{{ $item->obat->nama_obat }}</td>
            <td class="text-center">{{ $item->diskon }} %</td>
            <td class="text-center">{{ $item->jumlah_obat }} {{ $item->unit->nama }}</td>
            <td class="text-center">{{ number_format($item->harga) }}</td>
            <td class="text-center">{{ number_format($item->subtotal) }}</td>
        </tr>
        @endforeach
        <tr>
            <th class="text-center">TOTAL 1</th>
            <th class="text-center">POT PENJUALAN</th>
            <th class= "text-center">TOTAL 2</th>
            <th class="text-center">PPN</th>
            <th colspan="2" class="text-center">JUMLAH TAGIHAN</th>
        </tr>
        <tr>
            <th class="text-center">{{ number_format($penjualan->total_1) }}</th>
            <th class="text-center">{{ number_format($penjualan->pot_pen) }}</th>
            <th class= "text-center">{{ number_format($penjualan->total_1 - $penjualan->pot_pen) }}</th>
            <th class="text-center">{{ number_format(($penjualan->pajak / 100) * $penjualan->jumlah_tagihan) }}</th>
            <th colspan="2" class="text-center">{{ number_format($penjualan->total_1 - $penjualan->pot_pen) }}</th>
        </tr>
        <tr>
            <td colspan="6">Terbilang : {{ $penjualan->terbilang }}</td>
        </tr> --}}

    </table>

    <br>
    <table border="0" style="width:100%">
        @php
            $admins = \App\User::where('role', 'admin')->first();
        @endphp
        <tr>
            <td class="text-right"><strong> Penanggung Jawab </strong></td>



        </tr>
        <tr>
            <td><br><br></td>
        </tr>
        <tr>
            <td class="text-right"><strong>{{ $admins->name }}</strong></td>
        </tr>
    </table>

    <script>
        // $(document).ready(function() {
        // var css = '@page { size: landscape; }',
        //     head = document.head || document.getElementsByTagName('head')[0],
        //     style = document.createElement('style');

        // style.type = 'text/css';
        // style.media = 'print';

        // if (style.styleSheet) {
        //     style.styleSheet.cssText = css;
        // } else {
        //     style.appendChild(document.createTextNode(css));
        // }

        // head.appendChild(style);

        window.print();
        // })
    </script>
</body>

</html>
