@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Booking</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Daftar Booking </span>

                    {{-- <a href="{{ route('item.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Data</span>
                    </a> --}}
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tabelItem">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle">No</th>
                                    <th style="vertical-align: middle">
                                        {{ $status != 'unfinish' ? 'Kode Booking' : 'Tanggal Transaksi' }}</th>
                                    <th style="vertical-align: middle">Penyewa</th>
                                    <th style="vertical-align: middle">Nama Tim</th>
                                    <th style="vertical-align: middle">Tanggal Transaksi</th>
                                    <th style="vertical-align: middle">Tanggal Penyewaan</th>
                                    <th style="vertical-align: middle">Status</th>
                                    @if ($status == 'pending')
                                        <th style="vertical-align: middle">Batas Pembayaran</th>
                                    @endif
                                    <th style="vertical-align: middle;min-width:150px">Total Harga</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($booking as $key => $item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $status != 'unfinish' ? $item->kode : \App\Helper\helper::tgl_indo_jam($item->created_at) }}
                                        </td>
                                        <td>{{ $item->users->name }}</td>
                                        <td>{{ $item->team_name }}</td>
                                        <td>{{ \App\Helper\helper::tgl_indo_jam($item->created_at) }}</td>
                                        <td>{{ \App\Helper\helper::tgl_indo($item->detail[0]->tanggal) }}
                                            - ({{ $item->detail[0]->jam->jam_awal }} - {{ $item->detail[count($item->detail) - 1]->jam->jam_akhir }})</td>
                                        @if ($item->status_pembayaran == 'pending')
                                            <td><span class="badge badge-warning"
                                                    style="background-color: rgb(152, 149, 74)">Menunggu
                                                    Pembayaran</span> <br>(Bayar Sebelum
                                                {{ \App\Helper\helper::tgl_indo_jam($item->payment_exp) }})
                                            </td>
                                        @elseif($item->status_pembayaran == 'success' || $item->status_pembayaran == 'siap-digunakan')
                                            <td><span class="badge badge-success" style="background-color: green">
                                                    @if ($item->jenis_pembayaran == 'dp')
                                                        @if ($item->is_lunas == 1)
                                                            Pembayaran Pelunasan Sukses
                                                        @else
                                                            Pembayaran DP Sukses
                                                        @endif
                                                    @else
                                                        Pembayaran Full Sukses
                                                    @endif
                                                </span></td>
                                        @elseif($item->status_pembayaran == 'unfinish')
                                            <td><span class="badge badge-success" style="background-color: red">Transaksi
                                                    Dibatalkan</span></td>
                                        @elseif($item->status_pembayaran == 'selesai')
                                            <td><span class="badge badge-success"
                                                    style="background-color: blue">Selesai</span></td>
                                        @endif
                                        @if ($status == 'pending')
                                            <td>{{ \App\Helper\helper::tgl_indo_jam($item->payment_exp) }}</td>
                                        @endif
                                        <td>
                                            @if ($item->jenis_pembayaran == 'full')
                                                Total Rp. {{ number_format($item->total_harga) }}
                                            @else
                                                Rp. {{ number_format($item->total_harga) }} <br> DP = Rp.
                                                {{ number_format($item->total_dp) }} {!! $item->is_lunas == 1 ? '<span class="badge badge-success">Lunas</span>' : '' !!}
                                                {!! $item->keterangan_lunas == 'Tidak Lunas' ? '<span class="badge badge-danger">Tidak Melakukan Pelunasan</span>' : '' !!}
                                            @endif
                                        </td>
                                        <td>
                                            @if (Auth::user()->role != 'owner')
                                                @if ($item->status_pembayaran == 'pending')
                                                @elseif($item->status_pembayaran == 'siap-digunakan')
                                                    {{-- <a href="javascript:void(0)"
                                                        data-url="{{ route('dataBooking.selesaikan', $item->id) }}"
                                                        onclick="selesaikan(this)" class="btn btn-success btn-circle btn-sm"
                                                        data-toggle="tooltip" data-placement="top"
                                                        title="Lunasi Pembayaran">
                                                        <i class="fas fa-check"></i>
                                                    </a> --}}
                                                @elseif($item->status_pembayaran == 'success')
                                                    @if ($item->jenis_pembayaran == 'dp')
                                                        @if ($item->is_lunas != 1)
                                                            <a href="javascript:void(0)" data-id="{{ $item->id }}"
                                                                onclick="lunasi(this)"
                                                                class="btn btn-secondary btn-circle btn-sm"
                                                                data-toggle="tooltip" data-placement="top"
                                                                title="Lunasi Pembayaran">
                                                                <i class="fas fa-money-check"></i>
                                                            </a>
                                                        @else
                                                            {{-- <a href="javascript:void(0)" onclick="siap(this)"
                                                                data-url="{{ route('dataBooking.siap', $item->id) }}"
                                                                class="btn btn-success btn-circle btn-sm"
                                                                data-toggle="tooltip" data-placement="top"
                                                                title="Lunasi Pembayaran">
                                                                <i class="fas fa-check"></i>
                                                            </a> --}}
                                                        @endif
                                                    @else
                                                        {{-- <a href="javascript:void(0)"
                                                            data-url="{{ route('dataBooking.siap', $item->id) }}"
                                                            onclick="siap(this)" class="btn btn-success btn-circle btn-sm"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="Lunasi Pembayaran">
                                                            <i class="fas fa-check"></i>
                                                        </a> --}}
                                                    @endif
                                                @endif
                                            @endif
                                            <a href="javascript:void(0)" onclick="lihatDetail(this)"
                                                id="{{ $item->id }}" class="btn btn-warning btn-circle btn-sm">
                                                <i class="fas fa-list"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="modalLunasi" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">

    </div>

    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">

    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        $('#tabelItem').DataTable();

        function lihatDetail(obj) {
            let id = $(obj).attr('id');
            $.ajax({
                url: '{{ route('dataBooking.detail') }}',
                type: 'get',
                data: {
                    id: id
                },
                success: function(res) {
                    $('#modalDetail').html(res);
                    $('#modalDetail').modal('show');
                }
            })
        }

        function proses(obj) {
            let url = $(obj).attr('data-url');
            Swal.fire({
                title: 'Yakin ?',
                text: "akan melanjutkan proses ini",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }

        function siap(obj) {
            let url = $(obj).attr('data-url');
            Swal.fire({
                title: 'Yakin ?',
                text: "akan melanjutkan proses ini ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }

        function selesaikan(obj) {
            let url = $(obj).attr('data-url');
            Swal.fire({
                title: 'Yakin ?',
                text: "akan selesaikan proses ini",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }

        function lunasi(obj) {
            let id = $(obj).attr('data-id');

            $.ajax({
                url: '{{ route('dataBooking.lunasi') }}',
                type: 'get',
                data: {
                    id: id
                },
                success: function(res) {
                    $('#modalLunasi').html(res);
                    $('#modalLunasi').modal('show');
                }
            })
        }

        function cekMetode(obj) {
            let metode = $(obj).val();
            if (metode != null) {
                $('#btn_proses_bayar').hide();
                $('#btn_link_bayar').hide();
                $('#btn_cek_status').hide();
                if (metode == 'cash') {
                    $('#btn_proses_bayar').show();
                } else if (metode == 'virtual') {
                    $('#btn_link_bayar').show();
                    $('#btn_cek_status').show();
                }
            }
        }

        function cekStatus(obj) {
            let id = $(obj).attr('data-id');

            $.ajax({
                url: '{{ route('dataBooking.cekStatus') }}',
                type: 'get',
                data: {
                    id: id
                },
                success: function(res) {
                    if (res.status == 1) {
                        Swal.fire('Sukses', 'Pembayaran Berhasil !', 'success').then(function() {
                            window.location.reload();
                        });

                        // $('#btn_proses_bayar').show();
                    } else {
                        Swal.fire('', 'Pembayaran Belum dilakukan !', 'warning');
                        $('#btn_proses_bayar').hide();
                    }
                }
            })
        }

        var runValidator = function() {
            var form = $('#formAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    nama_aplikasi: {
                        required: true,
                        minlength: 3,
                    },
                    tentang: {
                        required: true,
                        minlength: 10,
                    },
                    // logo: {
                    //     required: true,
                    // },




                },
                // messages: {
                //     username: {
                //         required: "Please enter a username",
                //         minlength: "Your username must consist of at least 3 characters"
                //     },
                //     password: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long"
                //     },
                //     kpassword: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long",
                //         equalTo: "Please enter the same password as above"
                //     },
                // },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
