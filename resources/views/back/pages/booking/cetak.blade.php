@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        {{-- <h1 class="h3 mb-0 text-gray-800">Cetak Laporan Penggunaan Lapangan</h1> --}}
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Cetak Laporan Penggunaan Lapangan </span>

                    {{-- <a href="{{ route('item.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Data</span>
                    </a> --}}
                </div>
                <div class="card-body">
                    <form action="{{ route('dataBooking.cetak-proses') }}" target="_blank" method="POST">
                        @csrf

                        <label>Pilih Periode</label>
                        <div id="reportrange"
                            style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                        <input type="hidden" name="tanggal" id="tanggal">
                        <br>

                        <button type="submit" class="btn btn-primary">Cetak</button>

                    </form>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="modalLunasi" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">

    </div>

    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">

    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        $('#tabelItem').DataTable();

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#tanggal').val(start.format('YYYY-MM-DD') + '/' + end.format('YYYY-MM-DD'))
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month')
                    .endOf(
                        'month')
                ]
            }
        }, cb);

        cb(start, end);

        function lihatDetail(obj) {
            let id = $(obj).attr('id');
            $.ajax({
                url: '{{ route('dataBooking.detail') }}',
                type: 'get',
                data: {
                    id: id
                },
                success: function(res) {
                    $('#modalDetail').html(res);
                    $('#modalDetail').modal('show');
                }
            })
        }

        function proses(obj) {
            let url = $(obj).attr('data-url');
            Swal.fire({
                title: 'Yakin ?',
                text: "akan melanjutkan proses ini",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }

        function siap(obj) {
            let url = $(obj).attr('data-url');
            Swal.fire({
                title: 'Yakin ?',
                text: "akan melanjutkan proses ini ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }

        function selesaikan(obj) {
            let url = $(obj).attr('data-url');
            Swal.fire({
                title: 'Yakin ?',
                text: "akan selesaikan proses ini",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = url;
                }
            })
        }

        function lunasi(obj) {
            let id = $(obj).attr('data-id');

            $.ajax({
                url: '{{ route('dataBooking.lunasi') }}',
                type: 'get',
                data: {
                    id: id
                },
                success: function(res) {
                    $('#modalLunasi').html(res);
                    $('#modalLunasi').modal('show');
                }
            })
        }

        function cekMetode(obj) {
            let metode = $(obj).val();
            if (metode != null) {
                $('#btn_proses_bayar').hide();
                $('#btn_link_bayar').hide();
                $('#btn_cek_status').hide();
                if (metode == 'cash') {
                    $('#btn_proses_bayar').show();
                } else if (metode == 'virtual') {
                    $('#btn_link_bayar').show();
                    $('#btn_cek_status').show();
                }
            }
        }

        function cekStatus(obj) {
            let id = $(obj).attr('data-id');

            $.ajax({
                url: '{{ route('dataBooking.cekStatus') }}',
                type: 'get',
                data: {
                    id: id
                },
                success: function(res) {
                    if (res.status == 1) {
                        Swal.fire('Sukses', 'Pemabayaran Berhasil !', 'success').then(function() {
                            window.location.reload();
                        });

                        // $('#btn_proses_bayar').show();
                    } else {
                        Swal.fire('', 'Pemabayaran Belum dilakukan !', 'warning');
                        $('#btn_proses_bayar').hide();
                    }
                }
            })
        }

        var runValidator = function() {
            var form = $('#formAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    nama_aplikasi: {
                        required: true,
                        minlength: 3,
                    },
                    tentang: {
                        required: true,
                        minlength: 10,
                    },
                    // logo: {
                    //     required: true,
                    // },




                },
                // messages: {
                //     username: {
                //         required: "Please enter a username",
                //         minlength: "Your username must consist of at least 3 characters"
                //     },
                //     password: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long"
                //     },
                //     kpassword: {
                //         required: "Please provide a password",
                //         minlength: "Your password must be at least 5 characters long",
                //         equalTo: "Please enter the same password as above"
                //     },
                // },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
