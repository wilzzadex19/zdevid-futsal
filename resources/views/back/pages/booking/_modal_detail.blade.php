<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Detail Booking</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('booking.pay') }}" method="POST">
                @csrf
                {{-- <input type="hidden" name="item_id" value="{{ $jadwal->item_id }}">
                <input type="hidden" name="hari_id" value="{{ $jadwal->hari_id }}">
                <input type="hidden" name="jam_id" value="{{ $jadwal->jam_id }}">
                <input type="hidden" name="tanggal" value="{{ $tanggal }}"> --}}
                <div class="form-group">
                    <label>Booking ID</label>
                    <input type="text" readonly value="{{ $booking->kode }}" class="form-control">
                </div>
                {{-- <div class="form-group">
                    <label>Bayar Sebelum</label>
                    <input type="text" readonly value="{{ \App\Helper\helper::tgl_indo_jam($booking->payment_exp) }}"
                        class="form-control">
                </div> --}}
                {{-- <div class="form-group">
                    <label>Booking ID</label>
                    <input type="text" readonly value="{{ $booking->kode }}" class="form-control">
                </div> --}}
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" readonly value="{{ Auth::user()->name }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>Nama Tim</label>
                    <input type="text" required name="team_name" readonly value="{{ $booking->team_name }}"
                        class="form-control">
                </div>
                <div class="form-group">
                    <label>Tanggal Transaksi</label>
                    <input type="text" required name="team_name" readonly
                        value="{{ \App\Helper\helper::tgl_indo($booking->detail[0]->tanggal) }}" class="form-control">
                </div>
                <div class="form-group">
                    <label>Tanggal Penyewaan</label>
                    <input type="text" required name="team_name" readonly
                        value="{{ \App\Helper\helper::tgl_indo($booking->detail[0]->tanggal) }} - {{ date('H:i', strtotime($booking->detail[0]->jam->jam_awal)) }}"
                        class="form-control">
                </div>
                <div class="form-group">
                    {{-- <label>Durasi</label> --}}
                    <table style="width: 100%" class="table table-bordered">
                        <tr>
                            <td>Lapangan</td>
                            <td>Jam</td>
                            <td>Total Durasi</td>
                        </tr>
                        @foreach ($booking->detail as $key => $item)
                            <tr>
                                <td>{{ $item->item->name }}</td>
                                <td>{{ $item->jam->display }}</td>
                                @if ($key == 0)
                                    <td rowspan="{{ count($booking->detail) }}">{{ count($booking->detail) }} Jam
                                    </td>
                                @endif

                            </tr>
                        @endforeach
                    </table>
                </div>

                <div class="form-group">
                    <label>No Hp Penanggung Jawab</label>
                    <input type="text" required name="no_hp" readonly value="{{ $booking->no_hp }}"
                        class="form-control">
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <textarea name="keterangan" readonly class="form-control" cols="30" rows="5">{{ $booking->keterangan }}</textarea>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label>Pembayaran</label>
                        <select disabled name="jenis_pembayaran" id="jenis_pembayaran" class="form-control">
                            <option value="full" {{ $booking->jenis_pembayaran == 'full' ? 'selected' : '' }}>
                                Full</option>
                            <option value="dp" {{ $booking->jenis_pembayaran == 'dp' ? 'selected' : '' }}>DP
                                (20% Total Harga)</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>{{ $booking->jenis_pembayaran == 'dp' ? 'Pembayaran DP' : 'Pembayaran Full' }}</label>
                        <input type="text" readonly name="jumlah_bayar" id="jumlah_bayar"
                            value="{{ $booking->jenis_pembayaran == 'dp' ?  number_format($booking->total_dp) : number_format($booking->total_harga)   }}" name="total_harga"
                            class="form-control">
                    </div>
                </div>
                @if ($booking->jenis_pembayaran == 'dp')
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Sisa Pembayaran {{ $booking->is_lunas == 1 ? '(Lunas)' : '' }}</label>
                            <input type="text" readonly 
                                value="{{ $booking->is_lunas == 1 ? 0 : number_format($booking->total_harga - $booking->total_dp) }}"
                                class="form-control">
                        </div>
                        <div class="col-md-6">
                            <label>Jumlah Pelunasan</label>
                            <input type="text" readonly 
                                value="{{ $booking->is_lunas != 1 ? 0 : number_format($booking->total_harga - $booking->total_dp) }}"
                                class="form-control">
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <label>Total Biaya Sewa Lapangan</label>
                    <input type="text" required name="no_hp" readonly
                        value="{{ number_format($booking->total_harga) }}" class="form-control">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            {{-- <button type="button" class="btn btn-primary">Save</button> --}}
        </div>
    </div>
</div>
