<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $judul }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        @media print {
            @page {
                size: 25cm 35.7cm;
                margin-top: 0;
                margin-bottom: 0px;
            }

            #data,
            #data th,
            #data td {
                border: 1px solid;
            }

            #data td,
            #data th {
                padding: 5px;
            }

            #data {
                border-spacing: 0px;
                margin-top: 40px;
                font-size: 17px;
            }

            #childTable {
                border: none;
            }

            body {
                padding-top: 10px;
                font-family: sans-serif;
            }
        }

    </style>
</head>

<body>
    <table style="width:100%;margin-top: 50px">
        <tr>
            <td style="width: 100%" colspan="3">
                <div class="row">
                    <div class="col-3 text-center">
                        <img width="60%" src="{{ url('assets/img/' . config('global.app_setting')->app_logo) }}"
                            alt="">
                    </div>
                    <div class="col-8">
                        <div class="row">
                            <div class="col-12">
                                <h4> <strong> {{ config('global.app_setting')->app_name }} </strong></h4>
                                <h5><strong>{!! $judul !!}</strong></h5>
                            </div>
                            <div class="col-12">
                                {{-- No. {{ $data->no_faktur }} --}}
                            </div>
                            <div class="col-12">
                                <span>Jl. Raya Leles No. 8 Garut</span>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="row">
                    <div class="col-6 text-center">
                        <span>{{ config('global.app_setting')->app_name }}</span>
                    </div>
                </div> --}}
            </td>

        </tr>
    </table>

    <hr style="border: 1px solid black">

    <table id="data" style="width:100%">
        {{-- <tr>
            <td colspan="2" class="text-left"> <strong>Sewa Lapangan</strong> </td>
        </tr> --}}
        {{-- <tr>
            <td class="text-left"> Pendapatan Penyewaan </td>
            <td class="text-center"> Rp. {{ number_format($sisa_pembayaran) }} </td>
        </tr>
        <tr>
            <td class="text-left"> Menunggu Pembayaran </td>
            <td class="text-center"> Rp. {{ number_format($menunggu_pembayaran) }} </td>
        </tr> --}}
        <tr>
            <td class="text-left"> Pendapatan Penyewaan </td>
            <td class="text-center"> Rp. {{ number_format($total_sewa_lapang) }} </td>
    
        <tr>
            <td class="text-left"> Pendapatan Penjualan Barang </td>
            <td class="text-center"> Rp. {{ number_format($barang_keluar) }} </td>
        </tr>
        <tr>
            <td class="text-left"> Pengeluaran Pembelian Barang </td>
            <td class="text-center"> Rp. {{ number_format($barang_masuk) }} </td>
        </tr>
        <tr>
            @php
                $all = $total_sewa_lapang + ($barang_keluar - $barang_masuk);
            @endphp
            <td class="text-left"> <strong>Total Pendapatan Keseluruhan</strong> </td>
            
            <td class="text-center "> <strong class="{{ $all < 0 ? 'text-danger' : '' }}"> Rp. {{ number_format($all) }} {{ $all < 0 ? '(-)' : '' }}</strong>
            </td>
        </tr>

    </table>

    <script>
        // $(document).ready(function() {
            // var css = '@page { size: landscape; }',
            //     head = document.head || document.getElementsByTagName('head')[0],
            //     style = document.createElement('style');

            // style.type = 'text/css';
            // style.media = 'print';

            // if (style.styleSheet) {
            //     style.styleSheet.cssText = css;
            // } else {
            //     style.appendChild(document.createTextNode(css));
            // }

            // head.appendChild(style);

            window.print();
        // })
    </script>
</body>

</html>
