<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Lunasi Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <label>Kode Booking</label>
                    <input type="text" readonly class="form-control"  value="{{ $booking->kode }}">
                </div>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" readonly class="form-control"  value="{{ $booking->users->name }}">
                </div>
                <div class="form-group">
                    <label>Total Harga</label>
                    <input type="text" readonly class="form-control"  value="{{ number_format($booking->total_harga) }}">
                </div>
                <div class="form-group">
                    <label>DP</label>
                    <input type="text" readonly class="form-control"  value="{{ number_format($booking->total_dp) }}">
                </div>
                <div class="form-group">
                    <label>Sisa Pembayaran</label>
                    <input type="text" readonly class="form-control"  value="{{ number_format($booking->total_harga - $booking->total_dp) }}">
                </div>
                <div class="form-group">
                    <label>Metode Pembayaran</label>
                    <select name="metode" onchange="cekMetode(this)" id="metode" class="form-control">
                        <option value="">- Pilih Metode -</option>
                        <option value="cash" selected>Cash</option>
                        {{-- <option value="virtual">Virtual</option> --}}
                    </select>
                </div>
                <div class="form-group">
                    <a href="{{ route('dataBooking.virtual',$booking->id) }}" target="_blank" style="display: none" class="btn btn-success btn-sm btn-block" id="btn_link_bayar">Proses Pembayaran</a>
                    <button type="button" style="display: none" onclick="cekStatus(this)" data-id="{{ $booking->id }}" class="btn btn-warning btn-sm btn-block" id="btn_cek_status">Cek Status Pembayaran</button>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="button" id="btn_proses_bayar" onclick="proses(this)" data-url="{{ route('dataBooking.proses',$booking->id)  }}" style="display: " class="btn btn-primary">Proses</button>
        </div>
    </div>
</div>