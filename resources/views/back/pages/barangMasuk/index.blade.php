@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Pembelian Barang</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Daftar Pembelian Barang</span>
                    <div class="div">
                        @if (Auth::user()->role != 'owner')
                            <a href="{{ route('barangMasuk.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-plus"></i>
                                </span>
                                <span class="text">Tambah Data</span>
                            </a>
                        @endif
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#modalCetak"
                            class="btn btn-warning btn-sm btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-print"></i>
                            </span>
                            <span class="text">Cetak Laporan</span>
                        </a>
                    </div>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="user_table">
                            <thead>
                                <tr>
                                    <th width="10px">No.</th>
                                    <th>No Struk Pembelian</th>
                                    <th>Tanggal Pembelian</th>
                                    <th>Suplier</th>
                                    <th>Sub Total</th>
                                    <th>Total Diskon</th>
                                    {{-- <th>Pajak</th> --}}
                                    <th>Grand Total</th>
                                    <th style="min-width: 150px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $key => $item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->no_faktur }}</td>
                                        <td>{{ \App\Helper\helper::tgl_indo($item->tgl_faktur) }}</td>
                                        <td>{{ $item->suplier->nama }}</td>
                                        <td>Rp. {{ number_format($item->sub_total) }}</td>
                                        <td>Rp. {{ number_format($item->total_diskon) }}</td>
                                        {{-- <td>{{ $item->pajak }}</td> --}}
                                        <td>Rp. {{ number_format($item->grand_total) }}</td>
                                        <td>
                                            <button onclick="detail(this)" id="{{ $item->id }}"
                                                class="btn btn-info btn-sm btn-block"><i class="fa fa-list"></i>
                                                Detail</button>
                                            <a href="{{ route('barangMasuk.print', $item->id) }}" target="_blank"
                                                class="btn btn-warning btn-sm btn-block"><i class="fa fa-print"></i>Print
                                                Struk</a>
                                            <a href="{{ url('img/', $item->file_faktur_suplier) }}" target="_blank"
                                                class="btn btn-primary btn-sm btn-block"><i class="fa fa-print"></i>Print
                                                Struk Suplier</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">

    </div>

    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="modalCetak" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Laporan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('barangMasuk.cetak') }}" target="_blank" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-group">
                                <label>Suplier</label> <br>
                                <select name="suplier_id" class="form-control" style="width: 100%" id="suplier_id"> 
                                    <option value="">Semua Suplier</option>
                                    @foreach ($suplier as $suplier)
                                        <option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Pilih Periode</label>
                            <div id="reportrange"
                                style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                                <i class="fa fa-calendar"></i>&nbsp;
                                <span></span> <i class="fa fa-caret-down"></i>
                            </div>
                            <input type="hidden" name="tanggal" id="tanggal">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Cetak</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        $('#user_table').DataTable();

        $('.select2').select2()

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#tanggal').val(start.format('YYYY-MM-DD') + '/' + end.format('YYYY-MM-DD'))
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month')
                    .endOf(
                        'month')
                ]
            }
        }, cb);

        cb(start, end);

        function detail(obj) {
            let id = $(obj).attr('id');
            $.ajax({
                url: '{{ route('barangMasuk.detail') }}',
                type: 'get',
                data: {
                    id: id,
                },
                success: function(res) {
                    $('#modalDetail').html(res);
                    $('#modalDetail').modal('show');
                }
            })
        }
    </script>
@endsection
