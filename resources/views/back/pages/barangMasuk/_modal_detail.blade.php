<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Detail Pembelian Barang</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <table style="width: 100%" border="0">
                <tr>
                    <th style="max-width: 70px">No Struk Pembelian</th>
                    <th> : {{ $data->no_faktur }}</th>
                </tr>
                <tr>
                    <th style="max-width: 20px">Tanggal Pembelian</th>
                    <th> : {{ \App\Helper\helper::tgl_indo($data->tgl_faktur) }}</th>
                </tr>
                <tr>
                    <th style="max-width: 20px">Suplier</th>
                    <th> : {{ $data->suplier->nama }}</th>
                </tr>
            </table>
            <br>
            <label>List Barang</label>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Kategori</th>
                        <th>Jenis</th>
                        <th>Merk</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Diskon (%)</th>
                        <th>Total Harga</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data->detail as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->barang->kategori }}</td>
                            <td>{{ $item->barang->jenis->nama }}</td>
                            <td>{{ $item->barang->merk->nama }}</td>
                            <td>{{ number_format($item->barang->harga) }}</td>
                            <td>{{ $item->jumlah }}</td>
                            <td>{{ $item->diskon }}</td>
                            <td>{{ number_format($item->total_harga) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br>
            <table style="width: 100%" border="0">
                <tr>
                    <th style="max-width: 20px">Sub Total</th>
                    <th> : {{ number_format($data->sub_total) }}</th>
                </tr>
                <tr>
                    <th style="max-width: 20px">Total Diskon</th>
                    <th> : {{ number_format($data->total_diskon) }}</th>
                </tr>
                {{-- <tr>
                    <th style="max-width: 20px">Pajak</th>
                    <th> : {{ number_format($data->pajak) }}</th>
                </tr> --}}
                <tr>
                    <th style="max-width: 20px">Grand Total</th>
                    <th> : {{ number_format($data->grand_total) }}</th>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            {{-- <button type="button" class="btn btn-primary">Save</button> --}}
        </div>
    </div>
</div>
