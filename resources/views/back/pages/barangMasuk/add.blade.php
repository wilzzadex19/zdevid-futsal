@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Tambah Pembelian Barang </span>

                    {{-- <a href="{{ route('item.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Data</span>
                    </a> --}}
                </div>
                <div class="card-body">
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ session('error') }}</strong>
                        </div>
                    @endif

                    <form action="{{ route('barangMasuk.store') }}" method="POST" enctype="multipart/form-data" id="userAdd">
                        @csrf
                        <div class="form-group">
                            <label>No Faktur</label>
                            <input type="text" name="no_faktur" value="{{ $kode }}" readonly class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Masuk</label>
                            <input type="date" name="tgl_masuk" class="form-control" required>
                        </div>
                       
                        <div class="form-group">
                            <label>Suplier</label>
                            <select name="suplier_id" class="form-control select2" required id="suplier_id">
                                <option value="">- Pilih Suplier -</option>
                                @foreach ($suplier as $suplier)
                                    <option value="{{ $suplier->id }}">{{ $suplier->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">

                            <button class="btn btn-sm btn-success float-right mb-2" type="button" data-toggle="modal"
                                data-target="#modalAdd">Tambah Barang</button>
                            <input type="text" style="display: none" name="jml_barang" readonly required id="jml"
                                class="form-control">
                            <div class="table-reponsive" id="renderTabel">

                            </div>

                        </div>
                        <div class="form-group">
                            <label>Faktur Dari Suplier</label>
                            <input type="file" name="file_faktur" required accept="image/jpg,image/png,image/jpeg" class="form-control">
                            <small>Format yang di dukung : jpg/jpeg/png</small>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <label for="">Sub Total</label>
                                <input type="text" name="sub_total" id="sub_total" required value="0" readonly class="form-control">
                            </div>
                            <div class="col-md-4">
                                <label for="">Total Diskon</label>
                                <input type="text" name="total_diskon" id="total_diskon" value="0" required readonly class="form-control">
                            </div>
                            <div class="col-md-3" style="display: none">
                                <label for="">Pajak (%)</label>
                                <input type="number" name="pajak" onchange="getDiskon(this)" id="pajak" required value="0" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <label for="">Grand Total</label>
                                <input type="number" name="grand_total" id="grand_total" value="0" required readonly value="0" class="form-control">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary float-right">Simpan </button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="formAddTemp">
                    <div class="modal-body">
                        <div class="container-fluid">

                            @csrf
                            <div class="form-group">
                                <label for="">Barang</label> <br>
                                <select name="barang_id" id="barang_id" style="width: 100%" class="form-control select2"
                                    required>
                                    <option value="">- Pilih Barang</option>
                                    @foreach ($barang as $item)
                                        <option value="{{ $item->id }}" data-harga="{{ $item->harga }}">
                                          {{ $item->kategori }}  {{ $item->jenis->nama }} Merk {{ $item->merk->nama }} - {{ $item->jumlah == null ? '0' : $item->jumlah }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Harga</label> <br>
                                <input type="text" name="harga" readonly class="form-control" required id="harga_barang">
                            </div>
                            <div class="form-group">
                                <label for="">Jumlah</label> <br>
                                <input type="number" min="0" name="jumlah" class="form-control" required
                                    id="jumlah_barang">
                            </div>
                            <div class="form-group">
                                <label for="">Diskon (%)</label> <br>
                                <input type="number" min="0" name="diskon" class="form-control" required id="diskon">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">

    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        // CKEDITOR.replace('editor_deskripsi');
        // $('#tabelItem').DataTable();
        renderTabel();
        $('.select2').select2();
        $('#barang_id').on('change', function() {
            let harga = $(this).find(':selected').attr('data-harga');
            if ($(this).find(':selected').val() != '') {
                $('#harga_barang').val(harga)
            } else {
                $('#harga_barang').val('')
            }
        });

        $('#modalAdd').on('hidden.bs.modal', function(e) {
            $(this).find('#formAddTemp')[0].reset();
            $('#barang_id').val('').trigger('change')
        });

        function getDiskon(obj){
            let pajak = $(obj).val();
            let sub_total = $('#sub_total').val();
            let total_diskon = $('#total_diskon').val();
            let grand_total = $('#grand_total').val();

            hitungAll(sub_total,total_diskon,pajak);
        }

        function getHarga(obj) {
            let harga = $(obj).find(':selected').attr('data-harga');
            if ($(obj).find(':selected').val() != '') {
                $('#edit_harga_barang').val(harga)
            } else {
                $('#edit_harga_barang').val('')
            }
        }

        function editData(obj) {
            let id = $(obj).attr('id');
            $.ajax({
                url: '{{ route('barangMasuk.edit') }}',
                type: 'get',
                data: {
                    id: id,
                },
                success: function(res) {
                    $('#modalEdit').html(res);
                    $('#modalEdit').modal('show');
                    runValidatorTempEdit();
                }
            })
        }

        function deleteData(obj) {
            let id = $(obj).attr('id');
            Swal.fire({
                title: "Anda Yakin ?",
                text: "Data akan terhapus permanen",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Iya, Hapus saja!",
                cancelButtonText: "Tidak, Batalkan!",
                reverseButtons: true
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: '{{ route('barangMasuk.delete') }}',
                        type: 'get',
                        data: {
                            id: id,
                        },
                        success: function(res) {
                            Swal.fire('Sukses','Berhasil Di Hapus','sucess')
                            renderTabel();
                        }
                    })
                }
            });

        }


        function renderTabel() {
            let sub_total = $('#sub_total').val();
            let total_diskon = $('#total_diskon').val();
            let pajak = $('#pajak').val();
            let grand_total = $('#grand_total').val();
            $.ajax({
                url: '{{ route('barangMasuk.render') }}',
                type: 'get',
                success: function(res) {
                    let jml_barang = (res.barang).length;
                    if (jml_barang > 0) {
                        $('#renderTabel').html(res.html);
                        $('#jml').val(jml_barang);

                       

                        let x_total_harga = 0;
                        let x_total_diskon = 0;
                        $.each(res.barang,function(k,i){
                            let total_harga_item = i.barang.harga * i.jumlah;
                            x_total_diskon += i.total_diskon;
                            x_total_harga += total_harga_item;
                        });

                        hitungAll(x_total_harga,x_total_diskon,pajak);



                    } else {
                        $('#renderTabel').html(`
                            <tr>
                                <td colspan="7">Tidak Ada Data</td>
                            </tr>
                        `);
                        $('#jml').val('');
                    }
                }
            })
        }

        function hitungAll(total_harga,total_diskon,pajak){
            let grand_total = total_harga - total_diskon;
            let total_pajak = (pajak/100) * grand_total;
            // console.log(total_pajak);
            grand_total = grand_total + total_pajak;
            $('#grand_total').val(grand_total);
            $('#sub_total').val(total_harga);
            $('#total_diskon').val(total_diskon);


        }

        var runValidator = function() {
            var form = $('#userAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    nama_barang: "required",
                    kode_barang: "required",
                    harga: "required",
                },
                messages: {
                    jml_barang: {
                        required: 'Barang Tidak boleh kosong'
                    }
                },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        var runValidatorTemp = function() {
            var form = $('#formAddTemp');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    nama_barang: "required",
                    kode_barang: "required",
                    harga: "required",
                },
                messages: {
                    // jml_barang: {
                    //     required: 'Barang Tidak boleh kosong'
                    // }
                },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        $.ajax({
                            url: '{{ route('barangMasuk.addTemp') }}',
                            type: 'post',
                            data: $('#formAddTemp').serialize(),
                            success: function(res) {
                                if (res == 'exist') {
                                    Swal.fire('', 'Barang Sudah ada di dalam daftar',
                                        'warning');
                                } else {
                                    Swal.fire('Sukses', 'Barang berhasil di tambahkan',
                                        'success');
                                    $('#modalAdd').modal('hide');
                                    renderTabel();
                                }
                            }
                        })

                    }
                }
            });
        };
        var runValidatorTempEdit = function() {
            var form = $('#formEditTemp');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    nama_barang: "required",
                    kode_barang: "required",
                    harga: "required",
                },
                messages: {
                    // jml_barang: {
                    //     required: 'Barang Tidak boleh kosong'
                    // }
                },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        console.log('tes edit');
                        $.ajax({
                            url: '{{ route('barangMasuk.editTemp') }}',
                            type: 'post',
                            data: $('#formEditTemp').serialize(),
                            success: function(res) {
                                if (res == 'exist') {
                                    Swal.fire('', 'Barang Sudah ada di dalam daftar',
                                        'warning');
                                } else {
                                    Swal.fire('Sukses', 'Barang berhasil di edit',
                                        'success');
                                    $('#modalEdit').modal('hide');
                                    renderTabel();
                                }
                            }
                        })

                    }
                }
            });
        };
        runValidator();
        runValidatorTemp();
    </script>
@endsection
