@extends('back.master')
@section('custom-css')
@endsection
@section('content')
    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header d-flex justify-content-between">
                    <span> Edit Jenis </span>

                    {{-- <a href="{{ route('item.add') }}" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus"></i>
                        </span>
                        <span class="text">Tambah Data</span>
                    </a> --}}
                </div>
                <div class="card-body">
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            <strong>{{ session('error') }}</strong>
                        </div>
                    @endif
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">
                                <strong>Jenis Sudah Terdaftar</strong>
                            </div>
                        @endforeach
                    @endif

                    <form method="POST" action="{{ route('jenis.update',$jenis->id) }}" id="userAdd">
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label>Kategori
                                    <span class="text-danger">*</span></label>
                                <select name="kategori" onchange="getKategori(this)" required class="form-control">
                                    <option value=""> - Pilih Kategori - </option>
                                    <option value="Makanan" {{ $jenis->kategori == 'Makanan' ? 'selected' : '' }}>Makanan</option>
                                    <option value="Minuman" {{ $jenis->kategori == 'Makanan' ? 'selected' : '' }}>Minuman</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Kode
                                    <span class="text-danger">*</span></label>
                                <input type="text" required readonly class="form-control" id="kode_makanan"
                                    value="{{ $jenis->kode }}" name="kode_makanan" placeholder="Kode..." />
                                {{-- <input type="text" required readonly class="form-control" style="display: none"
                                    id="kode_minuman" value="{{ $kode_minuman }}" name="kode_minuman"
                                    placeholder="Kode..." /> --}}
                            </div>
                            <div class="form-group">
                                <label>Nama
                                    <span class="text-danger">*</span></label>
                                <input type="text" required class="form-control" value="{{ $jenis->nama }}" id="nama" name="nama"
                                    placeholder="Nama..." />

                            </div>

                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        // CKEDITOR.replace('editor_deskripsi');
        // $('#tabelItem').DataTable();
        function getKategori(obj) {
            if ($(obj).find(':selected').val() == 'Makanan') {
                $('#kode_minuman').hide();
                $('#kode_makanan').show();
            } else {
                $('#kode_minuman').show();
                $('#kode_makanan').hide();
            }
        }
        var runValidator = function() {
            var form = $('#userAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    no_telpon: {
                        minlength: 3,
                        maxlength: 13,
                        digits: true,
                    }
                },
                messages: {

                },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
