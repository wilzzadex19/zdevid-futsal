<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
            <img src="{{ asset('assets/img/' . config('global.app_setting')->app_logo) }}" style="width: 100%" alt="">
        </div>
        <div class="sidebar-brand-text mx-3">{{ config('global.app_setting')->app_name }}</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ Request::is('dashboard*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    @if (Auth::user()->role == 'admin-barang')
        <li class="nav-item {{ Request::is('master-data/suplier*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('suplier') }}">
                <i class="fas fa-fw fa-users"></i>
                <span>Data Suplier</span></a>
        </li>

        <li class="nav-item {{ Request::is('master-data/jenis*') || Request::is('master-data/merk*') || Request::is('master-data/barang') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('master-data/jenis*') || Request::is('master-data/merk*') || Request::is('master-data/barang') ? '' : 'collapsed' }}" href="#" data-toggle="collapse"
                data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-table"></i>
                <span>Master Barang</span>
            </a>
            <div id="collapseTwo" class="collapse {{ Request::is('master-data/jenis*') || Request::is('master-data/merk*') || Request::is('master-data/barang') ? 'show' : '' }}"
                aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{ Request::is('master-data/jenis*') ? 'active' : '' }}"
                        href="{{ route('jenis') }}">Data Jenis</a>
                    <a class="collapse-item {{ Request::is('master-data/merk*') ? 'active' : '' }}"
                        href="{{ route('merk') }}">Data Merk</a>
                    <a class="collapse-item {{ Request::is('master-data/barang') ? 'active' : '' }}"
                        href="{{ route('barang') }}">Data Barang</a>
                </div>
            </div>
        </li>
      
       
        {{-- <li class="nav-item {{ Request::is('master-data/jenis*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('jenis') }}">
                <i class="fas fa-fw fa-archive"></i>
                <span>Data Jenis</span></a>
        </li>
        <li class="nav-item {{ Request::is('master-data/merk*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('merk') }}">
                <i class="fas fa-fw fa-clone"></i>
                <span>Data Merk</span></a>
        </li>
        <li class="nav-item {{ Request::is('master-data/barang*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('barang') }}">
                <i class="fas fa-fw fa-box"></i>
                <span>Data Barang</span></a>
        </li> --}}
        <li class="nav-item {{ Request::is('master-data/barangMasuk*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('barangMasuk') }}">
                <i class="fas fa-fw fa-download"></i>
                <span>Pembelian Barang</span></a>
        </li>
        <li class="nav-item {{ Request::is('master-data/barangKeluar*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('barangKeluar') }}">
                <i class="fas fa-fw fa-upload"></i>
                <span>Penjualan Barang</span></a>
        </li>
    @endif
    @if (Auth::user()->role == 'admin')
        <li class="nav-item {{ Request::is('jadwal-all*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('jadwal-all') }}">
                <i class="fas fa-fw fa-calendar"></i>
                <span>Jadwal</span></a>
        </li>
        <li class="nav-item {{ Request::is('master-data*') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('master-data*') ? '' : 'collapsed' }}" href="#" data-toggle="collapse"
                data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-table"></i>
                <span>Master Data</span>
            </a>
            <div id="collapseTwo" class="collapse {{ Request::is('master-data*') ? 'show' : '' }}"
                aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{ Request::is('master-data/item*') ? 'active' : '' }}"
                        href="{{ route('item') }}">Data Lapangan</a>
                    <a class="collapse-item {{ Request::is('master-data/jadwal*') ? 'active' : '' }}"
                        href="{{ route('jadwal') }}">Data Jadwal Lapangan</a>
                    <a class="collapse-item {{ Request::is('master-data/member*') ? 'active' : '' }}"
                        href="{{ route('member') }}">Data Penyewa</a>

                    {{-- <a class="collapse-item {{ Request::is('master-data/barang*') ? 'active' : '' }}"
                        href="{{ route('barang') }}">Data Barang</a> --}}
                </div>
            </div>
        </li>
        <li class="nav-item {{ Request::is('input*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('input') }}">
                <i class="fas fa-fw fa-calendar-plus"></i>
                <span>Input Booking</span></a>
        </li>
        <li class="nav-item {{ Request::is('data-booking*') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('master-data*') ? '' : 'collapsed' }}" href="#" data-toggle="collapse"
                data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                <i class="fas fa-fw fa-futbol"></i>
                <span>Data Booking</span>
            </a>
            <div id="collapse2" class="collapse {{ Request::is('data-booking*') ? 'show' : '' }}"
                aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{ Request::is('data-booking/list/pending*') ? 'active' : '' }}"
                        href="{{ route('dataBooking', 'pending') }}">Menunggu Pembayaran</a>
                    <a class="collapse-item {{ Request::is('data-booking/list/success*') ? 'active' : '' }}"
                        href="{{ route('dataBooking', 'success') }}">Pembayaran Berhasil</a>
                    {{-- <a class="collapse-item {{ Request::is('data-booking/list/siap-digunakan*') ? 'active' : '' }}"
                        href="{{ route('dataBooking', 'siap-digunakan') }}">Siap Digunakan</a> --}}
                    <a class="collapse-item {{ Request::is('data-booking/list/selesai*') ? 'active' : '' }}"
                        href="{{ route('dataBooking', 'selesai') }}">Selesai</a>

                    <a class="collapse-item {{ Request::is('data-booking/list/unfinish*') ? 'active' : '' }}"
                        href="{{ route('dataBooking', 'unfinish') }}">Transaksi Gagal</a>
                        <a class="collapse-item {{ Request::is('data-booking/cetak*') ? 'active' : '' }}"
                        href="{{ route('dataBooking.cetak') }}">Cetak Laporan</a>
                </div>
            </div>
        </li>
        <li class="nav-item {{ Request::is('setting*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('setting') }}">
                <i class="fas fa-fw fa-cog"></i>
                <span>Pengaturan Aplikasi</span></a>
        </li>
    @endif
    @if (Auth::user()->role == 'owner')
        <li class="nav-item {{ Request::is('jadwal-all*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('jadwal-all') }}">
                <i class="fas fa-fw fa-calendar"></i>
                <span>Jadwal</span></a>
        </li>
        <li class="nav-item {{ Request::is('data-booking*') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('master-data*') ? '' : 'collapsed' }}" href="#" data-toggle="collapse"
                data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                <i class="fas fa-fw fa-futbol"></i>
                <span> Laporan Data Booking</span>
            </a>
            <div id="collapse2" class="collapse {{ Request::is('data-booking*') ? 'show' : '' }}"
                aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item {{ Request::is('data-booking/list/pending*') ? 'active' : '' }}"
                        href="{{ route('dataBooking', 'pending') }}">Menunggu Pembayaran</a>
                    <a class="collapse-item {{ Request::is('data-booking/list/success*') ? 'active' : '' }}"
                        href="{{ route('dataBooking', 'success') }}">Pembayaran Berhasil</a>
                    {{-- <a class="collapse-item {{ Request::is('data-booking/list/siap-digunakan*') ? 'active' : '' }}"
                        href="{{ route('dataBooking', 'siap-digunakan') }}">Siap Digunakan</a> --}}
                    <a class="collapse-item {{ Request::is('data-booking/list/selesai*') ? 'active' : '' }}"
                        href="{{ route('dataBooking', 'selesai') }}">Selesai</a>

                    <a class="collapse-item {{ Request::is('data-booking/list/unfinish*') ? 'active' : '' }}"
                        href="{{ route('dataBooking', 'unfinish') }}">Transaksi Gagal</a>
                    <a class="collapse-item {{ Request::is('data-booking/cetak*') ? 'active' : '' }}"
                        href="{{ route('dataBooking.cetak') }}">Cetak Laporan</a>
                </div>
            </div>
        </li>
        <li class="nav-item {{ Request::is('master-data/barang*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('barang') }}">
                <i class="fas fa-fw fa-box"></i>
                <span>Laporan Stok Barang</span></a>
        <li class="nav-item {{ Request::is('master-data/barangMasuk*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('barangMasuk') }}">
                <i class="fas fa-fw fa-download"></i>
                <span> Laporan Pembelian Barang</span></a>
        </li>
        <li class="nav-item {{ Request::is('master-data/barangKeluar*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('barangKeluar') }}">
                <i class="fas fa-fw fa-upload"></i>
                <span> Laporan Penjualan Barang</span></a>
        <li class="nav-item {{ Request::is('laporan-all*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('laporan.all') }}">
                <i class="fas fa-fw fa-file"></i>
                <span> Laporan Keseluruhan</span></a>
        </li>
        <li class="nav-item {{ Request::is('setting*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('setting') }}">
                <i class="fas fa-fw fa-cog"></i>
                <span>Pengaturan Aplikasi</span></a>
        </li>
    @endif



    <!-- Divider -->
    {{-- <hr class="sidebar-divider"> --}}

    <!-- Heading -->
    {{-- <div class="sidebar-heading">
        Interface
    </div> --}}

    <!-- Nav Item - Pages Collapse Menu -->


    <!-- Nav Item - Utilities Collapse Menu -->


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
