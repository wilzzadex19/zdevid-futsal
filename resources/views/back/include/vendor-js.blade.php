    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('assets') }}/vendor/jquery/jquery.min.js"></script>
    <script src="{{ asset('assets') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('assets') }}/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('assets') }}/js/sb-admin-2.min.js"></script>
    <script src="{{ asset('assets') }}/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript" src="{{ asset('assets/jv/dist/jquery.validate.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    {{-- <script src="//cdn.ckeditor.com/4.18.0/standard/ckeditor.js"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


    <script>
        @if (session('success'))
            Swal.fire('Sukses !', '{{ session('success') }}', 'success')
        @endif
        @if (session('warning'))
            Swal.fire('Sukses !', '{{ session('warning') }}', 'warning')
        @endif
        var no_pic =
            'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22200%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20200%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_180711e833f%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_180711e833f%22%3E%3Crect%20width%3D%22200%22%20height%3D%22200%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2274.421875%22%20y%3D%22104.5%22%3E200x200%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E';
        // $('#preview_image').attr('src', no_pic);

        function readFile(obj, preview = false, is_edit = false) {
            var url = $(obj).val();
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            var text = url.substring(12);


            if (obj.files && obj.files[0] && (ext == "jpg" || ext == "jpeg" || ext == "png")) {
                if (obj.files[0].size > 1048576) {
                    var mb = (1048576 / 1024 / 1024);


                    Swal.fire(
                        'Gagal !',
                        'File yang di upload harus lebih kecil dari ' + mb + ' MB',
                        'warning',
                    );
                    $('#preview_image').attr('src', no_pic);
                    $(obj).val('');
                    return false;
                } else {

                    if (preview) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#preview_image').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(obj.files[0]);
                    }


                    return true;
                }
            } else {
                Swal.fire(
                    'Gagal !',
                    'Format Tidak didukung',
                    'warning'
                )
                $('#preview_image').attr('src', no_pic);
                $(obj).val('');
                return false
            }
        }

        function readFileEdit(obj, preview = false, is_edit = false) {
            var url = $(obj).val();
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            var text = url.substring(12);


            if (obj.files && obj.files[0] && (ext == "jpg" || ext == "jpeg" || ext == "png")) {
                if (obj.files[0].size > 1048576) {
                    var mb = (1048576 / 1024 / 1024);


                    Swal.fire(
                        'Gagal !',
                        'File yang di upload harus lebih kecil dari ' + mb + ' MB',
                        'warning',
                    );
                    $('#preview_image').attr('src', no_pic);
                    if (is_edit) {
                        $('#preview_image').attr('src', is_edit);
                    }
                    $(obj).val('');
                    return false;
                } else {

                    if (preview) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            $('#preview_image').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(obj.files[0]);
                    }




                    return true;
                }
            } else {
                Swal.fire(
                    'Gagal !',
                    'Format Tidak didukung',
                    'warning'
                )
                $('#preview_image').attr('src', no_pic);
                if (is_edit) {
                    $('#preview_image').attr('src', is_edit);
                }
                $(obj).val('');
                return false
            }
        }

        function myBlock() {

        }
    </script>
