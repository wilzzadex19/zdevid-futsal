<footer id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="full">
                    <div class="footer-widget">
                        <div class="footer-logo">
                            <a href="#"><img src="{{ asset('assets/img/' . config('global.app_setting')->app_logo) }}" alt="#" /></a>
                        </div>
                        {{-- <p>Most of our events have hard and easy route choices as we are always keen to encourage new
                            riders.</p> --}}
                        <ul class="social-icons style-4 pull-left">
                            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="youtube" href="#"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-2">
                
            </div> --}}
            <div class="col-md-5">
                <div class="full">
                    <div class="footer-widget" style="color: white">
                        <h3 style="color: white">Kontak</h3>
                        <span><i class="fa fa-map-marker"></i> Jl. Raya Leles No. 8 Garut</span> <br>
                        <span><i class="fa fa-envelope"></i> premierefutsal@gmail.com</span> <br>
                        <span><i class="fa fa-phone"></i> +62 857 - 7455 -3807</span>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="full">
                    <div class="contact-footer">
                        <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="350" id="gmap_canvas" src="https://maps.google.com/maps?q=de%20premiere%20futsal&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://putlocker-is.org"></a><br><style>.mapouter{position:relative;text-align:right;height:350px;width:600px;}</style><a href="https://www.embedgooglemap.net">google embed</a><style>.gmap_canvas {overflow:hidden;background:none!important;height:350px;width:600px;}</style></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p>Copyright © {{ date('Y') }} {{ config('global.app_setting')->app_name }}</p>
        </div>
    </div>
</footer>
