<script src="{{ asset('assets/fe') }}/js/all.js"></script>
<!-- ALL PLUGINS -->
<script src="{{ asset('assets/fe') }}/js/custom.js"></script>
<script type="text/javascript" src="{{ asset('assets/jv/dist/jquery.validate.js') }}"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<script>
    @if (session('success'))
        Swal.fire('Sukses !','{{ session('success') }}','success')
    @endif
    @if (session('warning'))
        Swal.fire('','{{ session('warning') }}','warning')
    @endif

    var no_pic =
        'https://png.pngtree.com/png-vector/20190820/ourmid/pngtree-no-image-vector-illustration-isolated-png-image_1694547.jpg';
    // $('#preview_image').attr('src', no_pic);

    function readFile(obj, preview = false, is_edit = false) {
        var url = $(obj).val();
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        var text = url.substring(12);


        if (obj.files && obj.files[0] && (ext == "jpg" || ext == "jpeg" || ext == "png")) {
            if (obj.files[0].size > 1048576) {
                var mb = (1048576 / 1024 / 1024);


                Swal.fire(
                    'Gagal !',
                    'File yang di upload harus lebih kecil dari ' + mb + ' MB',
                    'warning',
                );
                $('#preview_image').attr('src', no_pic);
                $(obj).val('');
                return false;
            } else {

                if (preview) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#preview_image').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(obj.files[0]);
                }


                return true;
            }
        } else {
            Swal.fire(
                'Gagal !',
                'Format Tidak didukung',
                'warning'
            )
            $('#preview_image').attr('src', no_pic);
            $(obj).val('');
            return false
        }
    }
</script>
