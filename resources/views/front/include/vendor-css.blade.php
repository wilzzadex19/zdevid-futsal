<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Site Metas -->
<title>{{ config('global.app_setting')->app_name }}</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<!-- Site Icons -->
<link rel="shortcut icon" href="" type="image/x-icon" />
<link rel="apple-touch-icon" href="">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('assets/fe') }}/css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="{{ asset('assets/fe') }}/style.css">
<!-- Colors CSS -->
{{-- <link rel="stylesheet" href="{{ asset('assets/fe') }}/css/colors.css"> --}}
<!-- ALL VERSION CSS -->
{{-- <link rel="stylesheet" href="{{ asset('assets/fe') }}/css/versions.css"> --}}
<!-- Responsive CSS -->
<link rel="stylesheet" href="{{ asset('assets/fe') }}/css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="{{ asset('assets/fe') }}/css/custom.css">

<link rel="icon" href="{{ asset('assets/img/' . config('global.app_setting')->app_logo) }}">


<style>
    .my-img {
        width: 40px;
        height: 50px;
        max-width: 80%;
        max-height: 80%;
        object-fit: contain;
        border-radius: 20%;
        /* transform: translate(-50%, -50%); */
    }

    .inner-page-banner {
        background-image: url('{{ asset('img/bg.png') }}') !important;
        min-height: 300px;
    }

    .box {
        height: 20px;
        width: 20px;
        border: 1px solid black;
    }

    .red {
        background-color: red;
    }

    .green {
        background-color: #90EE90;
    }

    .blue {
        background-color: blue;
    }

    .buttons {
        background-color: #4CAF50;
        /* Green */
        border: none;
        color: white;
        padding: 10px 22px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 13px;
        margin: 2px 1px;
        cursor: pointer;
    }
    .buttonss {
        background-color: #4CAF50;
        /* Green */
        border: none;
        color: white;
        padding: 10px;
        text-align: center;
        text-decoration: none;
        /* display: inline-block; */
        font-size: 12px;
        margin: 2px 2px;
        height: 30px;
        cursor: pointer;
    }

    .binfo {
        background-color: #008CBA;
    }

    /* Blue */
    .bdanger {
        background-color: #f44336;
    }

    /* Red */
    .bblack {
        background-color: #e7e7e7;
        color: black;
    }

    /* Gray */
    .bgrey {
        background-color: #555555;
    }

    .invalid-feedback {
        color: red;
        margin-top: -10px;
    }

    .is-invalid {
        margin-bottom: -3px !important;
    }

    /* Black */
</style>
<!-- font family -->
<link
    href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">
<!-- end font family -->
<link rel="stylesheet" href="{{ asset('assets/fe') }}/css/3dslider.css" />
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<script src="{{ asset('assets/fe') }}/js/3dslider.js"></script>
</head>
