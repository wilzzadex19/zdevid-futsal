<section id="top">
    <header>
        <div class="container">
            <div class="header-top">
                <div class="row">
                    <div class="col-md-6">
                        <div class="full">
                            <div class="logo">
                                <a href="{{ route('home') }}"><img src="{{ asset('assets/logo.png') }}"
                                        style="width: 220px" alt="#" /></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="right_top_section">
                            <!-- social icon -->
                            {{-- <ul class="social-icons pull-left">
                         <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                         <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                         <li><a class="youtube" href="#"><i class="fa fa-youtube-play"></i></a></li>
                         <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                      </ul> --}}
                            <!-- end social icon -->
                            <!-- button section -->
                            @if (Auth::user())
                                @if (Auth::user()->role == 'member')
                                    <ul class="login">

                                        {{-- <li class="login-modal">
                                            <a href="{{ route('panduan') }}" class="login"><i
                                                    class="fa fa-question"></i>Panduan</a>
                                        </li> --}}
                                        <li class="login-modal">
                                            <a href="{{ route('logout') }}" class="login"><i
                                                    class="fa fa-sign-out"></i>Logout</a>
                                        </li>
                                    </ul>
                                @else
                                    <ul class="login">
                                        <li class="login-modal">
                                            <a href="{{ route('login') }}" class="login"><i
                                                    class="fa fa-user"></i>Login /
                                                Daftar</a>
                                        </li>
                                    </ul>
                                @endif
                            @else
                                <ul class="login">
                                    <li class="login-modal">
                                        <a href="{{ route('login.home') }}" class="login"><i
                                                class="fa fa-user"></i>Login /
                                            Daftar</a>
                                    </li>
                                </ul>
                            @endif

                            <!-- end button section -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom">
                <div class="row">
                    <div class="col-md-12">
                        <div class="full">
                            <div class="main-menu-section">
                                <div class="menu">
                                    <nav class="navbar navbar-inverse ">
                                        <div class="navbar-header">
                                            <button class="navbar-toggle" type="button" data-toggle="collapse"
                                                data-target=".js-navbar-collapse">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <a class="navbar-brand" href="#">Menu</a>
                                        </div>
                                        <div class="collapse navbar-collapse js-navbar-collapse">
                                            <ul class="nav navbar-nav">
                                                <li><a href="{{ route('home') }}"
                                                        style="color: {{ Request::is('/') ? 'red' : '' }}">Home</a>
                                                </li>
                                                <li><a href="{{ route('tentang') }}"
                                                        style="color: {{ Request::is('about*') ? 'red' : '' }}">Tentang</a>
                                                </li>
                                                <li><a href="{{ route('jadwal-front') }}"
                                                        style="color: {{ Request::is('jadwal*') ? 'red' : '' }}">Jadwal</a>
                                                </li>
                                                <li><a href="{{ route('booking') }}"
                                                        style="color: {{ Request::is('booking*') ? 'red' : '' }}">Booking
                                                        Lapangan</a></li>


                                                {{-- <li><a href="news.html">Jadwal</a></li> --}}
                                                @if (Auth::user())
                                                    @if (Auth::user()->role == 'member')
                                                        @if (count(Auth::user()->booking) > 0)
                                                            <li><a style="color: {{ Request::is('histori*') ? 'red' : '' }}"
                                                                    href="{{ route('histori') }}">Histori Booking</a>
                                                            </li>
                                                        @endif
                                                        @if (count(Auth::user()->booking_success) > 0)
                                                            <li><a style="color: {{ Request::is('edit-jadwal*') ? 'red' : '' }}"
                                                                    href="{{ route('edit-jadwal') }}">Pergantian
                                                                    Jadwal</a>
                                                            </li>
                                                        @endif
                                                    @endif
                                                @else
                                                    {{-- <li><a style="color: {{ Request::is('panduan*') ? 'red' : '' }}"
                                                            href="{{ route('panduan') }}">Panduan</a>
                                                    </li> --}}
                                                @endif
                                                {{-- <li class="dropdown mega-dropdown">
                                                    <a href="match" class="dropdown-toggle"
                                                        data-toggle="dropdown">Match<span
                                                            class="caret"></span></a>
                                                    <ul class="dropdown-menu mega-dropdown-menu" style="width:200px;margin-right:800px">
                                                        <li class="col-sm-8">
                                                            <ul>
                                                                <li class="dropdown-header">Men Collection</li>
                                                               
                                                            </ul>
                                                        </li>
                                                       
                                                    </ul>
                                                </li> --}}
                                                {{-- <li><a href="blog.html">Blog</a></li>
                                                <li><a href="contact.html">contact</a></li> --}}
                                            </ul>
                                        </div>
                                        <!-- /.nav-collapse -->
                                    </nav>
                                    @if (Auth::user())
                                        @if (Auth::user()->role == 'member')
                                            <div class="search-bar" style="width: 250px">
                                                <a href="{{ route('member.profile') }}">
                                                    <div id="imaginary_container">
                                                        <img style="width:{{ Auth::user()->foto != null ? '40' : '50' }}px;float: right;margin-left: 20px;"
                                                            class="img-profile rounded-circle {{ Auth::user()->foto != null ? 'my-img' : '' }}"
                                                            src="{{ Auth::user()->foto != null ? asset('img/member/' . Auth::user()->foto) : asset('assets/img/undraw_profile.svg') }}">
                                                        <strong
                                                            style="float: right;margin-top:15px">{{ Auth::user()->name }}</strong>
                                                    </div>
                                                </a>
                                            </div>
                                        @endif
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @if (Request::is('/'))
        <div class="full-slider" style="max-height: 600px">
            <div id="carousel-example-generic" class="carousel slide">

                <div class="carousel-inner" role="listbox">

                    <!-- Second slide -->
                    <div class="item active skyblue" data-ride="carousel" data-interval="5000">
                        <div class="carousel-caption" >
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12"></div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="slider-contant" data-animation="animated fadeInRight">
                                    <h3>Kepuasan Anda Prioritas Kami</h3>
                                    <p style="font-size: 30px;color: orange"><strong> Rivandhika Futsal </strong></p>
                                    {{-- <button class="btn btn-primary btn-lg">Button</button> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.item -->

                </div>
            </div>
            <!-- /.carousel -->
            <div class="news">
                <div class="container">
                    <div class="heading-slider">
                        <p class="headline"><i class="fa fa-star" aria-hidden="true"></i> Alamat :</p>
                        <!--made by vipul mirajkar thevipulm.appspot.com-->
                        <h1>
                            <a href="" class="typewrite" data-period="2000"
                                data-type='[ "Jl. Raya Leles No. 8 Garut" ]'>
                                <span class="wrap"></span>
                            </a>
                        </h1 <span class="wrap"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="inner-page-banner">
            <div class="container">
            </div>
        </div>
        @yield('information')
    @endif
</section>
