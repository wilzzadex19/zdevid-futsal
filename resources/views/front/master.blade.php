<!DOCTYPE html>
<html lang="en">
<!-- Basic -->
@include('front.include.vendor-css')
@yield('custom-css')
<body class="game_info" data-spy="scroll" data-target=".header">
    <!-- LOADER -->
    {{-- <div id="preloader">
        <img class="preloader" src="{{ asset('assets/fe') }}/images/loading-img.gif" alt="">
    </div> --}}
    <!-- END LOADER -->
    @include('front.include.topbar')
    @yield('content')
    @include('front.include.footer')
    <a href="#home" data-scroll class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>
    <!-- ALL JS FILES -->
    @include('front.include.vendor-js')

    @yield('custom-js')
</body>

</html>
