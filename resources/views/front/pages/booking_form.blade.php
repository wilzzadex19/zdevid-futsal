@extends('front.master')
@section('custom-css')
@endsection
@section('information')
    <div class="inner-information-text">
        <div class="container">
            <h3>Form Booking</h3>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('booking') }}">Booking</a></li>
                <li class="active">Form</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <section id="contant" class="contant main-heading team">
        <div class="row">
            <div class="container">
                <div class="contact">
                    <div class="col-md-12">
                        @if (!empty($is_alert))
                            <div class="alert alert-danger" style="text-align: left">
                                <strong>Info Penting !</strong> Anda memiliki 2 Data penyewaan yang belum di lunasi , jika
                                sampai memiliki 3 data dan tidak melakukan pelunasan hingga jadwal sewa berakhir maka akun
                                anda akan di tangguhkan selama 3 Minggu
                            </div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="contact-info">
                            <div class="kode-section-title">
                                <h3>Detail Booking</h3>
                            </div>
                            <div class="kode-forminfo">
                                <ul class="kode-form-list">
                                    <li>
                                        <i class="fa fa-map-marker"></i>
                                        <p><strong>Lapangan:</strong> {{ $jadwal->item->kode }}
                                            {{ $jadwal->item->name }}</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o"></i>
                                        <p><strong>Jadwal:</strong> {{ $jadwal->hari->name }}
                                            {{ $jadwal->jam->display }}</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-calendar"></i>
                                        <p><strong>Tanggal:</strong> {{ \App\Helper\helper::tgl_indo($tanggal) }}</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-dollar"></i>
                                        <p><strong>Harga:</strong> Rp. {{ number_format($jadwal->harga) }} /Jam</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" style="text-align: left">
                        <div class="kode-section-title">
                            <h3>Formulir Booking</h3>
                        </div>
                        <form action="{{ route('booking.form.add') }}" method="POST" id="userAdd">
                            @csrf
                            <input type="hidden" name="item_id" value="{{ $jadwal->item_id }}">
                            <input type="hidden" name="hari_id" value="{{ $jadwal->hari_id }}">
                            <input type="hidden" name="jam_id" value="{{ $jadwal->jam_id }}">
                            <input type="hidden" name="tanggal" value="{{ $tanggal }}">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" readonly value="{{ Auth::user()->name }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Nama Tim</label>
                                <input type="text" required name="team_name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>No Hp Penanggung Jawab</label>
                                <input type="text" required name="no_hp" class="form-control">
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Durasi (Jam)</label>
                                    <select name="durasi" class="form-control" required onchange="updateHarga(this)">
                                        {{-- <option value="1">1</option> --}}
                                        @php
                                            $dur = 1;
                                            $jam_ada = \App\Detail_Booking::where([
                                                'item_id' => $jadwal->item_id,
                                                'hari_id' => $jadwal->hari_id,
                                                'tanggal' => $tanggal,
                                            ])
                                                ->with('jam')
                                                ->where('jam_id', '>', $jadwal->jam_id)
                                                ->with('jam')
                                                ->first();
                                            
                                            if ($jam_ada != null) {
                                                $hasil = $jam_ada->jam_id - $jadwal->jam_id;
                                            }
                                            
                                        @endphp

                                        @foreach ($jam_tersedia as $key => $item)
                                            @if (empty($hasil))
                                                <option value="{{ $dur }}">{{ $dur }}
                                                    ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
                                                    {{ date('H:i', strtotime($item->jam->jam_akhir)) }})
                                                </option>
                                            @else
                                                <option value="{{ $dur }}"
                                                    {{ $key < $hasil ? '' : 'disabled' }}>
                                                    {{ $dur }}
                                                    ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
                                                    {{ date('H:i', strtotime($item->jam->jam_akhir)) }})
                                                </option>
                                            @endif

                                            @php
                                                $dur++;
                                            @endphp
                                        @endforeach
                                    </select>
                                </div>
                                {{-- {{ dump($hasil) }} --}}
                                {{-- {{ dump($hasil) }} --}}
                                <div class="col-md-6">
                                    <label>Total Biaya Sewa Lapangan</label>
                                    <input type="text" readonly id="total_harga" value="{{ $jadwal->harga }}"
                                        name="total_harga" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea name="keterangan" class="form-control" cols="30" rows="5"></textarea>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Pembayaran</label>
                                    <select name="jenis_pembayaran" required id="jenis_pembayaran" class="form-control">
                                        <option value="full">Full</option>
                                        <option value="dp">DP (Minimal 20% Total Harga)</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Jumlah Dibayar</label>
                                    <input type="text" required name="jumlah_bayar" readonly id="jumlah_bayar"
                                        onkeyup="cekDp(this)" value="{{ $jadwal->harga }}" name="total_harga"
                                        class="form-control">

                                </div>
                                <div class="col-md-6" style="display: none" id="box_dp">
                                    <label>Minimal Pembayaran</label>
                                    <input type="text" readonly name="min_dp" id="min_dp" value="0"
                                        class="form-control">

                                </div>
                                <div class="col-md-6" style="display: none" id="box_dp2">
                                    <label>Sisa Pembayaran</label>
                                    <input type="text" readonly name="sisa_bayar" id="sisa_bayar" value="0"
                                        class="form-control">

                                </div>
                               

                               
                            </div>
                            <div>
                                <label>Catatan Booking :</label> <br>
                                <small>- Metode pembayaran DP hanya bisa dibayar minimal 20% dari harga lapangan yang di booking</small> <br>
                                <small>- Sistem akan otomatis mensuspen akun yang sudah melakukan penyewaan sebanyak 3 kali tanpa melakukan pembayaran pelunasan</small> <br>
                                <small>- Konsumen dapat melakukan pergantian jadwal dengan ketentuan lebih dari 24 jam dari sisa waktu penyewaan anda (Maks H-1). Bila sisa waktu anda kurang dari 24 jam, anda tidak dapat melakukan pergantian terhadap penyewaan anda</small>
                            </div>
                            <br>
                            
                            <a href="{{ route('booking') }}" class="btn btn-secondary">Kembali</a>
                            <button class="btn btn-primary" id="btn_book">Booking</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-js')
    <script>
        // CKEDITOR.replace('editor_deskripsi');
        // $('#tabelItem').DataTable();
        var runValidator = function() {
            var form = $('#userAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    no_hp: {
                        digits: true,
                        required: true,
                        minlength: 11,
                        maxlength: 13,

                        // pwcheck: true,
                        // minlength: 8
                    },
                },
                messages: {
                    no_hp: {
                        required: 'No Hp Harus diisi !',
                        digits: 'Masukan format no hp yang benar !',
                        maxlength: 'Maksimal 13 digit !',
                        minlength: 'Minimal 11 digit !'


                    },
                    team_name: {
                        required: 'Nama Tim Harus diisi !',
                    },

                    email: {
                        required: "Email harus diisi !",
                        email: "Masukan format email yang benar !",
                    },
                    alamat: {
                        required: "Alamat harus diisi !",
                        minlength: 'Alamat harus diisi setidaknya 5 karakter !',
                    },
                    password: {
                        required: 'Password harus diisi !',
                        minlength: 'Password minimal 6 Karakter !',
                    },
                    // password_confirm : {
                    //     required : 'Password harus diisi !',
                    //     minlength : 'Password minimal 8 Karakter !',
                    //     equalTo : 'Konfirmasi Password harus sama !',
                    // }
                },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        // myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#total_harga').mask('000.000.000', {
                reverse: true
            });
            $('#jumlah_bayar').mask('000.000.000', {
                reverse: true
            });
        })

        $('#jenis_pembayaran').on('change', function() {
            hitungBayar();
            let jenis = $('#jenis_pembayaran').find(':selected').val();
            if (jenis == 'dp') {
                $('#jumlah_bayar').prop('readonly', false);
            } else {
                $('#jumlah_bayar').prop('readonly', true);
            }
        })

        function cekDp(obs) {
            let jenis = $('#jenis_pembayaran').find(':selected').val();
            if (jenis == 'dp') {
                $('#jumlah_bayar').prop('readonly', false);
                let total_harga = ($('#total_harga').val()).replace(/\./g, "");
                let min_dp = ($('#min_dp').val()).replace(/\./g, "");
                let bayar = ($(obs).val()).replace(/\./g, "");
                if (parseInt(bayar) < parseInt(min_dp)) {
                    $('#btn_book').prop('disabled', true);
                } else if (parseInt(bayar) >= parseInt(total_harga)) {
                    $('#btn_book').prop('disabled', true);
                } else {
                    $('#btn_book').prop('disabled', false);
                }


                let sisa_bayar = parseInt(total_harga) - parseInt(bayar);
                $('#sisa_bayar').mask('000.000.000', {
                    reverse: true
                }).val(sisa_bayar).trigger('input');
            } else {
                $('#jumlah_bayar').prop('readonly', true);
                $('#sisa_bayar').mask('000.000.000', {
                    reverse: true
                }).val(0).trigger('input');
            }


        }

        function updateHarga(obj) {
            let dur = $(obj).find(':selected').val();
            let harga = '{{ $jadwal->harga }}';
            let total_harga = parseInt(harga) * parseInt(dur);
            $('#total_harga').mask('000.000.000', {
                reverse: true
            }).val(total_harga).trigger('input');

            hitungBayar();

            // sewa_tempat.replace(/\./g, "");
        }

        function hitungBayar() {
            let jenis = $('#jenis_pembayaran').find(':selected').val();
            let total_harga = ($('#total_harga').val()).replace(/\./g, "");
            let jumlah_bayar = 0;

            if (jenis == 'full') {
                jumlah_bayar = total_harga;
                $('#box_dp').hide();
                $('#box_dp2').hide();
            } else {
                let potongan = (20 / 100) * parseInt(total_harga);
                jumlah_bayar = potongan;
                $('#box_dp').show();
                $('#box_dp2').show();
            }





            $('#jumlah_bayar').mask('000.000.000', {
                reverse: true
            }).val(jumlah_bayar).trigger('input');
            $('#min_dp').mask('000.000.000', {
                reverse: true
            }).val(jumlah_bayar).trigger('input');
            // $('#min_dp').val(jumlah_bayar);


        }
    </script>
@endsection
