<style>
    * {
        font-family: "Arial";
    }

</style>

<body style="width:60%;text-align:center;" onload="window.print()">
    <div style="border: 1px solid black">
    <div style="width:100%;text-align:center;margin-top:20px;">
        <img src="{{ asset('assets/img/' . config('global.app_setting')->app_logo) }}" alt="" style="width:100px">
    </div>
    <br>
    <div class="mt-3">
        <h4 class="mt-4">

            Kode Booking

        </h4>
        <h4 class="mt-3 mb-2">
            <p style="font-size: 30px">{{ $booking->kode }}</p>
            <h4>Admin : {{ \App\User::where('role','admin')->first()->name }}</h4>
            <br>
            Simpan ini untuk bukti.
        </h4>
        
        <table style="text-align: left;width:100%;margin-left:10px">
            <tr>
                <td>Nama Penyewa</td>
                <td>:</td>
                <td>{{ $booking->users->name }}</td>
            </tr>
            <tr>
                <td>Kode Pembayaran</td>
                <td>:</td>
                <td>{{ $booking->payment_code == null ? $booking->kode . '-' . date('His') : $booking->payment_code }}</td>
            </tr>
            <tr>
                <td>Nama Tim</td>
                <td>:</td>
                <td>{{ $booking->team_name }}</td>
            </tr>
            <tr>
            <tr>
                <td>Lapangan</td>
                <td>:</td>
                <td>{{ $booking->detail[0]->item->name }}</td>
            </tr>
            <tr>
                <td>Lama Pemakaian</td>
                <td>:</td>
                <td>{{ count($booking->detail) }} Jam</td>
            </tr>
            <tr>
                <td>Tanggal Pembayaran</td>
                <td>:</td>
                <td>{{ \App\Helper\helper::tgl_indo_jam($booking->tanggal_bayar) }}</td>
            </tr>

            <tr>
                <td>Tanggal Penyewaan</td>
                <td>:</td>
                <td>{{ \App\Helper\helper::tgl_indo($booking->detail[0]->tanggal) }} -
                    ( {{ $booking->detail[0]->jam->jam_awal }} - {{ $booking->detail[count($booking->detail)-1]->jam->jam_akhir }})</td>
            </tr>


            <tr>
                <td>Telah Bayar</td>
                <td>:</td>
                <td>
                    @if ($booking->jenis_pembayaran == 'dp')
                        Rp. {{ number_format($booking->total_dp) }}
                    @else
                        Rp. {{ number_format($booking->total_harga) }}
                    @endif
                </td>
            </tr>
            <tr>
                <td>Sisa Pembayaran</td>
                <td>:</td>
                <td>
                    @if ($booking->jenis_pembayaran == 'dp')
                        Rp. {{ number_format($booking->total_harga - $booking->total_dp) }}
                    @else
                        Rp. 0 (Lunas)
                    @endif
                </td>
            </tr>


            {{-- @if (empty($is_admin))
                @if ($booking->jenis_pembayaran == 'dp')
                    <tr>
                        <td>Sisa Pembayaran</td>
                        <td>:</td>
                        <td>Rp. {{ number_format($booking->total_dp) }}</td>
                    </tr>
                @endif
            @endif --}}
            <tr>
                <td>Total Harga</td>
                <td>:</td>
                <td>Rp. {{ number_format($booking->total_harga) }}</td>
            </tr>


        </table>
        {{-- <small>*email ini dikirim secara otomatis dan tidak perlu dibalas.</small> --}}
    </div>
    <br> <br>
</div>
   
</body>
