@extends('front.master')
@section('custom-css')
@endsection
@section('information')
    <div class="inner-information-text">
        <div class="container">
            <h3>Form Edit Booking</h3>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('booking') }}">Edit Jadwal Booking</a></li>
                <li class="active">Form</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <section id="contant" class="contant main-heading team">
        <div class="row">
            <div class="container">
                <div class="contact">
                    <div class="col-md-4">
                        <div class="contact-info">
                            <div class="kode-section-title">
                                <h3>Detail Booking</h3>
                            </div>
                            <div class="kode-forminfo">
                                <ul class="kode-form-list">
                                    <li>
                                        <i class="fa fa-map-marker"></i>
                                        <p><strong>Lapangan:</strong> {{ $jadwal->item->kode }}
                                            {{ $jadwal->item->name }}</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o"></i>
                                        <p><strong>Jadwal:</strong> {{ $jadwal->hari->name }}
                                            {{ $jadwal->jam->display }}</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-calendar"></i>
                                        <p><strong>Tanggal:</strong> {{ \App\Helper\helper::tgl_indo($tanggal) }}</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-dollar"></i>
                                        <p><strong>Harga:</strong> Rp. {{ number_format($jadwal->harga) }} /Jam</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" style="text-align: left">
                        <div class="kode-section-title">
                            <h3>Formulir Pergantian Jadwal Booking</h3>
                        </div>

                        <form action="{{ route('booking.form.update') }}" method="POST">
                            @csrf
                            <input type="hidden" name="item_id" value="{{ $jadwal->item_id }}">
                            <input type="hidden" name="hari_id" value="{{ $jadwal->hari_id }}">
                            <input type="hidden" name="jam_id" value="{{ $jadwal->jam_id }}">
                            <input type="hidden" name="tanggal" value="{{ $tanggal }}">
                            <input type="hidden" name="edit_id" value="{{ $booking->id }}">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" readonly value="{{ Auth::user()->name }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Nama Tim</label>
                                <input type="text" required name="team_name" value="{{ $booking->team_name }}"
                                    class="form-control">
                            </div>

                            <div class="form-group">
                                <label>No Hp Penanggung Jawab</label>
                                <input type="text" required name="no_hp" value="{{ $booking->no_hp }}"
                                    class="form-control">
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Total Uang Masuk Jadwal Sebelumnya</label>
                                    <input type="text" readonly id="total_harga_sebelumnya" name="total_harga_sebelumnya"
                                        class="form-control"
                                        value="{{ $booking->jenis_pembayaran == 'dp' ? $booking->total_dp : $booking->total_harga }}">
                                </div>
                                <div class="col-md-6">
                                    <label>Total Harga Sewa Jadwal Sebelumnya</label>
                                    <input type="text" readonly id="total_sewa_sebelumnya" name="total_sewa_sebelumnya"
                                        class="form-control"
                                        value="{{ $booking->total_harga }}">
                                </div>

                            </div>
                            {{-- @dump($booking); --}}
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Durasi (Jam)</label>
                                    <select name="durasi" class="form-control" required onchange="updateHarga(this)">
                                        <option value="">- Pilih Durasi -</option>
                                        @php
                                            $dur = 1;
                                            $jam_ada = \App\Detail_Booking::where([
                                                'item_id' => $jadwal->item_id,
                                                'hari_id' => $jadwal->hari_id,
                                                'tanggal' => $tanggal,
                                            ])
                                                ->with('jam')
                                                ->where('jam_id', '>', $jadwal->jam_id)
                                                ->with('jam')
                                                ->first();
                                            
                                            if ($jam_ada != null) {
                                                $hasil = $jam_ada->jam_id - $jadwal->jam_id;
                                            }
                                            
                                        @endphp

                                        @foreach ($jam_tersedia as $key => $item)
                                            @if ($dur <= count($booking->detail))
                                                @if (empty($hasil))
                                                    <option value="{{ $dur }}">{{ $dur }}
                                                        ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
                                                        {{ date('H:i', strtotime($item->jam->jam_akhir)) }})
                                                    </option>
                                                @else
                                                    <option value="{{ $dur }}"
                                                        {{ $key < $hasil ? '' : 'disabled' }}>{{ $dur }}
                                                        ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
                                                        {{ date('H:i', strtotime($item->jam->jam_akhir)) }})
                                                    </option>
                                                @endif
                                            @endif


                                            @php
                                                $dur++;
                                            @endphp
                                        @endforeach
                                    </select>
                                </div>

                                {{-- {{ dump($hasil) }} --}}
                                {{-- {{ dump($hasil) }} --}}
                                <div class="col-md-6">
                                    <label>Total Harga Jadwal Baru</label>
                                    <input type="text" readonly id="total_harga" value="{{ $jadwal->harga }}"
                                        name="total_harga" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Sisa Pembayaran</label>
                                    <input type="text" readonly id="sisa_pembayaran" name="sisa_pembayaran"
                                        class="form-control">
                                </div>
                                <div class="col-md-6">

                                    <label>Uang hangus</label>
                                    <input type="text" readonly id="hangus" name="hangus" class="form-control">
                                </div>

                                {{-- <input type="text" name="uang_masuk" id="uang_masuk" value="{{ $booking->jenis_pembayaran == 'dp' ? $booking->total_dp : $booking->total_harga }}"> --}}
                            </div>

                            {{-- <div class="form-group">

                                <label></label>
                                <input type="text" readonly id="total_harga_sebelumnya"
                                    name="harga_" class="form-control" value="{{ $booking->total_harga }}">
                            </div> --}}
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea name="keterangan" class="form-control" cols="30" rows="5">{{ $booking->keterangan }}</textarea>
                            </div>

                            <div>
                                <label>Catatan Pergantian :</label> <br>
                                <small>- Durasi booking tidak bisa melebihi jadwal sebelumnya</small> <br>
                                <small>- Jika Harga sewa booking Sebelumnya melebihi jadwal booking yang baru maka sisa yang
                                    sebelumnya dianggap hangus</small> <br>
                                <small>- Jika Harga sewa booking yang baru lebih besar dari jadwal booking sebelumnya maka
                                    diwajibkan membayar sisa</small>
                            </div>
                            <a href="{{ route('booking') }}" class="buttons bgrey">Kembali</a>
                            <button class="buttons binfo">Ubah Jadwal</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#total_harga').mask('000.000.000', {
                reverse: true
            });
            $('#jumlah_bayar').mask('000.000.000', {
                reverse: true
            });
            $('#total_harga_sebelumnya').mask('000.000.000', {
                reverse: true
            });
            $('#total_sewa_sebelumnya').mask('000.000.000', {
                reverse: true
            });
        })

        $('#jenis_pembayaran').on('change', function() {
            hitungBayar();
        })

        function updateHarga(obj) {
            let dur = $(obj).find(':selected').val();
            let harga = '{{ $jadwal->harga }}';
            let total_harga = parseInt(harga) * parseInt(dur);
            let uang_masuk_sebelumnya = ($('#total_harga_sebelumnya').val()).replace(/\./g, "");
            let total_sewa_sebelumnya = ($('#total_sewa_sebelumnya').val()).replace(/\./g, "");

            let sisa_pembayaran = total_harga - parseInt(uang_masuk_sebelumnya);

            console.log(sisa_pembayaran);

            let uang_hangus = parseInt(uang_masuk_sebelumnya) - total_harga;

            let fix_uang_hangus = uang_hangus <= 0 ? 0 : uang_hangus;

            $('#total_harga').mask('000.000.000', {
                reverse: true
            }).val(total_harga).trigger('input');
            $('#sisa_pembayaran').mask('000.000.000', {
                reverse: true
            }).val(sisa_pembayaran <= 0 ? 0 : sisa_pembayaran).trigger('input');
            $('#hangus').mask('000.000.000', {
                reverse: true
            }).val(fix_uang_hangus).trigger('input');

            hitungBayar();

            // sewa_tempat.replace(/\./g, "");
        }

        function hitungBayar() {
            let jenis = $('#jenis_pembayaran').find(':selected').val();
            let total_harga = ($('#total_harga').val()).replace(/\./g, "");
            let jumlah_bayar = 0;

            if (jenis == 'full') {
                jumlah_bayar = total_harga;
            } else {
                let potongan = (20 / 100) * parseInt(total_harga);
                jumlah_bayar = potongan;
            }
            $('#jumlah_bayar').mask('000.000.000', {
                reverse: true
            }).val(jumlah_bayar).trigger('input');


        }
    </script>
@endsection
