@extends('front.master')
@section('custom-css')
    <style>
        .invalid-feedback {
            color: red;
            margin-top: -10px;
        }

        .is-invalid {
            margin-bottom: -3px !important;
        }
    </style>
@endsection
@section('information')
    <div class="inner-information-text">
        <div class="container">
            <h3>Register</h3>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li class="active">Register</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <section id="contant" class="contant main-heading team">
        <div class="row">
            <div class="container">
                <div class="contact">

                    <div class="col-md-3">

                    </div>
                    <div class="col-md-6">
                        @if (session('error'))
                            <div class="alert alert-danger" style="text-align: left">
                                <strong>{{ session('error') }}</strong>
                            </div>
                        @endif
                        @if (session('success-al'))
                            <div class="alert alert-success" style="text-align: left">
                                <strong>{{ session('success-al') }}</strong>
                            </div>
                        @endif

                        <div class="contact-us">
                            <form method="post" action="{{ route('register.post') }}" id="userAdd"
                                enctype="multipart/form-data">
                                @csrf
                                <ul>
                                    <li><input type="text" id="name" name="name" required placeholder="Nama *">
                                    </li>
                                    <li><input type="email" id="email" name="email" required placeholder="Email *">
                                    </li>
                                    <li>
                                        <textarea name="alamat" minlength="5" id="message" required placeholder="Alamat"></textarea>
                                    </li>
                                    <li><input type="password" id="password" minlength="6" name="password" required
                                            placeholder="Password *"></li>
                                    <li><input type="password" id="password_confirm" name="password_confirm" required
                                            placeholder="Konfirmasi Password *"></li>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label style="float: left">Foto </label>
                                                <input type="file" onchange="readFile(this,'preview')" id="input_image"
                                                    class="form-control" name="logo" accept="image/*">
                                                <br>
                                                <small>Format yang di dukung : jpg,jpeg,png</small> <br>
                                                <small>Ukuran File Maksimal : 2MB</small>
                                            </div>
                                            <div class="col-md-4">
                                                <img src="https://png.pngtree.com/png-vector/20190820/ourmid/pngtree-no-image-vector-illustration-isolated-png-image_1694547.jpg"
                                                    alt="" class="img-thumbnail" id="preview_image">
                                            </div>
                                        </div>

                                    </div>
                                    <button type="submit" class="btn btn-danger btn-block rounded-0">Daftar</button>
                                    <li><small>Sudah Punya Akun ?</small> <a href="{{ route('login.home') }}"
                                            class="text-danger"><u>Login disini</u></a> </li>
                                </ul>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-js')
    <script>
        // CKEDITOR.replace('editor_deskripsi');
        // $('#tabelItem').DataTable();
        var runValidator = function() {
            var form = $('#userAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    password: {
                        required: true,
                        // pwcheck: true,
                        // minlength: 8
                    },
                    password_confirm: {
                        required: true,
                        equalTo: "#password"
                    },
                },
                messages: {
                    name: {
                        required: 'Nama Harus diisi !',
                    },

                    email: {
                        required: "Email harus diisi !",
                        email: "Masukan format email yang benar !",
                    },
                    alamat: {
                        required: "Alamat harus diisi !",
                        minlength: 'Alamat harus diisi setidaknya 5 karakter !',
                    },
                    password : {
                        required : 'Password harus diisi !',
                        minlength : 'Password minimal 6 Karakter !',
                    },
                    password_confirm : {
                        required : 'Password harus diisi !',
                        minlength : 'Password minimal 6 Karakter !',
                        equalTo : 'Konfirmasi Password harus sama !',
                    }
                },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>
@endsection
