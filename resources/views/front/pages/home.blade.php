@extends('front.master')
@section('custom-css')
@endsection
@section('content')
    <div class="matchs-info">
        @foreach ($lapangan as $item)
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="full">
                        <div class="right-match-time"
                            style="background-image: url('{{ asset('img/item/' . $item->foto) }}')">
                            <h3 style="color: white;font-size: 2.3em;font-weight: bold">{{ $item->name }}</h3>
                            <ul id="countdown-1" class="countdown" style="color: white">
                                @if (!empty($booking))
                                    @if ($detail_booking->item_id == $item->id)
                                        <li><span class="days">{{ date('H:i', strtotime($cur_hour->jam_awal)) }} -
                                                {{ date('H:i', strtotime($detail->jam->jam_akhir)) }}</span> </li>
                                        <li><span class="days" style="font-size: 0.8em">Lapangan Sedang Digunakan <strong>
                                                    {{ $booking->team_name }} </strong></span> </li>
                                    @else
                                        @if (!empty($is_tutup))
                                            <li><span class="days">{{ $cur_hour->display }}</span> </li>
                                            <li><span class="days"><strong>De'Premiere Sudah Tutup</strong></span> </li>
                                        @else
                                            <li><span class="days">{{ $cur_hour->display }}</span> </li>
                                            <li><span class="days">Lapangan Tersedia</span> </li>
                                        @endif
                                    @endif
                                @else
                                    @if (!empty($is_tutup))
                                        <li><span class="days">{{ $cur_hour->display }}</span> </li>
                                        <li><span class="days"><strong>De'Premiere Sudah Tutup</strong></span> </li>
                                    @else
                                        <li><span class="days">{{ $cur_hour->display }}</span> </li>
                                        <li><span class="days">Lapangan Tersedia</span> </li>
                                    @endif
                                @endif


                            </ul>
                            {{-- <span>{!! $item->desc !!}</span> --}}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach


    </div>
    
@endsection
@section('custom-js')
@endsection
