<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Detail Informasi</h3>

        </div>
        @if (!empty($edit_id))
            <form action="{{ route('booking.form.edit') }}" method="POST">
            @else
                <form action="{{ route('booking.form') }}" method="POST" id="bookingForm">
        @endif
        @csrf
        <input type="hidden" name="item_id" value="{{ $jadwal->item_id }}">
        <input type="hidden" name="hari_id" value="{{ $jadwal->hari_id }}">
        <input type="hidden" name="jam_id" value="{{ $jadwal->jam_id }}">
        <input type="hidden" name="tanggal" value="{{ $tanggal }}">
        @if (!empty($edit_id))
            <input type="hidden" name="edit_id" value="{{ $edit_id }}">
        @endif
        <div class="modal-body">
            <table style="width:100%;color:black">
                <tr>
                    <td style="width: 200px">Kode Lapangan</td>
                    <td> : </td>
                    <td> {{ $jadwal->item->kode }} </td>
                </tr>
                <tr>
                    <td>Lapangan</td>
                    <td> : </td>
                    <td> {{ $jadwal->item->name }} </td>
                </tr>
                <tr>
                    <td>Jadwal</td>
                    <td> : </td>
                    <td> {{ $jadwal->hari->name }} | {{ $jadwal->jam->display }}</td>
                </tr>
                <tr>
                    <td>Tanggal</td>
                    <td> : </td>
                    <td> {{ \App\Helper\helper::tgl_indo($tanggal) }} </td>
                </tr>
                <tr>
                    <td>Harga Sewa</td>
                    <td> : </td>
                    <td>Rp. {{ number_format($jadwal->harga) }}/Jam </td>
                </tr>
                @php
                    $cek = \App\Detail_Booking::where([
                        'item_id' => $jadwal->item->id,
                        'hari_id' => $jadwal->hari->id,
                        'jam_id' => $jadwal->jam->id,
                        'tanggal' => $tanggal,
                    ])
                        ->whereHas('parents', function ($q) {
                            $q->whereIn('status_pembayaran', ['success', 'siap-digunakan']);
                        })
                        ->first();
                    
                @endphp
                @if ($cek != null)
                    <tr>
                        <td>Telah Di Booking Oleh</td>
                        <td> : </td>
                        <td> {{ $cek->parents->team_name }} </td>
                    </tr>
                @endif

            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="buttons bgrey" data-dismiss="modal">Close</button>
            @if ($info == 'booking')
                @if (!empty($edit_id))
                    <button type="submit" class="buttons binfo">Pilih</button>
                @else
                    <button type="submit" class="buttons binfo">Booking</button>
                @endif

            @endif

            @if (!empty($info_booking))
                @if ($cek == null)
                    <button type="button" onclick="simpanHistory()" class="buttons binfo">Booking</button>
                @endif
            @endif
        </div>
        </form>
    </div>
</div>
