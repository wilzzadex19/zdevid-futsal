@extends('front.master')
@section('custom-css')
@endsection
@section('information')
    <div class="inner-information-text">
        <div class="container">
            <h3>Edit Jadwal Booking</h3>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li class="active">Edit Jadwal Booking</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <section id="contant" class="contant main-heading team">
        <div class="row">
            <div class="container">
                <div class="contact">

                    <div class="col-md-12">
                        <div class="contact-info">
                            <div class="kode-section-title">
                                {{-- <h3>Histori Booking Saya</h3> --}}
                            </div>
                            <div class="kode-forminfo">
                                <div class="feature-matchs">
                                    <small style="color: black">Ketentuan :</small> <br>
                                    <small style="color: black">Pergantian Jadwal Bisa dilakukan maksimal 1 Hari sebelum
                                        jadwal Sewa.</small>
                                    <br>
                                    <br>
                                    <div class="table-resposive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="vertical-align: middle">No</th>
                                                    <th style="vertical-align: middle">Kode Booking</th>
                                                    <th style="vertical-align: middle">Nama Tim</th>
                                                    <th style="vertical-align: middle">Tanggal Transaksi</th>
                                                    <th style="vertical-align: middle">Tanggal Penyewaan</th>
                                                    <th style="vertical-align: middle">Status</th>
                                                    <th style="vertical-align: middle">Total</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($booking as $key => $item)
                                                    @php
                                                        // $times = strtotime($item->detail[0]->tanggal . ' ' . $item->detail[0]->jam->jam_awal);
                                                        $dec_date = strtotime('-1 day', strtotime($item->detail[0]->tanggal . ' ' . $item->detail[0]->jam->jam_awal));
                                                        // dd(date('Y-m-d H:i:s',$dec_date));
                                                        $cur_dates = time();
                                                        $no = 1;
                                                    @endphp
                                                    @if ($dec_date > $cur_dates)
                                                        <tr>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{{ $item->kode }}</td>
                                                            <td>{{ $item->team_name }}</td>
                                                            <td>{{ \App\Helper\helper::tgl_indo_jam($item->created_at) }}
                                                            </td>
                                                            <td>{{ \App\Helper\helper::tgl_indo($item->detail[0]->tanggal) }}
                                                                - {{ $item->detail[0]->jam->jam_awal }}</td>
                                                            @if ($item->status_pembayaran == 'pending')
                                                                <td><span class="badge badge-warning"
                                                                        style="background-color: rgb(152, 149, 74)">Menunggu
                                                                        Pembayaran</span> <br>(Bayar Sebelum
                                                                    {{ \App\Helper\helper::tgl_indo_jam($item->payment_exp) }})
                                                                </td>
                                                            @elseif($item->status_pembayaran == 'success' || $item->status_pembayaran == 'siap-digunakan')
                                                                <td><span class="badge badge-success"
                                                                        style="background-color: green">
                                                                        @if ($item->jenis_pembayaran == 'dp')
                                                                            @if ($item->is_lunas == 1)
                                                                                Pembayaran Pelunasan Sukses
                                                                            @else
                                                                                Pembayaran DP Sukses
                                                                            @endif
                                                                        @else
                                                                            Pembayaran Full Sukses
                                                                        @endif
                                                                    </span></td>
                                                            @elseif($item->status_pembayaran == 'unfinish')
                                                                <td><span class="badge badge-success"
                                                                        style="background-color: red">Transaksi
                                                                        Dibatalkan</span></td>
                                                            @elseif($item->status_pembayaran == 'selesai')
                                                                <td><span class="badge badge-success"
                                                                        style="background-color: blue">Selesai</span></td>
                                                            @endif
                                                            <td>
                                                                @if ($item->jenis_pembayaran == 'full')
                                                                    Total Rp. {{ number_format($item->total_harga) }}
                                                                @else
                                                                    Total Rp. {{ number_format($item->total_harga) }} <br>
                                                                    DP
                                                                    20% = Rp.
                                                                    {{ number_format($item->total_dp) }}
                                                                @endif
                                                            </td>
                                                            <td>

                                                                <a href="javascript:void(0)" onclick="lihatDetail(this)"
                                                                    id="{{ $item->id }}" style="font-size: 18px"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="Detail"><i class="fa fa-list"></i></a>
                                                                @if ($item->status_pembayaran == 'pending')
                                                                    <a href="{{ $item->payment_url }}" target="_blank"
                                                                        style="font-size: 18px" data-toggle="tooltip"
                                                                        data-placement="top" title="Lanjutkan Pembayaran"><i
                                                                            class="fa fa-shopping-cart"></i></a>
                                                                @elseif($item->status_pembayaran == 'success')
                                                                    <a href="{{ route('edit-jadwal-q', $item->id) }}"
                                                                        style="font-size: 18px" data-toggle="tooltip"
                                                                        data-placement="top" title="Edit Jadwal"><i
                                                                            class="fa fa-edit"></i></a>
                                                                    {{-- <a href="{{ route('booking.print',$item->id) }}" target="_blank" style="font-size: 18px" data-toggle="tooltip" data-placement="top"
                            title="Print Bukti Booking"><i
                                class="fa fa-print"></i></a> --}}
                                                                @elseif($item->status_pembayaran == 'selesai')
                                                                    {{-- <a href="{{ route('booking.print',$item->id) }}" target="_blank" style="font-size: 18px" data-toggle="tooltip" data-placement="top"
                            title="Print Bukti Booking"><i
                                class="fa fa-print"></i></a> --}}
                                                                @elseif($item->status_pembayaran == 'siap-digunakan')
                                                                    {{-- <a href="{{ route('booking.print',$item->id) }}" target="_blank" style="font-size: 18px" data-toggle="tooltip" data-placement="top"
                            title="Print Bukti Booking"><i
                                class="fa fa-print"></i></a> --}}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>

                                        </table>
                                    </div>

                                    {{-- <br>
                                    <small>Keterangan :</small> --}}


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">

    </div>
@endsection
@section('custom-js')
    <script>
        function lihatDetail(obj) {
            let id = $(obj).attr('id');
            // console.log(id);
            $.ajax({
                type: 'get',
                url: '{{ route('dataBooking.detail') }}',
                data: {
                    id: id,
                },
                success: function(res) {
                    $('#modalDetail').html(res);
                    $('#modalDetail').modal('show');
                }
            })
        }
    </script>
@endsection
