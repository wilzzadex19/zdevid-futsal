@extends('front.master')
@section('custom-css')
@endsection
@section('information')
    <div class="inner-information-text">
        <div class="container">
            <h3>Jadwal</h3>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li class="active">Jadwal</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <section id="contant" class="contant main-heading team">
        <div class="row">
            <div class="container">
                <div class="contact">

                    <div class="col-md-12">
                        <div class="contact-info">
                            {{-- <div class="kode-section-title"> --}}
                            <table style="width: 100%" border="0">
                                <tr>
                                    <td>
                                        <h3>Daftar Jadwal</h3>
                                    </td>
                                    <td style="float: right">
                                        @if ($week != 7)
                                            <a href="{{ route('jadwal-front') }}?week=7" class="buttonss binfo">
                                                Minggu Sekarang</a>
                                            <a href="{{ route('jadwal-front') }}?week={{ $week - 7 }}" class="buttonss bgrey"><i class="fa fa-angle-left"></i>
                                                Prev</a>
                                        @endif

                                        <a href="{{ route('jadwal-front') }}?week={{ $week + 7 }}" class="buttonss bgrey">Next <i
                                                class="fa fa-angle-right"></i></a>
                                    </td>
                                </tr>
                            </table>

                            {{-- </div> --}}
                            <div class="kode-forminfo">
                                @foreach ($lapangan as $item)
                                    <div class="feature-matchs">
                                        <h5>{{ $item->name }}</h5>
                                        <div class="table-resposive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th style="vertical-align: middle">Jam</th>
                                                        @foreach ($tanggal as $value)
                                                            <th
                                                                style="background-color: {{ \App\Helper\helper::to_hari(date('D')) == \App\Helper\helper::to_hari($value->format('D')) ? '#90EE90' : '' }}">
                                                                {{ \App\Helper\helper::to_hari($value->format('D')) }}
                                                                <br>
                                                                <small>{{ \App\Helper\helper::tgl_indo($value->format('Y-m-d')) }}</small>
                                                            </th>
                                                        @endforeach


                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                        $jadwal = \App\Jadwal::where('item_id', $item->id)
                                                            ->groupBy('jam_id')
                                                            ->orderBy('jam_id', 'asc')
                                                            ->get();
                                                    @endphp
                                                    @foreach ($jadwal as $j)
                                                        <tr>
                                                            <td>{{ $j->jam->display }}</td>
                                                            @foreach ($tanggal as $value)
                                                                @foreach ($hari as $h)
                                                                    @if ($h->code == $value->format('D'))
                                                                        @php
                                                                            $booked = false;
                                                                            $cur_date = date('Y-m-d H:i:s');
                                                                            $jam_jadwal = $value->format('Y-m-d') . ' ' . $j->jam->jam_awal;
                                                                            $btn = 'info';
                                                                            if (strtotime($jam_jadwal) < strtotime($cur_date)) {
                                                                                $btn = 'info';
                                                                            }
                                                                            
                                                                            $is_booked = \App\Detail_Booking::where([
                                                                                'item_id' => $item->id,
                                                                                'hari_id' => $h->id,
                                                                                'jam_id' => $j->jam_id,
                                                                                'tanggal' => $value->format('Y-m-d'),
                                                                            ])->first();
                                                                            
                                                                            if ($is_booked != null) {
                                                                                try {
                                                                                    if ($is_booked->parents->status_pembayaran == 'success' || $is_booked->parents->status_pembayaran == 'siap-digunakan') {
                                                                                        $booked = true;
                                                                                        $btn = 'info';
                                                                                    }
                                                                                } catch (\Throwable $th) {
                                                                                    // dump($is_booked->id);
                                                                                }
                                                                            }
                                                                            
                                                                        @endphp
                                                                        @if (strtotime($jam_jadwal) > strtotime($cur_date))
                                                                            <td class="text-center"
                                                                                style="background-color: {{ !empty($booked) ? '#0882e5' : (strtotime($jam_jadwal) < strtotime($cur_date) ? '#d65252' : '') }}">

                                                                                <span class="badge badge-success"
                                                                                    data-item_id="{{ $item->id }}"
                                                                                    data-hari_id="{{ $h->id }}"
                                                                                    data-jam_id="{{ $j->jam_id }}"
                                                                                    data-tanggal="{{ $value->format('Y-m-d') }}"
                                                                                    data-info="{{ $btn }}"
                                                                                    onclick="getInfo(this)"
                                                                                    style="background-color: {{ $btn == 'info' ? 'grey' : 'grey' }};cursor: pointer;">

                                                                                    @if ($btn == 'info')
                                                                                        Info
                                                                                    @else
                                                                                        Booking
                                                                                    @endif
                                                                                </span>


                                                                            </td>
                                                                        @else
                                                                            <td class="text-center"
                                                                                style="background-color: {{ strtotime($jam_jadwal) < strtotime($cur_date) ? '#d65252' : '' }}">


                                                                            </td>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        <br>
                                        <small>Keterangan :</small>
                                        <table border="0">
                                            <tr>
                                                <td style="vertical-align: middle">
                                                    <div class='box green' style="background-color: grey"></div>
                                                </td>
                                                <td> &nbsp; Tersedia</td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: middle">
                                                    <div class='box green'></div>
                                                </td>
                                                <td> &nbsp; Hari Ini</td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: middle">
                                                    <div class='box red'></div>
                                                </td>
                                                <td> &nbsp; Jam Terlewat</td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: middle">
                                                    <div class='box blue'></div>
                                                </td>
                                                <td> &nbsp; Telah Di Booking</td>
                                            </tr>
                                            {{-- <tr>
                                                <td style="vertical-align: middle">
                                                    <div class='box blue' style="background-color: yellow"></div>
                                                </td>
                                                <td> &nbsp; Sedang Berlangsung</td>
                                            </tr> --}}
                                        </table>

                                    </div>
                                    <br>
                                    <br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">

    </div>
@endsection
@section('custom-js')
    <script>
        function getInfo(obj) {
            let item_id = $(obj).attr('data-item_id');
            let hari_id = $(obj).attr('data-hari_id');
            let jam_id = $(obj).attr('data-jam_id');
            let tanggal = $(obj).attr('data-tanggal');
            let info = $(obj).attr('data-info');


            // console.log(info);
            $.ajax({
                type: 'get',
                url: '{{ route('booking.getInfo') }}',
                data: {
                    item_id: item_id,
                    hari_id: hari_id,
                    jam_id: jam_id,
                    tanggal: tanggal,
                    info: info,
                    info_booking: true
                },
                success: function(res) {
                    $('#modalDetail').html(res);
                    $('#modalDetail').modal('show');
                }
            })
        }

        function simpanHistory() {
            let formData = $('#bookingForm').serialize();
            $.ajax({
                url: '{{ route('booking.save.history') }}',
                type: 'get',
                data: formData,
                success: function(res) {
                    Swal.fire({
                        title: 'Silahkan login terlebih dahulu',

                        icon: 'warning'
                    }).then((result) => {
                        window.location.href = '{{ route('login.home') }}'

                    })
                },
                error: function() {
                    Swal.fire('', 'Kesalahan Sistem', 'error');
                }
            })
        }
    </script>
@endsection
