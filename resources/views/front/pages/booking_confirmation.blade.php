@extends('front.master')
@section('custom-css')
@endsection
@section('information')
    <div class="inner-information-text">
        <div class="container">
            <h3>Konfirmasi Booking</h3>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('booking') }}">Booking</a></li>
                <li class="active">Konfirmasi</li>
            </ul>
        </div>
    </div>
@endsection
@section('content')
    <section id="contant" class="contant main-heading team">
        <div class="row">
            <div class="container">
                <div class="contact">
                    <div class="col-md-2">

                    </div>
                    <div class="col-md-8" style="text-align: left">
                        @if (!empty($kadaluarsa))
                            <div class="alert alert-danger">
                                Pembayaran anda sudah kadaluarsa, silahkan ulangi kembali proses booking anda !
                            </div>
                        @endif
                        @if (!empty($is_paid))
                            <div class="alert alert-success">
                                Pembayaran berhasil !
                            </div>
                        @endif
                        <div class="kode-section-title">
                            <h3>Konfirmasi Pembayaran</h3>
                        </div>
                        <form action="{{ route('booking.pay') }}" method="POST">
                            @csrf
                            {{-- <input type="hidden" name="item_id" value="{{ $jadwal->item_id }}">
                    <input type="hidden" name="hari_id" value="{{ $jadwal->hari_id }}">
                    <input type="hidden" name="jam_id" value="{{ $jadwal->jam_id }}">
                    <input type="hidden" name="tanggal" value="{{ $tanggal }}"> --}}
                            <div class="form-group">
                                <label>Booking ID</label>
                                <input type="text" readonly value="{{ $booking->kode }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Bayar Sebelum</label>
                                <input type="text" readonly
                                    value="{{ \App\Helper\helper::tgl_indo_jam($booking->payment_exp) }}"
                                    class="form-control">
                            </div>
                            {{-- <div class="form-group">
                        <label>Booking ID</label>
                        <input type="text" readonly value="{{ $booking->kode }}" class="form-control">
                    </div> --}}
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" readonly value="{{ Auth::user()->name }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Nama Tim</label>
                                <input type="text" required name="team_name" readonly value="{{ $booking->team_name }}"
                                    class="form-control">
                            </div>
                            <div class="form-group">
                                {{-- <label>Durasi</label> --}}
                                <table style="width: 100%" class="table table-bordered">
                                    <tr>
                                        <td>Lapangan</td>
                                        <td>Jam</td>
                                        <td>Total Durasi</td>
                                    </tr>
                                    @foreach ($booking->detail as $key => $item)
                                        <tr>
                                            <td>{{ $item->item->name }}</td>
                                            <td>{{ $item->jam->display }}</td>
                                            @if ($key == 0)
                                                <td rowspan="{{ count($booking->detail) }}">{{ count($booking->detail) }}
                                                    Jam</td>
                                            @endif
    
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
    
                            <div class="form-group">
                                <label>No Hp Penanggung Jawab</label>
                                <input type="text" required name="no_hp" readonly value="{{ $booking->no_hp }}"
                                    class="form-control">
                            </div>
                            {{-- <div class="form-group row">
                        <div class="col-md-6">
                            <label>Durasi (Jam)</label>
                            <select name="durasi" class="form-control" onchange="updateHarga(this)">
                                <option value="1">1</option>
                                @php
                                    $dur = 1;
                                @endphp
                                @foreach ($jam_tersedia as $item)
                                    <option value="{{ $dur }}">{{ $dur }}
                                        ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
                                        {{ date('H:i', strtotime($item->jam->jam_akhir)) }}) </option>
                                    @php
                                        $dur++;
                                    @endphp
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Total Harga</label>
                            <input type="text" readonly id="total_harga" value="{{ $booking->total_harga }}"
                                name="total_harga" class="form-control">
                        </div>
                    </div> --}}
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea name="keterangan" readonly class="form-control" cols="30" rows="5">{{ $booking->keterangan }}</textarea>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Pembayaran</label>
                                    <select disabled name="jenis_pembayaran" id="jenis_pembayaran" class="form-control">
                                        <option value="full" {{ $booking->jenis_pembayaran == 'full' ? 'selected' : '' }}>
                                            Full</option>
                                        <option value="dp" {{ $booking->jenis_pembayaran == 'dp' ? 'selected' : '' }}>DP
                                            (Minimal 20% Total Harga)</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Jumlah Dibayar</label>
                                    <input type="text" readonly name="jumlah_bayar" id="jumlah_bayar"
                                        value="{{ $booking->jenis_pembayaran == 'full' ? $booking->total_harga : $booking->total_dp }}"
                                        name="total_harga" class="form-control">
                                </div>
                            </div>
    
                            @if ($booking->jenis_pembayaran == 'dp')
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label>Total Biaya Sewa Lapangan</label>
                                    <input type="text" readonly
                                        value="{{ number_format($booking->total_harga)  }}"
                                        name="" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Sisa Pembayaran</label>
                                    <input type="text" readonly
                                        value="{{  number_format($booking->total_harga - $booking->total_dp) }}"
                                        name="" class="form-control">
                                </div>
                            </div>
                            @endif
                            {{-- @dump($booking->payment_url) --}}
    
                            @if (!empty($is_paid))
                                <a href="{{ route('booking') }}" class="buttons binfo">Lihat Histori</a>
                            @else
                                <a href="{{ route('booking') }}" class="buttons bgrey">Batalkan</a>
                            @endif
                            @if (!empty($kadaluarsa) || !empty($is_paid))
                            @else
                                @if (!empty($is_url))
                                    <input type="hidden" name="payment_url" value="{{ $booking->payment_url }}">
                                    <button type="submit" target="_blank" class="buttons binfo">Proses
                                        Pembayaran</button>
                                @else
                                    <a href="" class="buttons binfo">Refresh</a>
                                @endif
                            @endif
    
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#total_harga').mask('000.000.000', {
                reverse: true
            });
            $('#jumlah_bayar').mask('000.000.000', {
                reverse: true
            });
        })

        $('#jenis_pembayaran').on('change', function() {
            hitungBayar();
        })

        function updateHarga(obj) {
            let dur = $(obj).find(':selected').val();
            let harga = '';
            let total_harga = parseInt(harga) * parseInt(dur);
            $('#total_harga').mask('000.000.000', {
                reverse: true
            }).val(total_harga).trigger('input');

            hitungBayar();

            // sewa_tempat.replace(/\./g, "");
        }

        function hitungBayar() {
            let jenis = $('#jenis_pembayaran').find(':selected').val();
            let total_harga = ($('#total_harga').val()).replace(/\./g, "");
            let jumlah_bayar = 0;

            if (jenis == 'full') {
                jumlah_bayar = total_harga;
            } else {
                let potongan = (20 / 100) * parseInt(total_harga);
                jumlah_bayar = potongan;
            }
            $('#jumlah_bayar').mask('000.000.000', {
                reverse: true
            }).val(jumlah_bayar).trigger('input');


        }
    </script>
@endsection
