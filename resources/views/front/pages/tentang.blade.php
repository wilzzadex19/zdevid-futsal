@extends('front.master')
@section('custom-css')
@endsection
@section('information')
<div class="inner-information-text">
    <div class="container">
        <h3>Tentang Kami</h3>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Tentang Kami</li>
        </ul>
    </div>
</div>
@endsection
@section('content')
 <div class="team-holder theme-padding">
        <div class="container">
            <div class="main-heading-holder">
                <div class="main-heading sytle-2">
                    <h2>Tentang Kami</h2>
                    <p>{{ config('global.app_setting')->app_description }}</p>
                </div>
            </div>
            
        </div>
    </div>
@endsection
@section('custom-js')
@endsection
