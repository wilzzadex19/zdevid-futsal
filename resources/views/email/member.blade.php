<style>
    * {
        font-family: "Arial";
    }
</style>

<body style="width:100%;text-align:center;">
    <div style="width:100%;text-align:center;margin-top:20px;">
        <img src="{{ asset('assets/img/' . config('global.app_setting')->app_logo) }}" alt="">
    </div>
    <br>
    <div class="text-center mt-3">
        <h4 class="mt-4">
            
            Pendaftaran Member

        </h4>
        <h4 class="mt-3 mb-2">
            Halo {{ $member->name }},
            Selamat akun anda berhasil di daftarkan.
            <br>
            Email : {{ $member->email }}
            <br>
            Password : futsal2022
        </h4>
       

        <small>*email ini dikirim secara otomatis dan tidak perlu dibalas.</small>
    </div>
    <br> <br>
    <div style="border-radius:10px;background:#2c51c0; padding-left:80px;padding-right:80px;padding-top:40px;padding-bottom:40px;color:white;text-align:left">
        <table style="width:100%;color:white">
            <tr>
                <td>
                    Rivandhika Futsal <br>
                    Jl. Raya Leles No. 8 Garut
                </td>
                <td>
                    Copyright 2022 ©
                </td>
            </tr>
            <tr>
                <td> <br> <br>
                    Telp : +62 857 - 7455 -3807
                </td>
            </tr>
        </table>
    </div>
</body>
