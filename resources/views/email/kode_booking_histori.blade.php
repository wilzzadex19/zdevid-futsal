<style>
    * {
        font-family: "Arial";
    }
</style>

<body style="width:100%;text-align:center;">
    <div style="width:100%;text-align:center;margin-top:20px;">
        <img src="{{ asset('assets/img/' . config('global.app_setting')->app_logo) }}" width="150px" class="text-center"
            alt="">
    </div>
    <br>
    <div class="mt-3">
        <h4 class="mt-4">

            Kode Booking

        </h4>
        <h4 class="mt-3 mb-2">
            Halo {{ $booking->users->name }},
            Pergantian Jadwal Sewa Lapangan Anda Telah Berhasil Dilakukan
            <br>
            <p style="font-size: 30px">{{ $booking->kode }}</p>
            <br>
            Simpan email ini untuk bukti.
            <br>
            <br>
        </h4>

        <table style="text-align: left;width:70%;margin-left: auto; margin-right: auto;" border="0">
            <tr>
                <td style="width :49%;text-align:right">Nama Penyewa</td>
                <td style="width: 2%;text-align:center">:</td>
                <td style="width: 49%">{{ $booking->users->name }}</td>
            </tr>
            {{-- <tr>
                <td>Kode Pembayaran</td>
                <td style="text-align:center">:</td>
                <td>{{ $booking->payment_code }}</td>
            </tr> --}}
            <tr>
                <td style="text-align:right">Nama Tim</td>
                <td style="text-align:center">:</td>
                <td>{{ $booking->team_name }}</td>
            </tr>
            <tr>
            <tr>
                <td style="text-align:right">Lapangan</td>
                <td style="text-align:center">:</td>
                <td>{{ $booking->detail[0]->item->name }}</td>
            </tr>
            <tr>
                <td style="text-align:right">Lama Pemakaian</td>
                <td style="text-align:center">:</td>
                <td>{{ count($booking->detail) }} Jam</td>
            </tr>
            <tr>
                <td style="text-align:right">Tanggal Pembayaran</td>
                <td style="text-align:center">:</td>
                <td>{{ \App\Helper\helper::tgl_indo_jam($booking->tanggal_bayar) }}</td>
            </tr>

            <tr>
                <td style="text-align:right">Tanggal Awal Penyewaan (Jadwal Lama)</td>
                <td style="text-align:center">:</td>
                <td>{{ \App\Helper\helper::tgl_indo($detail_histori[0]->tanggal) }} -
                    (
                    {{ date('H:i', strtotime($detail_histori[0]->jam->jam_awal)) . ' - ' . date('H:i', strtotime($detail_histori[count($detail_histori) - 1]->jam->jam_akhir)) }})
                </td>
            </tr>
            <tr>
                <td style="text-align:right">Tanggal Pergantian (Jadwal Baru)</td>
                <td style="text-align:center">:</td>
                <td>{{ \App\Helper\helper::tgl_indo($booking->detail[0]->tanggal) }} -
                    ( {{ date('H:i', strtotime($booking->detail[0]->jam->jam_awal)) }} -
                    {{ date('H:i', strtotime($booking->detail[count($booking->detail) - 1]->jam->jam_awal)) }})</td>
            </tr>


            <tr>
                <td style="text-align:right">Telah Bayar</td>
                <td style="text-align:center">:</td>
                <td>
                    @if ($booking->jenis_pembayaran == 'dp')
                        Rp. {{ number_format($booking->total_dp) }}
                    @else
                        Rp. {{ number_format($booking->total_harga) }}
                    @endif
                </td>
            </tr>
            <tr>
                <td style="text-align:right">Sisa Pembayaran</td>
                <td style="text-align:center">:</td>
                <td>
                    @if ($booking->jenis_pembayaran == 'dp')
                        Rp. {{ number_format($booking->total_harga - $booking->total_dp) }}
                    @else
                        Rp. 0 (Lunas)
                    @endif
                </td>
            </tr>


            {{-- @if (empty($is_admin))
                @if ($booking->jenis_pembayaran == 'dp')
                    <tr>
                        <td>Sisa Pembayaran</td>
                        <td style="text-align:center">:</td>
                        <td>Rp. {{ number_format($booking->total_dp) }}</td>
                    </tr>
                @endif
            @endif --}}
            <tr>
                <td style="text-align:right">Total Harga</td>
                <td style="text-align:center">:</td>
                <td>Rp. {{ number_format($booking->total_harga) }}</td>
            </tr>


        </table>
        <br>
        <small>*email ini dikirim secara otomatis dan tidak perlu dibalas.</small>
    </div>
    <br> <br>
    <div
        style="border-radius:10px;background:#2c51c0; padding-left:80px;padding-right:80px;padding-top:40px;padding-bottom:40px;color:white;text-align:left">
        <table style="width:100%;color:white">
            <tr>
                <td>
                    Rivandhika Futsal <br>
                    Jl. Raya Leles No. 8 Garut
                </td>
                <td>
                    Copyright 2024©
                </td>
            </tr>
            <tr>
                <td> <br> <br>
                    Telp : +62 857 - 7455 -3807
                </td>
            </tr>
        </table>
    </div>
</body>
