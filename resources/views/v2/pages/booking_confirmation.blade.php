@extends('v2.master')
@section('custom-css')
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Konfirmasi Pembayaran</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">

        <div class="col-md-12" style="text-align: left">
            <div class="card">
                <div class="card-body">


                    @if (!empty($kadaluarsa))
                        <div class="alert alert-danger">
                            Pembayaran anda sudah kadaluarsa, silahkan ulangi kembali proses booking anda !
                        </div>
                    @endif
                    @if (!empty($is_paid))
                        <div class="alert alert-success">
                            Pembayaran berhasil !
                        </div>
                    @endif
                    <form action="{{ route('booking.pay') }}" method="POST">
                        @csrf
                        {{-- <input type="hidden" name="item_id" value="{{ $jadwal->item_id }}">
                <input type="hidden" name="hari_id" value="{{ $jadwal->hari_id }}">
                <input type="hidden" name="jam_id" value="{{ $jadwal->jam_id }}">
                <input type="hidden" name="tanggal" value="{{ $tanggal }}"> --}}
                        <div class="form-group">
                            <label>Booking ID</label>
                            <input type="text" readonly value="{{ $booking->kode }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Bayar Sebelum</label>
                            <input type="text" readonly
                                value="{{ \App\Helper\helper::tgl_indo_jam($booking->payment_exp) }}"
                                class="form-control">
                        </div>
                        {{-- <div class="form-group">
                    <label>Booking ID</label>
                    <input type="text" readonly value="{{ $booking->kode }}" class="form-control">
                </div> --}}
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" readonly value="{{ Auth::user()->name }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Nama Tim</label>
                            <input type="text" required name="team_name" readonly value="{{ $booking->team_name }}"
                                class="form-control">
                        </div>
                        <div class="form-group">
                            {{-- <label>Durasi</label> --}}
                            <table style="width: 100%" class="table table-bordered">
                                <tr>
                                    <td>Lapangan</td>
                                    <td>Jam</td>
                                    <td>Total Durasi</td>
                                </tr>
                                @foreach ($booking->detail as $key => $item)
                                    <tr>
                                        <td>{{ $item->item->name }}</td>
                                        <td>{{ $item->jam->display }}</td>
                                        @if ($key == 0)
                                            <td rowspan="{{ count($booking->detail) }}">{{ count($booking->detail) }}
                                                Jam</td>
                                        @endif

                                    </tr>
                                @endforeach
                            </table>
                        </div>

                        <div class="form-group">
                            <label>No Hp Penanggung Jawab</label>
                            <input type="text" required name="no_hp" readonly value="{{ $booking->no_hp }}"
                                class="form-control">
                        </div>
                        {{-- <div class="form-group row">
                    <div class="col-md-6">
                        <label>Durasi (Jam)</label>
                        <select name="durasi" class="form-control" onchange="updateHarga(this)">
                            <option value="1">1</option>
                            @php
                                $dur = 1;
                            @endphp
                            @foreach ($jam_tersedia as $item)
                                <option value="{{ $dur }}">{{ $dur }}
                                    ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
                                    {{ date('H:i', strtotime($item->jam->jam_akhir)) }}) </option>
                                @php
                                    $dur++;
                                @endphp
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label>Total Harga</label>
                        <input type="text" readonly id="total_harga" value="{{ $booking->total_harga }}"
                            name="total_harga" class="form-control">
                    </div>
                </div> --}}
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" readonly class="form-control" cols="30" rows="5">{{ $booking->keterangan }}</textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Pembayaran</label>
                                <select disabled name="jenis_pembayaran" id="jenis_pembayaran" class="form-control">
                                    <option value="full" {{ $booking->jenis_pembayaran == 'full' ? 'selected' : '' }}>
                                        Full</option>
                                    <option value="dp" {{ $booking->jenis_pembayaran == 'dp' ? 'selected' : '' }}>DP
                                        (Minimal 20% Total Harga)</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Jumlah Dibayar</label>
                                <input type="text" readonly name="jumlah_bayar" id="jumlah_bayar"
                                    value="{{ $booking->jenis_pembayaran == 'full' ? $booking->total_harga : $booking->total_dp }}"
                                    name="total_harga" class="form-control">
                            </div>
                        </div>

                        @if ($booking->jenis_pembayaran == 'dp')
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Total Biaya Sewa Lapangan</label>
                                <input type="text" readonly
                                    value="{{ number_format($booking->total_harga)  }}"
                                    name="" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>Sisa Pembayaran</label>
                                <input type="text" readonly
                                    value="{{  number_format($booking->total_harga - $booking->total_dp) }}"
                                    name="" class="form-control">
                            </div>
                        </div>
                        @endif
                        {{-- @dump($booking->payment_url) --}}

                        @if (!empty($is_paid))
                            <a href="{{ route('booking') }}" class="buttons binfo">Lihat Histori</a>
                        @else
                            <a href="{{ route('booking') }}" class="buttons bgrey">Batalkan</a>
                        @endif
                        @if (!empty($kadaluarsa) || !empty($is_paid))
                        @else
                            @if (!empty($is_url))
                                <input type="hidden" name="payment_url" value="{{ $booking->payment_url }}">
                                <button type="submit" target="_blank" class="buttons binfo">Proses
                                    Pembayaran</button>
                            @else
                                <a href="" class="buttons binfo">Refresh</a>
                            @endif
                        @endif

                    </form>
                </div>
            </div>
        </div>
    </div>
<br>
@endsection
@section('modal')
@endsection
@section('custom-js')
@endsection
