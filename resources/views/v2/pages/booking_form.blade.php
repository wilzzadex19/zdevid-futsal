@extends('v2.master')
@section('custom-css')
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Form Booking</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">


        <div class="col-md-3">
            <div class="card">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"> <strong> Detail Booking </strong></li>
                    <li class="list-group-item"><i class="fa fa-map-marker-alt"></i>
                        <strong>Lapangan:</strong> {{ $jadwal->item->kode }}
                        {{ $jadwal->item->name }}
                    </li>
                    <li class="list-group-item"> <i class="fa fa-clock"></i> <strong>Jadwal:</strong>
                        {{ $jadwal->hari->name }}
                        {{ $jadwal->jam->display }}</li>
                    <li class="list-group-item"> <i class="fa fa-calendar"></i>
                        <strong>Tanggal:</strong> {{ \App\Helper\helper::tgl_indo($tanggal) }}
                    </li>
                    <li class="list-group-item"> <i class="fa fa-dollar-sign"></i>
                        <strong>Harga:</strong> Rp. {{ number_format($jadwal->harga) }} /Jam
                    </li>
                </ul>
            </div>

        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="kode-section-title">
                        <h3>Formulir Booking</h3>
                    </div>
                    <form action="{{ route('booking.form.add') }}" method="POST" id="userAdd">
                        @csrf
                        <input type="hidden" name="item_id" value="{{ $jadwal->item_id }}">
                        <input type="hidden" name="hari_id" value="{{ $jadwal->hari_id }}">
                        <input type="hidden" name="jam_id" value="{{ $jadwal->jam_id }}">
                        <input type="hidden" name="tanggal" value="{{ $tanggal }}">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" readonly value="{{ Auth::user()->name }}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Nama Tim</label>
                            <input type="text" required name="team_name" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>No Hp Penanggung Jawab</label>
                            <input type="text" required name="no_hp" class="form-control">
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label>Durasi (Jam)</label>
                                <select name="durasi" class="form-control" required onchange="updateHarga(this)">
                                    {{-- <option value="1">1</option> --}}
                                    @php
                                        $dur = 1;
                                        $jam_ada = \App\Detail_Booking::where([
                                            'item_id' => $jadwal->item_id,
                                            'hari_id' => $jadwal->hari_id,
                                            'tanggal' => $tanggal,
                                        ])
                                            ->with('jam')
                                            ->where('jam_id', '>', $jadwal->jam_id)
                                            ->with('jam')
                                            ->first();
                                        
                                        if ($jam_ada != null) {
                                            $hasil = $jam_ada->jam_id - $jadwal->jam_id;
                                        }
                                        
                                    @endphp

                                    @foreach ($jam_tersedia as $key => $item)
                                        @if (empty($hasil))
                                            <option value="{{ $dur }}">{{ $dur }}
                                                ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
                                                {{ date('H:i', strtotime($item->jam->jam_akhir)) }})
                                            </option>
                                        @else
                                            <option value="{{ $dur }}" {{ $key < $hasil ? '' : 'disabled' }}>
                                                {{ $dur }}
                                                ({{ date('H:i', strtotime($jadwal->jam->jam_awal)) }} -
                                                {{ date('H:i', strtotime($item->jam->jam_akhir)) }})
                                            </option>
                                        @endif

                                        @php
                                            $dur++;
                                        @endphp
                                    @endforeach
                                </select>
                            </div>
                            {{-- {{ dump($hasil) }} --}}
                            {{-- {{ dump($hasil) }} --}}
                            <div class="col-md-6">
                                <label>Total Biaya Sewa Lapangan</label>
                                <input type="text" readonly id="total_harga" value="{{ $jadwal->harga }}"
                                    name="total_harga" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan" class="form-control" cols="30" rows="5"></textarea>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label>Pembayaran</label>
                                <select name="jenis_pembayaran" required id="jenis_pembayaran" class="form-control">
                                    <option value="full">Full</option>
                                    <option value="dp">DP (Minimal 20% Total Harga)</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Jumlah Yang Akan Dibayar</label>
                                <input type="text" required name="jumlah_bayar" id="jumlah_bayar" onkeyup="cekDp(this)"
                                    value="{{ $jadwal->harga }}" name="total_harga" class="form-control">

                            </div>
                            <div class="col-md-3" style="display: none" id="box_dp">
                                <label>Minimal Pembayaran</label>
                                <input type="text" readonly name="min_dp" id="min_dp" value="0"
                                    class="form-control">

                            </div>
                            <div class="col-md-3" style="display: none" id="box_dp2">
                                <label>Sisa Pembayaran</label>
                                <input type="text" readonly name="sisa_bayar" id="sisa_bayar" value="0"
                                    class="form-control">

                            </div>
                        </div>
                        <a href="{{ route('booking') }}" class="btn btn-secondary">Kembali</a>
                        <button class="btn btn-primary" id="btn_book">Booking</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <br>
@endsection
@section('modal')
@endsection
@section('custom-js')
    <script>
        // CKEDITOR.replace('editor_deskripsi');
        // $('#tabelItem').DataTable();
        var runValidator = function() {
            var form = $('#userAdd');
            var errorHandler = $('.errorHandler', form);
            var successHandler = $('.successHandler', form);
            form.validate({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'invalid-feedback',
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");

                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.next("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                ignore: "",
                rules: {
                    no_hp: {
                        digits: true,
                        required: true,
                        minlength: 11,
                        maxlength: 13,

                        // pwcheck: true,
                        // minlength: 8
                    },
                },
                messages: {
                    no_hp: {
                        required: 'No Hp Harus diisi !',
                        digits: 'Masukan format no hp yang benar !',
                        maxlength: 'Maksimal 13 digit !',
                        minlength: 'Minimal 11 digit !'


                    },
                    team_name: {
                        required: 'Nama Tim Harus diisi !',
                    },

                    email: {
                        required: "Email harus diisi !",
                        email: "Masukan format email yang benar !",
                    },
                    alamat: {
                        required: "Alamat harus diisi !",
                        minlength: 'Alamat harus diisi setidaknya 5 karakter !',
                    },
                    password: {
                        required: 'Password harus diisi !',
                        minlength: 'Password minimal 6 Karakter !',
                    },
                    // password_confirm : {
                    //     required : 'Password harus diisi !',
                    //     minlength : 'Password minimal 8 Karakter !',
                    //     equalTo : 'Konfirmasi Password harus sama !',
                    // }
                },
                errorElement: "em",
                invalidHandler: function(event, validator) { //display error alert on form submit
                    successHandler.hide();
                    errorHandler.show();
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                success: function(label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.validate ').removeClass('has-error').addClass('has-success').find(
                        '.symbol').removeClass('required').addClass('ok');
                },
                submitHandler: function(form) {
                    // $('#alert').hide();
                    successHandler.show();
                    errorHandler.hide();
                    // submit form
                    if (successHandler.show()) {
                        // myBlock()
                        form.submit();
                    }
                }
            });
        };
        runValidator();
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#total_harga').mask('000.000.000', {
                reverse: true
            });
            $('#jumlah_bayar').mask('000.000.000', {
                reverse: true
            });
        })

        $('#jenis_pembayaran').on('change', function() {
            hitungBayar();

        })

        function cekDp(obs) {
            let jenis = $('#jenis_pembayaran').find(':selected').val();
            if (jenis == 'dp') {
                let min_dp = ($('#min_dp').val()).replace(/\./g, "");
                let bayar = ($(obs).val()).replace(/\./g, "");
                if (parseInt(bayar) <  parseInt(min_dp)) {
                    $('#btn_book').prop('disabled',true);
                }else{
                    $('#btn_book').prop('disabled',false);
                }

                let total_harga = ($('#total_harga').val()).replace(/\./g, "");
            let sisa_bayar = parseInt(total_harga) - parseInt(bayar);
            $('#sisa_bayar').mask('000.000.000', {
                reverse: true
            }).val(sisa_bayar).trigger('input');
            }else{
                $('#sisa_bayar').mask('000.000.000', {
                reverse: true
            }).val(0).trigger('input');
            }

            
        }

        function updateHarga(obj) {
            let dur = $(obj).find(':selected').val();
            let harga = '{{ $jadwal->harga }}';
            let total_harga = parseInt(harga) * parseInt(dur);
            $('#total_harga').mask('000.000.000', {
                reverse: true
            }).val(total_harga).trigger('input');

            hitungBayar();

            // sewa_tempat.replace(/\./g, "");
        }

        function hitungBayar() {
            let jenis = $('#jenis_pembayaran').find(':selected').val();
            let total_harga = ($('#total_harga').val()).replace(/\./g, "");
            let jumlah_bayar = 0;

            if (jenis == 'full') {
                jumlah_bayar = total_harga;
                $('#box_dp').hide();
                $('#box_dp2').hide();
            } else {
                let potongan = (20 / 100) * parseInt(total_harga);
                jumlah_bayar = potongan;
                $('#box_dp').show();
                $('#box_dp2').show();
            }

           


           
            $('#jumlah_bayar').mask('000.000.000', {
                reverse: true
            }).val(jumlah_bayar).trigger('input');
            $('#min_dp').mask('000.000.000', {
                reverse: true
            }).val(jumlah_bayar).trigger('input');
            // $('#min_dp').val(jumlah_bayar);


        }
    </script>
@endsection
