@extends('v2.master')
@section('custom-css')
@endsection
@section('content')
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">

</div>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Histori Booking</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-resposive">
                        <table class="table table-bordered table-hover" id="mytable">
                            <thead>
                                <tr>
                                    <th style="vertical-align: middle">No</th>
                                    <th style="vertical-align: middle">Kode Booking</th>
                                    <th style="vertical-align: middle">Nama Tim</th>
                                    <th style="vertical-align: middle">Tanggal Transaksi</th>
                                    <th style="vertical-align: middle">Tanggal Penyewaan</th>
                                    <th style="vertical-align: middle">Status</th>
                                    <th style="vertical-align: middle">Total</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($booking as $key => $item)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $item->kode }}</td>
                                        <td>{{ $item->team_name }}</td>
                                        <td>{{ \App\Helper\helper::tgl_indo_jam($item->created_at) }}</td>
                                        <td>{{ \App\Helper\helper::tgl_indo($item->detail[0]->tanggal) }}
                                            - {{ $item->detail[0]->jam->jam_awal }}</td>
                                        @if ($item->status_pembayaran == 'pending')
                                            <td><span class="badge badge-warning"
                                                    style="background-color: rgb(152, 149, 74)">Menunggu
                                                    Pembayaran</span> <br>(Bayar Sebelum
                                                {{ \App\Helper\helper::tgl_indo_jam($item->payment_exp) }})
                                            </td>
                                        @elseif($item->status_pembayaran == 'success' || $item->status_pembayaran == 'siap-digunakan')
                                            <td><span class="badge badge-success" style="background-color: green">
                                                    @if ($item->jenis_pembayaran == 'dp')
                                                        Pembayaran DP Sukses
                                                    @else
                                                        Pembayaran Full Sukses
                                                    @endif
                                                </span></td>
                                        @elseif($item->status_pembayaran == 'unfinish')
                                            <td><span class="badge badge-success" style="background-color: red">Transaksi
                                                    Dibatalkan</span></td>
                                        @elseif($item->status_pembayaran == 'selesai')
                                            <td><span class="badge badge-success"
                                                    style="background-color: blue">Selesai</span></td>
                                        @endif
                                        <td>
                                            @if ($item->jenis_pembayaran == 'full')
                                                Total Rp. {{ number_format($item->total_harga) }}
                                            @else
                                                Total Rp. {{ number_format($item->total_harga) }} <br>
                                                @if ($item->is_edit == 2)
                                                    DP =
                                                @else
                                                    DP
                                                     =
                                                @endif
                                                Rp.
                                                {{ number_format($item->total_dp) }}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" class="btn btn-sm btn-info" onclick="lihatDetail(this)"
                                                id="{{ $item->id }}" data-toggle="tooltip"
                                                data-placement="top" title="Detail"><i class="fa fa-list"></i></a>
                                            @if ($item->status_pembayaran == 'pending')
                                                <a href="{{ $item->payment_url }}" class="btn btn-sm btn-warning" target="_blank"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="Lanjutkan Pembayaran"><i class="fa fa-shopping-cart"></i></a>
                                            @elseif($item->status_pembayaran == 'success')
                                                {{-- <a href="" data-toggle="tooltip" data-placement="top"
                                                title="Edit Jadwal"><i
                                                    class="fa fa-edit"></i></a> --}}
                                                <a href="{{ route('booking.print', $item->id) }}" class="btn btn-sm btn-warning" target="_blank"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="Print Bukti Booking"><i class="fa fa-print"></i></a>
                                            @elseif($item->status_pembayaran == 'selesai')
                                                <a href="{{ route('booking.print', $item->id) }}" class="btn btn-sm btn-warning" target="_blank"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="Print Bukti Booking"><i class="fa fa-print"></i></a>
                                            @elseif($item->status_pembayaran == 'siap-digunakan')
                                                <a href="{{ route('booking.print', $item->id) }}" target="_blank" class="btn btn-sm btn-warning"
                                                    data-toggle="tooltip" data-placement="top"
                                                    title="Print Bukti Booking"><i class="fa fa-print"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

   
@endsection
@section('modal')

@endsection
@section('custom-js')
    <script>
        $('#mytable').DataTable({
            ordering: false,
        });

        function lihatDetail(obj) {
            let id = $(obj).attr('id');
            // console.log(id);
            $.ajax({
                type: 'get',
                url: '{{ route('dataBooking.detail') }}',
                data: {
                    id: id,
                },
                success: function(res) {
                    $('#modalDetail').html(res);
                    $('#modalDetail').modal('show');
                }
            })
        }
    </script>
@endsection
