<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>{{ config('global.app_setting')->app_name }}</title>

<!-- Custom fonts for this template-->
<link href="{{ asset('assets') }}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

<!-- Custom styles for this template-->
<link href="{{ asset('assets') }}/css/sb-admin-2.min.css" rel="stylesheet">
<link href="{{ asset('assets') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="icon" href="{{ asset('assets/img/' . config('global.app_setting')->app_logo) }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


<style>
    .table th,
    .table td {
       padding: 2px !important;
       padding-right: 5px !important;
       padding-left: 5px !important;
    }

    .inner-page-banner {
        background-image: url('{{ asset('img/bg.png') }}') !important;
    }

    .select2-selection__rendered {
        line-height: 31px !important;
    }

    .select2-container .select2-selection--single {
        height: 35px !important;
    }

    .select2-selection__arrow {
        height: 34px !important;
    }

    .my-img {
        width: 40px;
        height: auto;
        max-width: 80%;
        max-height: 80%;
        object-fit: contain;
        border-radius: 20%;
        /* transform: translate(-50%, -50%); */
    }


    .buttons {
        background-color: #4CAF50;
        /* Green */
        border: none;
        color: white;
        padding: 10px 22px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 13px;
        margin: 2px 1px;
        cursor: pointer;
    }

    .binfo {
        background-color: #008CBA;
    }

    /* Blue */
    .bdanger {
        background-color: #f44336;
    }

    /* Red */
    .bblack {
        background-color: #e7e7e7;
        color: black;
    }

    /* Gray */
    .bgrey {
        background-color: #555555;
    }
</style>
