<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
        <div class="sidebar-brand-icon">
            <img src="{{ asset('assets/img/' . config('global.app_setting')->app_logo) }}" style="width: 100%" alt="">
        </div>
        <div class="sidebar-brand-text mx-3">{{ config('global.app_setting')->app_name }}</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ Request::is('my-dashboard*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
            <i class="fas fa-fw fa-home"></i>
            <span>Home</span></a>
    </li>
    <li class="nav-item {{ Request::is('jadwal*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('jadwal-front') }}">
            <i class="fas fa-fw fa-calendar-alt"></i>
            <span>Jadwal</span></a>
    </li>
    <li class="nav-item {{ Request::is('booking*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('booking') }}">
            <i class="fas fa-fw fa-user-clock"></i>
            <span>Booking Lapangan</span></a>
    </li>
    <li class="nav-item {{ Request::is('edit-jadwal*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('edit-jadwal') }}">
            <i class="fas fa-fw fa-edit"></i>
            <span>Pergantian Jadwal</span></a>
    </li>
    <li class="nav-item {{ Request::is('histori*') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('histori') }}">
            <i class="fas fa-fw fa-history"></i>
            <span>Histori Booking</span></a>
    </li>
  
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
