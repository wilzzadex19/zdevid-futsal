<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('get-info', 'BookingController@getInfo')->name('booking.getInfo');
Route::get('save-info', 'BookingController@saveHistory')->name('booking.save.history');
Route::get('panduan', 'HomeController@panduan')->name('panduan');

 Route::prefix('histori')->group(function () {
        Route::post('/', 'BookingController@historiPost')->name('histori.post');
    });

Route::get('/test', 'HomeController@test');
Route::get('/admin', 'AuthController@index')->name('login');
Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
Route::post('postlogin', ['as' => 'login.post', 'uses' => 'AuthController@post']);

Route::get('/',  'HomeController@index')->name('home');

Route::get('/login', 'HomeController@login')->name('login.home');
Route::post('/login', 'HomeController@loginPost')->name('login.home.post');
Route::get('/register', 'HomeController@register')->name('register');
Route::get('/jadwal', 'HomeController@jadwal')->name('jadwal-front');
Route::post('/register', 'HomeController@registerPost')->name('register.post');
Route::get('/about', 'HomeController@about')->name('tentang');

Route::prefix('payment')->group(function () {
    Route::post('notifications', 'PaymentController@notifications')->name('payement.notifications');
    Route::get('completed', 'PaymentController@completed')->name('payement.completed');
    Route::get('failed', 'PaymentController@failed')->name('payement.failed');
    Route::get('status', 'PaymentController@status')->name('payement.status');
});

Route::prefix('data-booking')->group(function () {
    Route::get('detail', 'BookingController@dataBookingDetail')->name('dataBooking.detail');
});

Route::group(['middleware' => ['auth', 'checkRole:member']], function () {
    Route::prefix('my-dashboard')->group(function(){
        Route::get('/', 'MyDashboardController@index')->name('member.dashboard');

    });

    Route::prefix('profile')->group(function(){
        Route::get('/','UserController@indexProfil')->name('member.profile');
        Route::post('update','UserController@update')->name('member.profile.post');
    });

    Route::prefix('booking')->group(function () {
        Route::get('/', 'BookingController@index')->name('booking');
        Route::post('form', 'BookingController@form')->name('booking.form');
        Route::post('form-edit', 'BookingController@formEdit')->name('booking.form.edit');
        Route::post('form-add', 'BookingController@formAdd')->name('booking.form.add');
        Route::post('form-update', 'BookingController@formUpdate')->name('booking.form.update');
        Route::get('checkout', 'BookingController@checkout')->name('booking.checkout');
        Route::post('pay', 'BookingController@pay')->name('booking.pay');
        Route::get('print/{id}', 'BookingController@print')->name('booking.print');
    });

    Route::prefix('histori')->group(function () {
        Route::get('/', 'BookingController@histori')->name('histori');
        Route::get('detail', 'BookingController@historiDetail')->name('histori.detail');
    });
    Route::prefix('edit-jadwal')->group(function () {
        Route::get('/', 'BookingController@editJadwalIndex')->name('edit-jadwal');
        Route::get('q/{id}', 'BookingController@editJadwalDetail')->name('edit-jadwal-q');
        // Route::get('detail', 'BookingController@historiDetail')->name('histori.detail');
    });
});

Route::group(['middleware' => ['auth', 'checkRole:admin-barang,admin,owner']], function () {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::prefix('profil')->group(function(){
        Route::get('/','UserController@indexProfilAdmin')->name('profil.admin');
        Route::post('update','UserController@updateAdmin')->name('profil.update');
    });
});

Route::group(['middleware' => ['auth', 'checkRole:member,admin,owner']], function () {
    Route::prefix('booking')->group(function () {
    });
});

Route::group(['middleware' => ['auth', 'checkRole:superadmin,admin,owner']], function () {

    Route::prefix('setting')->group(function () {
        Route::get('/', 'SettingController@index')->name('setting');
        Route::post('store', 'SettingController@store')->name('setting.store');
    });

    Route::prefix('jadwal-all')->group(function () {
        Route::get('/', 'BookingController@jadwalAdmin')->name('jadwal-all');
    });

   

    Route::prefix('input')->group(function () {
        Route::get('/', 'InputBookingController@index')->name('input');
        Route::get('getJadwal', 'InputBookingController@getJadwal')->name('input.getJadwal');
        Route::get('getJam', 'InputBookingController@getJam')->name('input.getJam');
        Route::post('store', 'InputBookingController@formAdd')->name('input.store');
        Route::get('proses/{id}', 'InputBookingController@proses')->name('input.proses');
        Route::get('cekStatus', 'InputBookingController@cekStatusPembayaran')->name('input.cekStatus');
    });

    Route::prefix('master-data')->group(function () {
        Route::prefix('item')->group(function () {
            Route::get('/', 'ItemController@index')->name('item');
            Route::get('add', 'ItemController@add')->name('item.add');
            Route::get('edit/{id}', 'ItemController@edit')->name('item.edit');
            Route::get('delete/{id}', 'ItemController@destroy')->name('item.delete');
            Route::post('store', 'ItemController@store')->name('item.store');
            Route::post('update/{id}', 'ItemController@update')->name('item.update');
        });

        Route::prefix('member')->group(function () {
            Route::get('/', 'MemberController@index')->name('member');
            Route::get('add', 'MemberController@add')->name('member.add');
            Route::get('edit/{id}', 'MemberController@edit')->name('member.edit');
            Route::post('store', 'MemberController@store')->name('member.store');
            Route::post('update/{id}', 'MemberController@update')->name('member.update');
        });

       

        Route::prefix('jadwal')->group(function () {
            Route::get('/', 'JadwalController@index')->name('jadwal');
            Route::get('add', 'JadwalController@add')->name('jadwal.add');
            Route::get('cek-jam', 'JadwalController@cekJam')->name('jadwal.cekJam');
            Route::get('cek-jam-edit', 'JadwalController@cekJamEdit')->name('jadwal.cekJamEdit');
            Route::get('edit/{id}', 'JadwalController@edit')->name('jadwal.edit');
            Route::get('delete/{id}', 'JadwalController@destroy')->name('jadwal.delete');
            Route::post('store', 'JadwalController@store')->name('jadwal.store');
            Route::post('update/{id}', 'JadwalController@update')->name('jadwal.update');
        });

       
    });

    Route::get('laporan-all', 'BookingController@laporan')->name('laporan.all');
    Route::post('laporan-all-proses', 'BookingController@laporanProses')->name('dataBooking.laporan.all.proses');


    Route::prefix('data-booking')->group(function () {
        Route::get('list/{status}', 'BookingController@dataBooking')->name('dataBooking');
        Route::get('lunasi', 'BookingController@dataBookingLunasi')->name('dataBooking.lunasi');
        Route::get('virtual/{id}', 'BookingController@dataBookingVirtual')->name('dataBooking.virtual');
        Route::get('cekStatus', 'BookingController@cekStatusPembayaran')->name('dataBooking.cekStatus');
        Route::get('proses/{id}', 'BookingController@dataBookingProses')->name('dataBooking.proses');
        Route::get('selesaikan/{id}', 'BookingController@dataBookingSelesaikan')->name('dataBooking.selesaikan');
        Route::get('siap/{id}', 'BookingController@dataBookingSiap')->name('dataBooking.siap');
        Route::get('cetak', 'BookingController@cetakLaporanIndex')->name('dataBooking.cetak');
        Route::post('cetak-proses', 'BookingController@cetakLaporanProses')->name('dataBooking.cetak-proses');
        // Route::get('detail', 'BookingController@dataBookingDetail')->name('dataBooking.detail');
    });
});



Route::group(['middleware' => ['auth', 'checkRole:admin-barang,owner']], function () {
    Route::prefix('master-data')->group(function () {
        Route::prefix('barang')->group(function () {
            Route::get('/', ['as' => 'barang', 'uses' => 'BarangController@index']);
            Route::get('add', ['as' => 'barang.add', 'uses' => 'BarangController@add']);
            Route::post('store', ['as' => 'barang.store', 'uses' => 'BarangController@store']);
            Route::get('destroy', ['as' => 'barang.destroy', 'uses' => 'BarangController@destroy']);
            Route::get('getKode', ['as' => 'barang.getKode', 'uses' => 'BarangController@getKode']);
            Route::post('cetak', ['as' => 'barang.cetak', 'uses' => 'BarangController@cetak']);
            Route::get('edit/{id}', ['as' => 'barang.edit', 'uses' => 'BarangController@edit']);
            Route::post('update/{id}', ['as' => 'barang.update', 'uses' => 'BarangController@update']);
        });
        Route::prefix('suplier')->group(function () {
            Route::get('/', ['as' => 'suplier', 'uses' => 'SuplierController@index']);
            Route::get('add', ['as' => 'suplier.add', 'uses' => 'SuplierController@add']);
            Route::post('store', ['as' => 'suplier.store', 'uses' => 'SuplierController@store']);
            Route::get('destroy', ['as' => 'suplier.destroy', 'uses' => 'SuplierController@destroy']);
            Route::get('edit/{id}', ['as' => 'suplier.edit', 'uses' => 'SuplierController@edit']);
            Route::post('update/{id}', ['as' => 'suplier.update', 'uses' => 'SuplierController@update']);
        });
        Route::prefix('jenis')->group(function () {
            Route::get('/', ['as' => 'jenis', 'uses' => 'JenisController@index']);
            Route::get('add', ['as' => 'jenis.add', 'uses' => 'JenisController@add']);
            Route::post('store', ['as' => 'jenis.store', 'uses' => 'JenisController@store']);
            Route::get('destroy', ['as' => 'jenis.destroy', 'uses' => 'JenisController@destroy']);
            Route::get('edit/{id}', ['as' => 'jenis.edit', 'uses' => 'JenisController@edit']);
            Route::post('update/{id}', ['as' => 'jenis.update', 'uses' => 'JenisController@update']);
        });
        Route::prefix('merk')->group(function () {
            Route::get('/', ['as' => 'merk', 'uses' => 'MerkController@index']);
            Route::get('add', ['as' => 'merk.add', 'uses' => 'MerkController@add']);
            Route::get('getJenis', ['as' => 'merk.getJenis', 'uses' => 'MerkController@getJenis']);
            Route::get('getJenisEdit', ['as' => 'merk.getJenis.edit', 'uses' => 'MerkController@getJenisEdit']);
            Route::get('getMerk', ['as' => 'merk.getMerk', 'uses' => 'MerkController@getMerk']);
            Route::get('getMerkEdit', ['as' => 'merk.getMerkEdit', 'uses' => 'MerkController@getMerkEdit']);
            Route::get('getKode', ['as' => 'merk.getKode', 'uses' => 'MerkController@getKode']);
            Route::post('store', ['as' => 'merk.store', 'uses' => 'MerkController@store']);
            Route::get('destroy', ['as' => 'merk.destroy', 'uses' => 'MerkController@destroy']);
            Route::get('edit/{id}', ['as' => 'merk.edit', 'uses' => 'MerkController@edit']);
            Route::post('update/{id}', ['as' => 'merk.update', 'uses' => 'MerkController@update']);
        });
        Route::prefix('barangMasuk')->group(function () {
            Route::get('/', ['as' => 'barangMasuk', 'uses' => 'BarangController@indexBarangMasuk']);
            Route::get('add', ['as' => 'barangMasuk.add', 'uses' => 'BarangController@addBarangMasuk']);
            Route::post('addTemp', ['as' => 'barangMasuk.addTemp', 'uses' => 'BarangController@addTempBarangMasuk']);
            Route::post('editTemp', ['as' => 'barangMasuk.editTemp', 'uses' => 'BarangController@editTempBarangMasuk']);
            Route::get('render', ['as' => 'barangMasuk.render', 'uses' => 'BarangController@renderTable']);
            Route::get('edit', ['as' => 'barangMasuk.edit', 'uses' => 'BarangController@editBarangMasuk']);
            Route::get('delete', ['as' => 'barangMasuk.delete', 'uses' => 'BarangController@deleteBarangMasuk']);
            Route::post('store', ['as' => 'barangMasuk.store', 'uses' => 'BarangController@storeBarangMasuk']);
            Route::get('get', ['as' => 'barangMasuk.detail', 'uses' => 'BarangController@detailBarangMasuk']);
            Route::get('print/{id}', ['as' => 'barangMasuk.print', 'uses' => 'BarangController@printBarangMasuk']);
            Route::post('print', ['as' => 'barangMasuk.cetak', 'uses' => 'BarangController@laporanBarangMasuk']);
        });
        Route::prefix('barangKeluar')->group(function () {
            Route::get('/', ['as' => 'barangKeluar', 'uses' => 'BarangController@indexbarangKeluar']);
            Route::get('add', ['as' => 'barangKeluar.add', 'uses' => 'BarangController@addbarangKeluar']);
            Route::post('addTemp', ['as' => 'barangKeluar.addTemp', 'uses' => 'BarangController@addTempbarangKeluar']);
            Route::post('editTemp', ['as' => 'barangKeluar.editTemp', 'uses' => 'BarangController@editTempbarangKeluar']);
            Route::get('render', ['as' => 'barangKeluar.render', 'uses' => 'BarangController@renderTableBarangKeluar']);
            Route::get('edit', ['as' => 'barangKeluar.edit', 'uses' => 'BarangController@editbarangKeluar']);
            Route::get('delete', ['as' => 'barangKeluar.delete', 'uses' => 'BarangController@deletebarangKeluar']);
            Route::post('store', ['as' => 'barangKeluar.store', 'uses' => 'BarangController@storebarangKeluar']);
            Route::get('get', ['as' => 'barangKeluar.detail', 'uses' => 'BarangController@detailbarangKeluar']);
            Route::get('print/{id}', ['as' => 'barangKeluar.print', 'uses' => 'BarangController@printbarangKeluar']);
            Route::post('print', ['as' => 'barangKeluar.cetak', 'uses' => 'BarangController@laporanBarangKeluar']);

        });
    });

});

// Route::group(['middleware' => ['auth', 'checkRole:owner']], function () {

// });
