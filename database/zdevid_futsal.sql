/*
 Navicat Premium Data Transfer

 Source Server         : LOCAL
 Source Server Type    : MySQL
 Source Server Version : 100418
 Source Host           : localhost:8860
 Source Schema         : zdevid_futsal

 Target Server Type    : MySQL
 Target Server Version : 100418
 File Encoding         : 65001

 Date: 30/04/2022 23:42:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_setting
-- ----------------------------
DROP TABLE IF EXISTS `app_setting`;
CREATE TABLE `app_setting`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `app_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `app_logo` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_setting
-- ----------------------------
INSERT INTO `app_setting` VALUES (1, 'DE\'Premiere Futsal', 'DE\'Premiere Futsal', 'app_logo.jpg', '2022-04-29 00:26:17', '2022-04-28 17:44:20');

SET FOREIGN_KEY_CHECKS = 1;
